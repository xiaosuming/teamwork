from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from repository.models.Topic import Topic

# @class TopicView 知识点分类视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	知识点分类视图
#   method:GET POST DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/11/13 10:26:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TopicView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName DeleteTopic
        @api {DELETE} /teamwork/repository/topics/{{topic_id}}/ Topic: Delete Repository Topic
        @apiGroup Repository
        @apiVersion 0.0.1
        @apiDescription [知识点分类管理]删除知识点分类
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request, topic_id):
        # 1 获取参数
        user = request.user
        topic_id = int(topic_id)

        # 2.校验参数
        try:
            topic_obj = Topic.objects.get(id=topic_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的分类不存在")

        # 3 删除知识分类
        topic_obj.delete()

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }
        return JsonResponse(res)
