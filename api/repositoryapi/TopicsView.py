from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from repository.models.Topic import Topic

# @class TopicView 知识点分类视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	知识点分类视图
#   method:GET POST DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/11/13 10:26:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TopicsView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName GetTopic
        @api {GET} /teamwork/repository/topics/ Topics: Get Repository Topics
        @apiGroup Repository
        @apiVersion 0.0.1
        @apiDescription [知识点分类管理]获取知识点分类列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Array} data 返回信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "id": 1,
                    "title": "前端",
                    "children": [
                        {
                        "id": 13,
                        "title": "DOM",
                        "children": [],
                        },
                    ]
                },
            ]
        }
    """
    def get(self, request):
        # 1 组织知识点结构
        topics = Topic.objects.all()
        res_data = list()
        for topic in topics:
            exec_str = 'topic_%d = {"id": %d, "title": "%s", "children":[]}' % (topic.id, topic.id, topic.title)
            exec(exec_str)
        for topic in topics:
            if topic.parent_id:
                exec('topic_%d["children"].append(topic_%d)' % (topic.parent_id, topic.id))
            else:
                exec('res_data.append(topic_%d)' % topic.id)

        # 2.返回应答
        res = {
            'code': 0, 
            'msg': 'SUCCESS',
            'data': res_data,
        }
        return JsonResponse(res)

    """
        @apiName PostTopic
        @api {POST} /teamwork/repository/topics/ Topics: Post Repository Topic
        @apiGroup Repository
        @apiVersion 0.0.1
        @apiDescription [知识点分类管理]新建知识点分类
        @apiParam {int} parent_id 父分类ID
        @apiParam {String} title 分类标题
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Array} data 返回信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "topic_id": 22,
                "warning": "该分类已存在"  当重复创建时
            }
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        parent_id = request.POST.get("parent_id")
        title = request.POST.get("title")

        # 2 校验参数
        if not title:
            raise ParaMissingException(error=u"分类名称缺失")

        # 3.新建分类
        topic_obj, created = Topic.objects.get_or_create(title=title, parent_id=parent_id)

        # 4 返回应答
        res_data = {'topic_id': topic_obj.id}
        if created:
            res_data.update({"warning": "该分类已存在"})
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': res_data,
        }
        return JsonResponse(res)
