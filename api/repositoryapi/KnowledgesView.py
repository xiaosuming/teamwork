from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from repository.models.Topic import Topic
from repository.models.Knowledge import Knowledge

# @class TopicView 知识点视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	知识点视图
#   method:GET POST DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/11/13 10:26:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class KnowledgesView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName GetKnowledge
        @api {GET} /teamwork/repository/knowledges/ KnowledgePoint: Get all Knowledge related to Topic
        @apiGroup Repository
        @apiVersion 0.0.1
        @apiDescription [知识点管理]获取具体类下的知识点
        @apiParam {Int} topic_id 分类id
        @apiParam {String} search 搜索数据
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Array} data 返回信息
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': 'SUCCESS',
            'data': {
                'topic_id': 12,
                'knowledge': [
                    {
                        "id": 1, 
                        "content"： 'PS'
                        "topic_id": 11,
                        "topics": ["PS", "软件使用", "设计"],
                    },
                ],
            }
        }
    """
    def get(self, request):
        # 1 获取参数
        topic_id = request.GET.get('topic_id')
        search = request.GET.get('search')

        # 2 校验参数
        if topic_id:
            try:
                topic_id = int(topic_id)
            except ValueError:
                raise ParaPatternException(error=u'topic_id必须是整数')
        if not search:
            search = ''

        # 3 获取关联的所有知识点
        res_data = list()
        knowledges = Knowledge.objects.filter(content__contains=search)
        if topic_id:
            knowledges = knowledges.filter(topic_id=topic_id)
        for knowledge in knowledges:
            info = {
                'id': knowledge.id,
                'content': knowledge.content,
                'topic_id': knowledge.topic_id
            }
            topics = iterate_topic(knowledge.topic, [])
            info["topics"] = topics
            res_data.append(info)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': res_data,
        }
        return JsonResponse(res)

    """
        @apiName PostKnowledge
        @api {POST} /teamwork/repository/knowledges/ Create a new knowledges
        @apiGroup Repository
        @apiVersion 0.0.1
        @apiDescription [知识点管理]新建知识点
        @apiParam {Int} topic_id 分类id
        @apiParam {String} content 知识点内容
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Json} data 返回信息 
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': 'SUCCESS',
            'data': {
                'id': 13,
                'content': 'ORM原理',
            },
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        topic_id = request.POST.get('topic_id')
        content = request.POST.get('content')

        # 2 校验参数
        if not user.id:
            raise AnonymousUserException(error=u'用户未登录')

        if topic_id:
            try:
                topic = Topic.objects.get(id=int(topic_id))
            except Exception:
                raise ParaPatternException(error=u'topic_id非法')
        else:
            raise ParaMissingException(error=u'topic_id为空')

        if not content:
            raise ParaMissingException(error=u'请输入知识点内容')

        # 3 获取关联的所有知识点
        knowledge = Knowledge.objects.create(
                        topic=topic,
                        content=content,
                        creater=user,
                        )

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': {
                'id': knowledge.id,
                'content': content,
            }
        }
        return JsonResponse(res)


def iterate_topic(topic, title_list):
    """
    返回分类以及父分类的标题类表
    :param topic:
    :param title_list: 分类标题列表
    :return:
    """
    if title_list is None:
        res_list = list()
    else:
        res_list = title_list
    res_list.append(topic.title)
    if topic.parent:
        return iterate_topic(topic.parent, res_list)
    else:
        return res_list
