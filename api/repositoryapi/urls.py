from django.urls import path
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from api.repositoryapi.TopicsView import TopicsView
from api.repositoryapi.TopicView import TopicView
from api.repositoryapi.KnowledgesView import KnowledgesView


app_name = 'repository'


urlpatterns = [
        path('topics/', csrf_exempt(TopicsView.as_view())),
        url(r'topics/(?P<topic_id>\d+)', csrf_exempt(TopicView.as_view())),
        path('knowledges/', csrf_exempt(KnowledgesView.as_view())),
        ]
