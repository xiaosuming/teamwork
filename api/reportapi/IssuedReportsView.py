from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from user.models import User
from report.models.Report import Report
from report.models.ReportUser import ReportUser
from utils.parse_report import parse_brief

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

import datetime


## @class ReportView 发送工作总结视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	发送工作总结视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/10 15:54:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class IssuedReportsView(View):
    def __init__(self):
        pass

    """
        @apiName issuedReportGet
        @api {GET} /teamwork/reports/issued/ Get Issued Report
        @apiGroup Report
        @apiVersion 0.0.1
        @apiDescription [工作总结管理]获取发送的工作总结列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} report 工作总结列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'copy_to_user': ['张三', '里斯'],
                'supervise_user': ['张三'，'里斯'],
                'related_date': "2019-10-09"
                'date': "2019-10-10",
                'report_type': 'd' 日报 'w' 周报 'm' 月报
                'plain_text': '春梦正关情，镜中蝉鬓轻。',
                'readed': True,
                }
            ]
        }
    """
    def get(self, request):
        # 1. 获取用户
        user = request.user

        # 2. 校验用户
        if user.username == None or user.username == '':
            raise AnonymousUserException(error=u'用户未登录')

        # 3.1 获取用户发送的邮件
        report_queryset = Report.objects.filter(user=user.id).order_by('-created_time')

        # 3.2 组织工作报告数据
        report_list = parse_brief(report_queryset)

        # 4.1 组织应答数据
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": report_list,
                }

        # 4.2 返回应答
        return JsonResponse(data=res) 

    """
        @apiName issuedReportPost
        @api {POST} /teamwork/reports/issued/ Post Issued Report
        @apiGroup Report
        @apiVersion 0.0.1
        @apiDescription [工作总结管理]发送工作总结
        @apiParam {list} copy_to_user 抄送用户id列表
        @apiParam {list} supervise_user 审阅用户id列表
        @apiParam {Date} related_date 工作总结对应日期
        @apiParam {string} report_type 工作总结类型 'd' 日报 'w' 周报 'm' 月报
        @apiParam {String} plain_text 纯文本内容
        @apiParam {String} rich_text 富文本内容
        @apiParam {List} diagram [{'content': 'Vue.js', 'project': '', 'task': '工作总结编辑器', 'knowledge': '前端'},] 结构树
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {String} data 工作总结id
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            'data': 47,
        }
    """
    def post(self, request):
        # 1. 获取参数
        reportUser = request.user
        report_type = request.POST.get('report_type')
        relatedDate = request.POST.get('related_date')
        plain_text = request.POST.get('plain_text')
        richText = request.POST.get('rich_text')
        diagram = str(request.POST.get('diagram'))
        copy_to_user = eval(request.POST.get('copy_to_user'))
        supervise_user = eval(request.POST.get('supervise_user'))

        # 2. 校验参数
        if reportUser.username == None or reportUser.username == '':
            raise AnonymousUserException(error=u'用户未登录')
        if report_type == None or report_type == '':
            raise ParaMissingException(error=u'请选择工作总结类型')
        if report_type not in ('d', 'w', 'm'):
            raise ParaPatternException(error=u'不能识别的工作总结类型')
        if plain_text == None or plain_text == '':
            raise ParaMissingException(error=u'纯文本内容不能为空')
        if richText == None or richText == '':
            raise ParaMissingException(error=u'富文本内容不能为空')
        try:
            relatedDate = datetime.date.fromisoformat(relatedDate)
        except Exception:
            raise ParaPatternException(error=u'请输入%s格式的日报相关日期'%("'%Y-%m-%d'"))

        # 3. 数据存储 
        with transaction.atomic():
            # 将日报存入工作总结数据库
            report = Report.objects.create(
                    user=reportUser,
                    related_date=relatedDate,
                    report_type=report_type,
                    plain_text=plain_text,
                    rich_text=richText,
                    diagram=diagram,
                    )
            print('stored rich text:', report.rich_text)

            # 建立工作报告与抄送人、审阅人的多对多联系
            for user in copy_to_user:
                user = User.objects.filter(id=user).first()
                if user == None:
                    continue
                ReportUser.objects.create(
                        report = report,
                        related_user = user,
                        relation_type = 'c',
                        )

            for user in supervise_user:
                user = User.objects.filter(id=user).first()
                if user == None:
                    continue
                ReportUser.objects.create(
                        report = report,
                        related_user = user,
                        relation_type = 's',
                        )

        send_message_users = set(copy_to_user+supervise_user)

        channel_layer = get_channel_layer()
        print(reportUser)
        json_message = {
            'id': report.id,
            'user': reportUser.username,
            'work_date':relatedDate.isoformat(),
            'date':report.created_time.strftime("%Y-%m-%d %H:%M:%S"),
            'report_type': report.get_report_type_display(),
            'plain_text': report.plain_text,
        }
        for user in send_message_users:
            async_to_sync(channel_layer.group_send)(
                "notification_"+str(user), 
                {
                    'type': 'get_report',
                    'message': json_message,
                }
            )

        # 4. 返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": report.id,
                }

        return JsonResponse(data = res)
