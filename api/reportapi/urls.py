from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from api.reportapi.IssuedReportsView import IssuedReportsView
from api.reportapi.ReceivedReportsView import ReceivedReportsView
from api.reportapi.ReportView import ReportView
from api.reportapi.EnclosuresView import EnclosuresView
from api.reportapi.EnclosureView import EnclosureView


app_name = 'report'


urlpatterns = [
        url(r'(?P<report_id>\d+)/$', csrf_exempt(ReportView.as_view())),
        url('^issued/$', csrf_exempt(IssuedReportsView.as_view())),
        url('^received/$', csrf_exempt(ReceivedReportsView.as_view())),
        url(r'^(?P<report_id>\d+)/enclosures/$', csrf_exempt(EnclosuresView.as_view())),
        url(r'^enclosures/(?P<enclosure_id>)/$', csrf_exempt(EnclosureView.as_view())),
        ]
