from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.FileMerge import FileManager
from report.models.Report import Report
from report.models.ReportEnclosure import ReportEnclosure
from enclosure.models.Enclosure import Enclosure

# @class EnclosuresView 工作总结评论附件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目附件视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/06 20:07:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class EnclosuresView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.file_manager = FileManager()

    """
        @apiName report_enclosures_post
        @api {POST} /teamwork/reports/{{report_id}}/enclosures/ Enclosures: Attach Enclosure to Report
        @apiGroup Report
        @apiVersion 0.0.1
        @apiDescription [任务评论附件]添加工作总结附件
        @apiParam {String} name 文件名称
        @apiParam {Int} enclosure_id 附件id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件列表
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
        }
    """
    def post(self, request, report_id):
        # 1 获取参数
        user = request.user
        name = request.POST.get('name')
        enclosure_id = request.POST.get('enclosure_id')

        # 2 校验参数
        if not name:
            raise ParaMissingException(error=u'文件名为空')

        try:
            report_id = int(report_id)
        except ValueError:
            raise ParaPatternException(error=u'参数格式错误')
        try:
            report = Report.objects.get(id=report_id)
        except ObjectDoesNotExist:
            raise ParaMissingException(error=u'评论不存在')

        try:
            enclosure_id = int(enclosure_id)
        except ValueError:
            raise ParaPatternException(error=u"附件id必须为整数")
        if Enclosure.objects.filter(id=enclosure_id).count() == 0:
            raise UnknowResourcesException(error=u'附件不存在')

        # 3. 创建项目附件
        enclosure_obj = ReportEnclosure.objects.create(
            user=user,
            name=name,
            report=report,
            enclosure_id=enclosure_id,
        )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': enclosure_obj.id,
                }
        return JsonResponse(res)
