from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.FileMerge import FileManager
from report.models.ReportEnclosure import ReportEnclosure

# @classTaskEnclosuresView 工作总结附件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目附件视图
#   method:DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/006 20:13:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class EnclosureView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.file_manager = FileManager()

    """
        @apiName report_enclosure_delete
        @api {DELETE} /teamwork/reports/enclosures/{{enclosure_id}}/ Enclosure: Delete Enclosures
        @apiGroup Report
        @apiVersion 0.0.1
        @apiDescription [任务附件]删除任务附件
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件id
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
        }
    """
    def get(self, request, enclosure_id):
        # 1 获取参数
        user = request.user
        enclosure_id = int(enclosure_id)

        # 2 校验参数
        try:
            enclosure = ReportEnclosure.objects.get(id=enclosure_id)
        except ObjectDoesNotExist:
            raise ParaMissingException(error=u'附件不存在')

        report = enclosure.report
        if user != report.user:
            raise AuthenticationException(error=u"没有操作权限")

        # 3 删除附件
        # 3.1.删除无关联的附件文件
        self.file_manager.file_delete(enclosure.enclosure)
        # 3.2.删除附件与任务的关联记录
        enclosure.delete()

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
                }
        return JsonResponse(res)
