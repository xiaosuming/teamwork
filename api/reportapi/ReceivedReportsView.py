from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from user.models import User
from report.models.Report import Report
from report.models.ReportUser import ReportUser
from utils.parse_report import parse_brief


## @class ReceivedReportView 收到工作总结视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	收到工作总结视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/11 10:39:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ReceivedReportsView(View):
    def __init__(self):
        pass

    """
        @apiName receivedReportsGet
        @api {GET} /teamwork/reports/received/ Get Received Report
        @apiGroup Report
        @apiVersion 0.0.1
        @apiDescription [工作总结管理]获取收到的工作总结列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Array} report 工作总结列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'user': '张三'
                'copy_to_user': ['张三', '里斯'],
                'supervisor': ['张三'，'里斯'],
                'date': "2019-10-10",
                'report_type': 'd' 日报 'w' 周报 'm' 月报
                'content': '{
                              ops: [
                                { insert: 'Gandalf', attributes: { bold: true } },
                                { insert: ' the ' },
                                { insert: 'Grey', attributes: { color: '#cccccc' } }
                              ]
                            }',
                'diagram': [
                            {
                            "content": 'Gandalf'i,
                            "project": '',
                            "task": '',
                            "knowledge": '前端',
                            },
                        ]
                }
            ]
        }
    """
    def get(self, request):
        # 1 获取用户
        user = request.user
        user_id = user.id

        # 2 校验用户身份
        if user_id == None or user_id == '':
            raise AnonymousUserException(error=u'用户未登录')
        if user.is_active == 0:
            raise AuthenticationException(error=u'没有操作权限')

        # 3.1 获取关联工作总结id
        report_id_list = ReportUser.objects.filter(related_user_id=user_id).values_list('report_id', flat=True).distinct()

        # 3.2 获取关联工作总结
        report_queryset = Report.objects.filter(id__in=report_id_list).order_by('-created_time')

        # 3.3 组织工作总结数据
        report_list = parse_brief(report_queryset, received=True, user=user)

        # 4.1 组织应答数据
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": report_list,
                }
        
        # 4.2 返回应答数据
        return JsonResponse(res)
