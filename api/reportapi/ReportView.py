from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from user.models import User
from report.models.Report import Report
from report.models.ReportUser import ReportUser
from utils.parse_report import parse_detail


## @class ReceivedReportView 收到工作总结视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	收到工作总结视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/11 10:39:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ReportView(View):
    def __init__(self):
        pass

    """
        @apiName report_idGet
        @api {GET} /teamwork/reports/id/ Get Report
        @apiGroup Report
        @apiVersion 0.0.1
        @apiDescription [工作总结管理]获取收到的工作总结
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Array} report 工作总结列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                'user': '张三'
                'copy_to_user': ['张三', '里斯'],
                'supervisor': ['张三'，'里斯'],
                'date': "2019-10-10",
                'report_type': '日报',
                'plain_text': '花楼音信断，芳草江南岸。',
                'rich_text': {
                              ops: [
                                { insert: 'Gandalf', attributes: { bold: true } },
                                { insert: ' the ' },
                                { insert: 'Grey', attributes: { color: '#cccccc' } }
                              ],
                            },
                'diagram': [
                            {
                            "content": 'Gandalf'i,
                            "project": '',
                            "task": '',
                            "knowledge": '前端',
                            },
                        ]
                    }
        }
    """
    def get(self, request, report_id):
        # 1 获取参数
        user = request.user
        user_id = user.id
        report_id = int(report_id)

        # 2 校验参数
        if user_id == None or user_id == '':
            raise AnonymousUserException(error=u'用户未登录')
        if user.is_active == 0:
            raise AuthenticationException(error=u'没有操作权限')

        try:
            reportObj = Report.objects.get(id=report_id, user=user)
        except Report.DoesNotExist:
            reportObj = ReportUser.objects.filter(report_id=report_id, related_user=user).first().report
            
        if not reportObj:
            raise AuthenticationException(error=u'用户没有浏览权限')

        # 3 获取工作总结详情
        reportDetail = parse_detail(reportObj)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': "SUCCESS",
                'data': reportDetail,
                }
        
        return JsonResponse(res)


    """
        @apiName report_idPut
        @api {PUT} /teamwork/reports/id/ Change Readed Value of a Report
        @apiGroup Report
        @apiVersion 0.0.1
        @apiDescription [工作总结管理]修改收到的工作总结为已读
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request, report_id):
        # 1.1 获取参数
        user = request.user
        user_id = user.id
        report_id = report_id

        # 2 校验参数
        if user_id == None or user_id == '':
            raise AnonymousUserException(error=u'用户未登录')
        elif user.is_active == 0:
            raise AuthenticationException(error=u'没有访问权限')

        reportRelations = ReportUser.objects.filter(report_id=report_id, related_user=user)

        # 3 修改已读状态
        for reportRelation in reportRelations:
            reportRelation.readed=True
            reportRelation.save()

        # 4 返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                }

        return JsonResponse(res)
