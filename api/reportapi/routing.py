from django.urls import re_path
from reportapi.ReportConsumers import ReportConsumers


websocket_urlpatterns = [
    # re_path(r'ws/comment/(?P<room_name>\w+)/$', consumers.CommentConsumer),    
    re_path(r'msg/(?P<userID>\w+)/$', ReportConsumers),    
]
