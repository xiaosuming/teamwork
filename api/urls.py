from django.urls import path
from django.urls import re_path
from django.urls import include


urlpatterns = [
    re_path('^users/', include('api.userapi.urls')),
    re_path('^groups/', include('api.groupapi.urls')),
    re_path('^reports/', include('api.reportapi.urls')),
    re_path('^privategroups/', include('api.privategroupapi.urls')),
    re_path('^projects/', include('api.projectapi.urls')),
    re_path('^tasks/', include('api.taskapi.urls')),
    re_path('^star/', include('api.interestapi.urls')),
    re_path('^enclosures/', include('api.enclosureapi.urls')),
    path('attendances/', include('api.attendanceapi.urls')),
    path('tags/', include('api.tagapi.urls')),
    path('agenda/', include('api.agendaapi.urls')),
    path('repository/', include('api.repositoryapi.urls')),
    path('oauth/', include('api.oauth.urls')),
]
