from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from api.enclosureapi.EnclosureUploadView import EnclosureUploadView
from api.enclosureapi.EnclosureMergeView import EnclosureMergeView


app_label = 'enclosure1'


urlpatterns = [
        path('upload/', csrf_exempt(EnclosureUploadView.as_view())),
        path('merge/', csrf_exempt(EnclosureMergeView.as_view()))
        ]
