from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
import os

# @class EnclosuresView 附件切片上传视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	附件切片上传视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/22 17:04:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class EnclosureUploadView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName enclosureUpload
        @api {POST} /teamwork/enclosures/upload/ UpLoad Enclosure
        @apiGroup Enclosures
        @apiVersion 0.0.1
        @apiDescription [附件管理]附件切片上传
        @apiParam {File} file 文件流
        @apiParam {String} resumableChunkNumber 序号
        @apiParam {String} resumableIdentifier 唯一标识
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request):
        # 1 获取参数
        stream = request.FILES['file'].file.read()
        seq = request.POST.get('resumableChunkNumber')
        uid = request.POST.get('resumableIdentifier')

        # 2 参数校验
        if not uid:
            raise ParaMissingException(error=u'uid为空')

        if seq is None:
            raise ParaMissingException(error=u'resumableChunkNumber为空')

        # 3 存储切片文件至临时文件夹 'media/temp/qdA4Mndd.../'
        path = os.path.join('media/temp/', str(uid))
        if not os.path.exists(path):
            os.makedirs(path)

        # 'media/temp/qdA4Mndd.../01'
        filepath = os.path.join(path, str(seq))

        with open(filepath, 'wb+') as fp:
            fp.write(stream)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }
        return JsonResponse(res)
