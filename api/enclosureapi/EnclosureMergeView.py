from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.FileMerge import FileManager
import os

# @class CommentEnclosuresView 任务评论附件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目附件视图
#   method:GET, POST, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/03 20:07:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class EnclosureMergeView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.file_manager = FileManager()

    """
        @apiName task_c_enclosures_post
        @api {POST} /teamwork/enclosures/merge/ Enclosures: Merge split enclosure
        @apiGroup Enclosures
        @apiVersion 0.0.1
        @apiDescription [任务评论附件]合并附件切片
        @apiParam {String} name 文件名称
        @apiParam {String} type 文件类型
        @apiParam {String} uid 唯一标识
        @apiParam {Int} totalchunk 切片总数
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件列表
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
            'data': {
                'id': 152
                'src': 'media/xxx.jpg'
            }
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        name = request.POST.get('name')
        file_type = request.POST.get('type')
        uid = request.POST.get('uid')
        total_chunk = request.POST.get('totalchunk')

        # 2 校验参数
        if not name:
            raise ParaMissingException(error=u'文件名为空')

        if not uid:
            raise ParaMissingException(error=u'文件标识符为空')

        try:
            total_chunk = int(total_chunk)
        except ValueError:
            raise ParaPatternException(error=u'请输入正确的切片数量')

        tmp_dir = os.path.join('media/temp', str(uid))
        if not os.path.exists(tmp_dir):
            raise ParaMissingException(error=u'切片文件不存在')

        # 3 存储文件
        # 3.1 读取切片文件写入
        enclosure_id, enclosure_path = self.file_manager.file_merge(
            tmp_dir, name, file_type, total_chunk)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': {
                "id": enclosure_id,
                "src": enclosure_path
            }
        }
        return JsonResponse(res)
