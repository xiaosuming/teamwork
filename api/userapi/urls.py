from django.urls import path, re_path
from django.views.decorators.csrf import csrf_exempt
from api.userapi.UsersView import UsersView
from api.userapi.login import LoginView
from api.userapi.PasswordView import PasswordView
from api.userapi.UserView import UserView


app_name = 'user'


urlpatterns = [
        path('<int:user_id>/', csrf_exempt(PasswordView.as_view())),
        path('login/', csrf_exempt(LoginView.as_view())),
        re_path('^$', csrf_exempt(UsersView.as_view())),
        re_path('^user/$', csrf_exempt(UserView.as_view())),
]
