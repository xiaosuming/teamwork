from django.views.generic import View
from django.http import JsonResponse
import random


## @class UserView 用户信息视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	用户信息视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/10/09 14:10|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class UserView(View):
    raise_exception = True
    """
        @apiName userGet
        @api {GET} /teamwork/users/user/ Get User Info
        @apiGroup User
        @apiVersion 0.0.1
        @apiDescription [用户管理]获取当前用户信息
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {List} data 所有用户信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data":[
                {
                'user_id': 1,
                'user_nickname': 'nickname',
                'user_colour': '#ffffff'
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        user_obj = request.user

        # 2.组织用户信息
        if not user_obj.colour:
            user_obj.colour = random_colour()
            user_obj.save()
        user_info = {
            "username": user_obj.username,
            "user_nickname": user_obj.nickname,
            "user_colour": user_obj.colour,
            "user_id": user_obj.id
        }

        # 2 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': user_info,
        }
        return JsonResponse(res)


def random_colour():
    COLOR_CHOICES = (
            '#e9e7ef', '#d6ecf0', '#f3f9f1',
            '#e0eee8', '#c0ebd7', '#88ada6',
            '#3d3b4f', '#e4c6d0', '#4c8dae',
            '#003472', '#3eede7', '#a78e44',
            '#eedeb0', '#f0c239', '#ffc773',
            '#3de1ad', '#a4e2c6', '#758a99',
            '#50616d', '#1bd1a5', '#003371',
            )

    return random.choice(COLOR_CHOICES)
