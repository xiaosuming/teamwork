from django.views.generic import View
from django.http import JsonResponse
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.get_all_users import get_all_users
from user.models import User
from user.profile import profile
import re
import random
import json


## @class UserView 用户视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	用户信息视图
#   method:POST DELETE PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/09 14:10|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class UsersView(LoginRequiredMixin, View):
    raise_exception = True
    """
        @apiName userGet
        @api {GET} /teamwork/users/ Get Users
        @apiGroup User
        @apiVersion 0.0.1
        @apiDescription [用户管理]获取用户
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {List} data 所有用户信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data":[
                {
                'user_id': 1,
                'user_nickname': 'nickname',
                'user_colour': '#ffffff'
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        userInfo = get_all_users()

        # 2 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': userInfo,
        }
        return JsonResponse(res)


    """
        @apiName userPost
        @api {POST} /teamwork/users/ Add New User
        @apiGroup User
        @apiVersion 0.0.1
        @apiDescription [用户管理]添加用户
        @apiParam {Json} data 用户信息 (username, password, nickname, email)
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {int} user_id 用户id
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data":1,
        }
    """
    def post(self, request):
        # 1.获取参数
        creater = request.user
        print("creater:", creater.username)
        userInfo = json.loads(request.body.decode("utf8")).get('data')
        
        # 2.校验参数
        if creater.username == None or creater.username == '':
            raise AnonymousUserException(error=u'用户未登录')
        if not creater.is_superuser:
            raise AuthenticationException(error=u'当前用户没有操作权限')

        if not userInfo:
            raise ParaMissingException(error=u'用户信息为空')

        # try:
        #     userInfo = eval(userInfo)
        # except Exception:
        #     raise ParaPatternException(error=u'用户信息格式错误')

        try:
            print('userInfo:', userInfo)
            print(type(userInfo))
            username = userInfo['username']
        except Exception:
            raise ParaPatternException(error=u'登录名为空或格式错误')

        try:
            password = userInfo['password']
        except Exception:
            raise ParaPatternException(error=u'初始密码为空或格式错误')

        try:
            nickname = userInfo['nickname']
        except Exception:
            raise ParaPatternException(error=u'用户姓名为空或格式错误')

        try:
            email = userInfo['email']
        except Exception:
            raise ParaMissingException(error=u'请输入用户邮箱')
        else:
            if re.match("^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+", email) is None:
                raise ParaPatternException(error=u'邮箱格式不正确')

        # 3.1创建用户
        user = User.objects.create(
                username=username,
                password=make_password(password),
                nickname=nickname,
                colour=self.random_colour(),
                email=email,
                )

        user_id = user.id

        # 3.2将新用户加入全体用户用户组
        all_user_group = Group.objects.get(id=1)
        user.groups.add(all_user_group)

        # 4.返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": user_id,
                }
        return JsonResponse(data = res)


    """
        @apiName userDelete
        @api {DELETE} /teamwork/users/ Delete User
        @apiGroup User
        @apiVersion 0.0.1
        @apiDescription [用户管理]删除用户
        @apiParam {String} username 用户名
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {int} user_id 用户id
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data":1,
        }
    """
    def delete(self, request):
        # 1 获取参数
        operator = request.user
        username = request.GET.get('username')
        print(username)

        # 2 校验参数
        if operator.username == None or operator.username == '':
            raise AnonymousUserException(error=u'用户未登录')
        if not operator.is_superuser:
            raise AuthenticationException(error=u'当前用户没有操作权限')
        if username == None or username == '':
            raise ParaMissingException(error=u'用户名不能为空')
        
        user = User.objects.filter(username=username).first()
        if user == None:
            raise UnknownResourceException(error=u'用户不存在')

        # 3.1标记用户被删除 
        user.is_active = 0
        user.save()
        user_id = user.id
        # 3.2删除用户与用户组的联系


        # 4.返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": user_id,
                }
        return JsonResponse(data = res)


    def random_colour(self):
        COLOR_CHOICES = (
                '#e9e7ef', '#d6ecf0', '#f3f9f1', 
                '#e0eee8', '#c0ebd7', '#88ada6',
                '#3d3b4f', '#e4c6d0', '#4c8dae',
                '#003472', '#3eede7', '#a78e44',
                '#eedeb0', '#f0c239', '#ffc773',
                '#3de1ad', '#a4e2c6', '#758a99',
                '#50616d', '#1bd1a5', '#003371',
                )

        return random.choice(COLOR_CHOICES)
