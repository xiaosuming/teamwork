from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.ParaMissingException import ParaMissingException
from user.models import User


class PasswordView(View):
    def __init__(self):
        pass

    """
        @apiName userPWPost
        @api {POST} /teamwork/user/user_id/ Change User Password
        @apiGroup User
        @apiVersion 0.0.1
        @apiDescription [用户管理]添加用户
        @apiParam {String} password 新设密码
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request, user_id):
        # 1 获取参数
        print('修改密码')
        user = request.user
        print(user, user.id)
        userID = user_id
        print('userID', userID)
        password = request.POST.get('password')
        print("password:", password)
        
        # 2 校验参数
        if user.id != userID:
            raise AnonymousUserException(error=u'当前用户没有操作权限')
        
        if password == None or password == '':
            raise ParaMissingException(error=u'新设密码不能为空')

        # 3 设置密码 
        #user.password = make_password(password)
        user.set_password(password)
        user.save()

        # 4.返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                }
        return JsonResponse(data = res)
