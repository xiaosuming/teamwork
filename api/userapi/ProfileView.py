from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.AuthenticationException import AuthenticationException
import datetime


class ProfileView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName userProfilePost
        @api {POST} /teamwork/user/profile/ Change User P
        @apiGroup User
        @apiVersion 0.0.1
        @apiDescription [用户管理]添加用户
        @apiParam {JSON} data {
                                'user_id': 6, 
                                'profile': {
                                    'gender': 'm', 
                                    'birthday': '2019-10-11', 
                                    'IDtype': 'id', 
                                    'IDnumber': '12345678901', 
                                    'education': 'b', 
                                    'college': '清华大学',
                                    }
                                }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request, user_id):
        # 1 获取参数
        user = request.user
        user_id = user.id
        profile = request.POST.get('profile')
        
        # 2 校验参数
        if not user.issupervisor:
            raise AuthenticationException(error=u'当前用户没有操作权限')
        
        if profile is None or profile == '':
            raise ParaMissingException(error=u'用户信息不能为空')

        try:
            profile = eval(profile)
            assert type(profile) is dict
        except Exception:
            raise ParaMissingException(error=u'用户信息格式错误')

        try:
            gender = profile['gender']
        except Exception:
            raise ParaMissingException(error=u'用户性别参数错误')
        else:
            if not gender in ('m', 'f'):
                raise ParaMissingException(error=u'非法的用户性别')

        try:
            birthday = datetime.date.fromisoformat(profile['birthday'])
        except Exception:
            raise ParaMissingException(error=u'请输入符合%s格式的出生日期' % '%Y-%m-%d')

        try:
            id_type = profile['IDtype']
        except Exception:
            raise ParaMissingException(error=u'请输入用户证件类型')
        else:
            if id_type not in ('id', 'pa', 'hp', 'tp', 'fi', 'hr'):
                raise ParaMissingException(error=u'非法的证件类型')

        try:
            id_number = profile['IDnumber']
        except Exception:
            raise ParaMissingException(error=u'请输入用户证件号')
        else:
            if id_type == 'id':
                if len(id_number) == 18:
                    *head, butt = id_number
                    butt_str = '10x98765432'
                    butt_map = [*butt_str]
                    weight = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
                    res = 0
                    for i in range(0, 17):
                        res += int(head[i]) * weight[i]
                    if butt_map[res % 11] != butt:
                        raise ParaMissingException(error=u'身份证号格式不正确')
                else:
                    raise ParaMissingException(error=u'身份证号格式不正确')

        try:
            education = profile['education']
        except Exception:
            raise ParaMissingException(error=u'请输入用户学历')
        else:
            if education not in ('b', 'm', 'd', 'e'):
                raise ParaMissingException(error=u'请输入正确的学历选项')

        try:
            college = profile['profile']
        except Exception:
            raise ParaMissingException(error=u'请输入用户毕业院校')

        # 3 存储数据
        profile.objects.create(
            user=user,
            gender=gender,
            birthday=birthday,
            IDtype=id_type,
            IDnumber=id_number,
            education=education,
            college=college
        )

        # 4.返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                }
        return JsonResponse(data = res)
