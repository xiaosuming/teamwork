1from django.views.generic import View
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.utils.safestring import mark_safe
from utils.errorcode.AnonymousUserException import AnonymousUserException 
import json


class LoginView(View):
    def get(self, request):
        return render(request, 'login.html')

    """
        @apiName login_post
        @api {PUT} /teamwork/users/login/ Users: log in the user
        @apiGroup User
        @apiVersion 0.0.1
        @apiDescription [任务提醒]用户登录
        @apiParam {String} username 用户名
        @apiParam {String} password 密码
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request):
        # 1 获取参数
        username = request.POST.get('username')
        password = request.POST.get('password')

        # 2 校验用户身份
        user = authenticate(username=username, password=password)
        print(user)

        if user:
            # 调用login函数，返回sessionid并设置过期时间
            login(request, user)
            request.session.set_expiry(60*60*24*7)

            return JsonResponse(
                {
                    "code": 0,
                    "msg": "SUCCESS"
                }
            )
            # 返回消息监控页面，查看WebSocket返回的消息
            # return render(
            #     request,
            #     'reportmsg.html',
            #     {
            #     'userID': mark_safe(
            #     json.dumps(user.id)
            #     ),
            #     },
            # )

        else:
            raise AnonymousUserException(error=u'用户名或密码错误')
