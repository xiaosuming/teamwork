from django.db.models import Q
from task.models.Task import Task
from task.models.SubTask import SubTask


class TaskMixin:
    def availabel_task(self, user):
        task_ids = Task.objects.filter(
            Q(director=user) | Q(supervisor=user) |
            Q(task_participant__participant=user) |
            Q(task_visitor__visitor=user)
        ).values_list("id", flat=True)
        task_id_set = set(task_ids)
        return task_id_set

    def can_verify_task(self, user):
        """
        返回可以审核的任务id
        :param user: 用户实例
        :return: 任务id列表
        """
        task_ids = Task.objects.filter(
            supervisor=user
        ).values_list("id", flat=True)
        task_ids = set(task_ids)
        return task_ids

    def can_apply_task(self, user):
        """
        返回可以请求审核的任务id
        :param user: 用户实例
        :return: 任务id列表
        """
        task_ids = Task.objects.filter(
            director=user
        ).values_list("id", flat=True)
        task_ids = set(task_ids)
        return task_ids

    def can_suspend_task(self, user):
        """
        返回可以取消的任务id
        :param user: 用户实例
        :return: 任务id列表
        """
        task_ids = Task.objects.filter(
            Q(director=user) | Q(supervisor=user)
        ).values_list("id", flat=True)
        task_ids = set(task_ids)
        return task_ids

    def can_verify_sub_task(self, user):
        """
        返回可以审核的子任务id
        :param user: 用户实例
        :return: 任务id列表
        """
        task_ids = SubTask.objects.filter(
            task__director=user
        ).values_list("id", flat=True)
        task_ids = set(task_ids)
        return task_ids

    def can_apply_sub_task(self, user):
        """
        返回可以请求审核的子任务id
        :param user: 用户实例
        :return: 任务id列表
        """
        task_ids = SubTask.objects.filter(
            participant__participant=user
        ).values_list("id", flat=True)
        task_ids = set(task_ids)
        return task_ids

    def can_suspend_sub_task(self, user):
        """
        返回可以取消的任务id
        :param user: 用户实例
        :return: 任务id列表
        """
        task_ids = Task.objects.filter(
            Q(participant=user) | Q(task__director=user)
        ).values_list("id", flat=True)
        task_ids = set(task_ids)
        return task_ids

    def task_participant(self, task):
        """
        返会任务的参与人
        :param task: 任务实例
        :return: 参与人id
        """
        taskparticipants = task.task_participant
        participant_ids = taskparticipants.values_list("participant__id", flat=True)
        return list(participant_ids)
