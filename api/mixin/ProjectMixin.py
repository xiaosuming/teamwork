from django.db.models import Q
from project.models.Project import Project


class ProjectMixin:
    def availabel_project(self, user):
        """
        可访问的项目
        :param user: 用户实例
        :return: 可访问的项目id
        """
        project_ids = Project.objects.filter(
            Q(secret=False)| Q(director=user) | Q(supervisor=user) |
            Q(projectparticipant__participant=user) |
            Q(projectvisitor__visitor=user)
        ).values_list("id", flat=True)
        project_ids = set(project_ids)
        return project_ids

    def can_verify_project(self, user):
        """
        返回可以审核的项目id
        :param user: 用户实例
        :return: 项目id列表
        """
        project_ids = Project.objects.filter(
            supervisor=user
        ).values_list("id", flat=True)
        project_ids = set(project_ids)
        return project_ids

    def can_apply_project(self, user):
        """
        返回可以请求审核的项目id
        :param user: 用户实例
        :return: 项目id列表
        """
        project_ids = Project.objects.filter(
            director=user
        ).values_list("id", flat=True)
        project_ids = set(project_ids)
        return project_ids

    def can_suspend_project(self, user):
        """
        返回可以取消的项目id
        :param user: 用户实例
        :return: 项目id列表
        """
        project_ids = Project.objects.filter(
            Q(director=user) | Q(supervisor=user)
        ).values_list("id", flat=True)
        project_ids = set(project_ids)
        return project_ids
