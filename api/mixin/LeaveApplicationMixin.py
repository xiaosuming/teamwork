from attendance.models.LeaveApplication import LeaveApplication


class LeaveApplicationMixin:
    def received_applications(self, user, approved=None):
        """
        返回受到的请假申请列表
        :param user: 用户实例
        :param approved: 是否已批准, None全部, False未批准, True已批准
        :return: 请假申请对象
        """
        applications = LeaveApplication.objects.filter(supervisor=user)
        if approved is False:
            applications = applications.filter(approved=False)
        if applications is True:
            applications = applications.filter(applications=True)

        return applications

    def applied_applications(self, user, start_time=None, end_time=None, approved=None):
        """
        返回提交的请假申请列表
        :param user: 用户实例
        :param start_time: 起始时间
        :param end_time: 截止时间
        :param approved: 是否批准
        :return: 请假申请查询集
        """
        applications = LeaveApplication.objects.filter(applicant=user)
        if start_time:
            applications = applications.filter(start_time__gte=start_time)
        if end_time:
            applications = applications.filter(end_time__lte=end_time)
        if approved is False:
            applications = applications.filter(approved='d')
        elif applications is True:
            applications = applications.filter(approved='a')

        return applications
