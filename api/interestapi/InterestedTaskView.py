from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.AuthenticationException import AuthenticationException
from task.models.Task import Task
from task.models.TaskVisitor import TaskVisitor
from interest.models.InterestedTask import InterestedTask

# @class InterestedTaskView 关注任务视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	关注任务视图
#   method:POST DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/22 11:20:10|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class InterestedTaskView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName interestedTaskPost
        @api {POST} /teamwork/star/tasks/ Interested: Post Interested Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [关注任务管理]新建关注任务
        @apiParam {List} task_id 新增关注任务id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        task_id = request.POST.get('task_id')

        # 2 校验参数
        try:
            task_id = int(task_id)
        except Exception:
            raise ParaPatternException(error=u'请输入正确的任务id')

        try:
            task_obj = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise ParaMissingException(error=u'当前任务不存在')

        if task_obj.secret and TaskVisitor.objects.filter(task=task_obj, visitor=user).count() == 0:
            raise AuthenticationException(error=u'当前任务不可见')

        # 3 存储数据
        if InterestedTask.objects.filter(task=task_obj, user=user).count() != 0:
            pass
        else:
            InterestedTask.objects.create(
                task=task_obj,
                user=user,
            )

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }
        return JsonResponse(data=res)

    """
        @apiName interestedTaskDelete
        @api {DELETE} /teamwork/star/tasks/ Interested: Delete Interested Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [关注任务管理]删除关注任务
        @apiParam {Int} task_id 取消关注的任务的id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request):
        # 1 获取参数
        user = request.user
        task_id = request.GET.get('task_id')

        # 2 校验参数
        try:
            task_id = int(task_id)
        except TypeError:
            raise ParaPatternException(error=u'请输入正确的任务id')

        try:
            interested_obj = InterestedTask.objects.get(task_id=task_id, user=user)
        except ObjectDoesNotExist:
            raise ParaMissingException(error=u'用户未关注请求任务')

        # 3 删除任务
        interested_obj.delete()

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }

        return JsonResponse(res)
