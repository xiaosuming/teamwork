from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.AnonymousUserException import AnonymousUserException
from project.models.Project import Project
from project.models.ProjectVisitor import ProjectVisitor
from interest.models.InterestedProject import InterestedProject


## @class InterestedProjectView 关注项目视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	关注项目视图
#   method:POST DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/22 11:20:10|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class InterestedProjectView(View):
    def __init__(self):
        pass

    """
        @apiName interestedProjectPost
        @api {POST} /teamwork/star/projects/ Interested: Post Interseted Project
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [关注项目管理]新建关注项目
        @apiParam {Int} project_id 新增关注项目id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        projectID = request.POST.get('project_id')

        # 2 校验参数
        try:
            projectID = int(projectID)
        except Exception:
            raise ParaPatternException(error=u'请输入正确的项目id')

        try:
            projectObj = Project.objects.get(id=projectID)
        except Exception:
            raise ParaMissingException(error=u'当前项目不存在')
        
        if projectObj.secret and ProjectVisitor.objects.filter(project=projectObj, visitor=user).count() == 0:
            raise AuthenticationException(error=u'当前项目不可见')

        # 3 存储数据
        if InterestedProject.objects.filter(project=projectObj, user=user).count() != 0:
            pass
        else:
            InterestedProject.objects.create(
                    project=projectObj,
                    user=user,
                    )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }
        return JsonResponse(res)


    """
        @apiName interestedProjectDelete
        @api {DELETE} /teamwork/star/projects/ Interested: Delete Interested Project
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [关注项目管理]删除关注项目
        @apiParam {Int} project_id 取消关注的项目的id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request):
        # 1 获取参数
        user = request.user
        projectID = request.GET.get('project_id')

        # 2 校验参数
        try:
            projectID = int(projectID)
        except Exception:
            raise ParaPatternException(error=u'请输入正确的项目id')

        try:
            InterestedObj = InterestedProject.objects.get(project_id=projectID, user=user)
        except Exception:
            raise ParaMissingException(error=u'用户未关注请求项目')

        # 3 删除项目
        InterestedObj.delete()

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }

        return JsonResponse(res)
