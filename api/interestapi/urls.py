from django.urls import re_path, path
from django.views.decorators.csrf import csrf_exempt
from api.interestapi.InterestedProjectView import InterestedProjectView
from api.interestapi.InterestedTaskView import InterestedTaskView


# app_name = 'interest'


urlpatterns = [
        path('projects/', csrf_exempt(InterestedProjectView.as_view())),
        path('tasks/', csrf_exempt(InterestedTaskView.as_view())),
        ]
