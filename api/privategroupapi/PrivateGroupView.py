from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from user.models import User
from privategroup.models.PrivateGroup import PrivateGroup
from privategroup.models.PrivateGroupUser import PrivateGroupUser


## @class PrivateGroupView 私有分组视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	私有分组视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/11 15:59:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class PrivateGroupView(View):
    def __init__(self):
        pass

    """
        @apiName privateGroupGet
        @api {GET} /teamwork/privategroup/ Get Private Group
        @apiGroup PrivateGroup
        @apiVersion 0.0.1
        @apiDescription [私有分组管理]获取自定义的私有分组
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Json} privategroup 私有分组信息列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                 {
                 "groupid": 1,
                 "groupname": '分组一',
                 "groupuser": [11, 12, 13]
                 }
            ]
        }
    """
    def get(self, request):
        # 1 获取用户
        user = request.user
        user_id = user.id

        # 2 校验用户
        if user_id == None or user_id == '':
            raise AnonymousUserException(error=u'用户未登录')

        # 3 获取分组信息
        privateGroups = PrivateGroup.objects.filter(user=user)
        privateGroupList = list()
        for privateGroup in privateGroups:
            groupInfo = {
                    "privategroupid": privateGroup.id,
                    "privategroupname": privateGroup.name,
                    }
            # 获取分组内用户id
            privategroupuser = PrivateGroupUser.objects.filter(privategroup=privateGroup).values_list('user', flat=True)
            groupInfo['privategroupuser'] = list(privategroupuser)
            privateGroupList.append(groupInfo)

        # 4 返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": privateGroupList
                }

        return JsonResponse(res)

    """
        @apiName privateGroupPost
        @api {POST} /teamwork/privategroup/ Post Private Group
        @apiGroup PrivateGroup
        @apiVersion 0.0.1
        @apiDescription [私有分组管理]新建私有分组
        @apiParam {str} name 私有分组名称
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} groupmsg 分组信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 14,
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        groupName = request.POST.get('name')

        # 2 校验参数
        if not user.id:
            raise AnonymousUserException(error=u'用户未登录')
        elif not user.is_active:
            raise AuthenticationException(error=u'当前用户没有操作权限')

        if not groupName:
            raise ParaMissingException(error=u'分组名不能为空')

        # 3 新建分组
        privateGroupObj = PrivateGroup.objects.filter(name=groupName, user=user).first()
        if privateGroupObj == None:
            privateGroupObj = PrivateGroup.objects.create(
                                                        user=user,
                                                        name=groupName,
                                                        )
        groupid = privateGroupObj.id

        # 4 返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": groupid, 
                }

        return JsonResponse(res)


    """
        @apiName privateGroupDelete
        @api {DELETE} /teamwork/privategroup/ Delete Private Group
        @apiGroup PrivateGroup
        @apiVersion 0.0.1
        @apiDescription [私有分组管理]删除私有分组
        @apiParam {int} group_id 私有分组id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 5,
        }
    """
    def delete(self, request):
        # 1 获取参数
        user = request.user
        user_id = user.id
        group_id = request.GET.get('groupid')

        # 2 校验参数
        if user_id == None or user_id == '':
            raise AnonymousUserException(error=u'用户未登录')

        try:
            group_id = int(group_id)
        except Exception:
            raise ParaPatternException(error=u'group_id格式错误')

        privateGroup = PrivateGroup.objects.filter(id=group_id, user=user)
        if privateGroup.count == 0:
            raise AuthenticationException(error=u'分组不存在')

        # 3 删除分组
        num = privateGroup.first().delete()[0]

        # 4 返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": num,
                }

        return JsonResponse(res)
