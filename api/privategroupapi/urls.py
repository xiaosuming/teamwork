from django.conf.urls import url
from django.urls import re_path, path
from django.views.decorators.csrf import csrf_exempt
from api.privategroupapi.PrivateGroupView import PrivateGroupView
from api.privategroupapi.PrivateGroupUserView import PrivateGroupUserView


app_name = 'privategroup'


urlpatterns = [
        re_path('^$', csrf_exempt(PrivateGroupView.as_view())),
        path('<int:group_id>/', csrf_exempt(PrivateGroupUserView.as_view())),
        ]
