from django.views.generic import View
from django.http import JsonResponse
from django.contrib.auth.models import Group
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.AnonymousUserException import AnonymousUserException
import re


## @class GroupsView 公共用户组视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	用户信息视图
#   method:GET POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/17 11:27:10|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class GroupsView(View):
    def __init__(self):
        pass

    """
        @apiName groupsGet
        @api {GET} /teamwork/groups/ Get Groups
        @apiGroup Group
        @apiVersion 0.0.1
        @apiDescription [公共用户组管理]获取所有公共用户组
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {List} data 公共用户组信息列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'groupid': 1,
                'groupname': 'all_user',
                'groupuser': [1, 2, 3,],
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取用户
        user = request.user
        
        # 2 校验用户信息
        if not user.id:
            raise AnonymousUserException(error=u'用户未登录')
        elif not user.is_active:
            raise AuthenticationException(error=u'没有访问权限')

        # 3 获取全部分组信息
        groups = Group.objects.all()
        groupList = list()
        for group in groups:
            user_id = list(group.user_set.all().values_list('id', flat=True))
            groupList.append(
                    {
                        'groupid': group.id,
                        'groupname': group.name,
                        'groupuser': user_id,
                    }
                    )


        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': groupList,
                }
        return JsonResponse(res)


    """
        @apiName groupsPost
        @api {POST} /teamwork/groups/ Post Group
        @apiGroup Group
        @apiVersion 0.0.1
        @apiDescription [公共用户组管理]新建公共用户组，超级用户可用
        @apiParam {String} name 名称
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Int} data 公共用户组id
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 3,
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        groupName = request.POST.get('name')

        # 2 校验参数
        if not user.id:
            raise AnonymousUserException(error=u'用户未登录')
        elif not user.is_superuser and user.is_active:
            raise AuthenticationException(error=u'当前用户没有操作权限')

        if not groupName:
            raise ParaMissingException(error=u'分组名不能为空')

        # 3 新建分组
        group = Group.objects.create(name = groupName)
        groupID = group.id

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': groupID,
                }
        return JsonResponse(res)
