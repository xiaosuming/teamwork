from django.views.generic import View
from django.http import JsonResponse
from django.contrib.auth.models import Group
from django.db import transaction
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.filt_valid_id import filt_valid_id
from user.models import User
import re


## @class GroupView 公共用户组内用户视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	用户信息视图
#   method:POST DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/17 11:27:10|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class GroupView(View):
    def __init__(self):
        pass

    """
        @apiName groupPost
        @api {POST} /teamwork/groups/id/ Post Group
        @apiGroup Group
        @apiVersion 0.0.1
        @apiDescription [公共用户组用户管理]修改所有公共用户组用户
        @apiParam {List} user_id 需要包含在组内的所有用户id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request, group_id):
        # 1 获取用户
        user = request.user
        groupID = group_id
        userID = request.POST.get('user_id')
        
        # 2 校验用户信息
        if not user.id:
            raise AnonymousUserException(error=u'用户未登录')
        elif not (user.is_superuser and user.is_active):
            raise AuthenticationException(error=u'没有操作权限')

        try:
            group = Group.objects.get(id=groupID)
        except Exception:
            raise ParaPatternException(error=u'请求的分组不存在')

        try:
            userIDs = filt_valid_id(eval(userID))[0]
        except Exception:
            raise ParaPatternException(error=u'用户id列表不正确')

        # 3 添加用户值分组
        with transaction.atomic():
            # 获取组内原有用户
            originalIDs = group.user_set.all().values_list('id', flat=True)
            # 获取删去的用户
            delIDs = set(originalIDs).difference(set(userIDs))
            # 删除用户
            for userID in delIDs:
                invalidUser = User.objects.get(id=userID)
                group.user_set.remove(invalidUser)
           
           # 获取新增的用户
            addIDs = set(userIDs).difference(set(originalIDs))
            # 添加用户
            for userID in addIDs:
                validUser = User.objects.get(id=userID)
                group.user_set.add(validUser)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }
        return JsonResponse(res)


    """
        @apiName groupDelete
        @api {DELETE} /teamwork/groups/id/ Delete Group
        @apiGroup Group
        @apiVersion 0.0.1
        @apiDescription [公共用户组用户管理]删除公共用户组用户
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request, group_id):
        # 1 获取参数
        user = request.user
        groupID = group_id

        # 2 校验参数
        if not user.id:
            raise AnonymousUserException(error=u'用户未登录')
        elif not (user.is_superuser or user.is_active):
            raise AuthenticationException(error=u'当前用户没有操作权限')

        try:
            group = Group.objects.get(id=groupID)
        except Exception:
            raise ParaPatternException(error=u'请求的分组不存在')

        # 3 删除分组，分组用户会级联删除
        group.delete()

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }
        return JsonResponse(res)
