from django.conf.urls import url
from django.urls import re_path, path
from django.views.decorators.csrf import csrf_exempt
from api.groupapi.GroupsView import GroupsView
from api.groupapi.GroupView import GroupView


app_name = 'group'


urlpatterns = [
        re_path('^$', csrf_exempt(GroupsView.as_view())),
        path('<int:group_id>/', csrf_exempt(GroupView.as_view())),
        ]
