from django.urls import path, re_path
from django.views.decorators.csrf import csrf_exempt
from api.tagapi.TaskTagView import TaskTagView


app_name = 'tag'


urlpatterns = [
        path('tasks/', csrf_exempt(TaskTagView.as_view())),
        ]
