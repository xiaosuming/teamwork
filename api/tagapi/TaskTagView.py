from django.views.generic import View
from django.http import JsonResponse
from django.db.models import Q
from project.models.ProjectParticipant import ProjectParticipant
from task.models.TaskParticipant import TaskParticipant

# @class TaskTagView 可见任务名称视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	获取可见总结视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/31 09:49:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskTagView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName taskTagGet
        @api {GET} /teamwork/tags/tasks/ TaskTag: Get All Task Tags
        @apiGroup Tag
        @apiVersion 0.0.1
        @apiDescription [标签管理]获取参与的任务和项目的标签
        @apiParam {String} search 搜索信息
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 可见任务的标签列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'projectID': 1,
                'projectName': '团队协作',
                'tasks':[
                    {"task_id": 1, "task_name": '日报界面复原'},
                ],
                },
                {
                'projectID': None,
                'projectName': None,
                'tasks':[
                    {"task_id": 23, "task_name": '私人任务1'},
                ],
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        user = request.user
        search = request.GET.get('search')

        # 2 获取返还数据
        res_data = list()

        # 2.1 获取参与的任务字典
        task_parts = TaskParticipant.objects.filter(participant=user)
        if search:
            task_parts = task_parts.filter(
                Q(task__name__contains=search) |
                Q(task__describe__contains=search)
            )

        task_dict = dict()
        for task_part in task_parts:
            task = task_part.task
            # 根据关联项目id构建任务字典，独立任务的键值为'HNP'
            try:
                related_project_id = str(task.project_id)
            except AttributeError:
                related_project_id = 'HNP'

            if related_project_id in task_dict:
                task_dict[related_project_id].append(
                    {"task_id": task.id, "task_name": task.name}
                )
            else:
                task_dict[related_project_id] = [{"task_id": task.id, "task_name": task.name}]

        # 2.2 获取参与的项目
        project_parts = ProjectParticipant.objects.filter(participant=user)
        if search:
            project_parts = project_parts.filter(
                Q(project__name__contains=search) |
                Q(project__describe__contains=search)
            )
        for project_part in project_parts:
            project = project_part.project
            res = {
                'project_id': project.id,
                'project_name': project.name,
                'tasks': task_dict.get(str(project.id), None),
            }
            task_dict.pop(str(project.id), None)
            res_data.append(res)

        # 2.3 将剩余的独立任务加入返还列表
        independent_task = {
            'project_id': None,
            'project_name': None,
            'tasks': list(),
        }

        for key in task_dict:
            tasks = task_dict[key]
            for task in tasks:
                independent_task['tasks'].append(task)

        res_data.append(independent_task)

        # 3 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': res_data,
        }

        return JsonResponse(res)
