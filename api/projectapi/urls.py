from django.urls import path, re_path
from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import url
from api.projectapi.ProjectsView import ProjectsView
from api.projectapi.ProjectView import ProjectView
from api.projectapi.ProjEnclosuresView import ProjEnclosuresView
from api.projectapi.ProjCommentsView import ProjCommentsView
from api.projectapi.ProjectStateView import ProjectStateView
from api.projectapi.ProjectSupervisorView import ProjectSupervisorView
from api.projectapi.ProjectRecordView import ProjectRecordView
from api.projectapi.ProjectProgressView import ProjectProgressView

app_name = 'project'


urlpatterns = [
        path('<int:project_id>/', csrf_exempt(ProjectView.as_view())),
        path('enclosures/', csrf_exempt(ProjEnclosuresView.as_view())),
        url(r'^(?P<project_id>\d+)/comments/', csrf_exempt(ProjCommentsView.as_view())),
        re_path('^$', csrf_exempt(ProjectsView.as_view())),
        url(r'^(?P<project_id>\d+)/state/$', csrf_exempt(ProjectStateView.as_view())),
        url(r'^(?P<project_id>\d+)/supervisor/$', csrf_exempt(ProjectSupervisorView.as_view())),
        url(r'^(?P<project_id>\d+)/records/$', csrf_exempt(ProjectRecordView.as_view())),
        url(r'^(?P<project_id>\d+)/progress/$', csrf_exempt(ProjectProgressView.as_view())),
]
