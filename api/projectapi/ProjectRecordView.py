from django.views.generic import View
from django.http.response import JsonResponse
from project.models.Project import Project
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.parse_record import parse_record
from api.mixin.ProjectMixin import ProjectMixin

# @class ProjectRecordView 项目操作记录视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/09 20:08:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjectRecordView(View, ProjectMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName project_record_get
        @api {GET} /teamwork/projects/{{project_id}}/records/ Records: Get Project Records
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [任务管理]获取项目操作记录
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "class": "project",
                    "name": "可怜无数山",
                    "created_time": "2019-10-14 14:17:20.800203",
                    "user": "李寻欢",
                    "record": "创建了本项目"
                },
            ]
        }
    """
    def get(self, request, project_id):
        # 1.获取参数
        project_id = int(project_id)

        # 2.校验参数
        user = request.user
        try:
            project = Project.objects.get(id=project_id)
        except Exception:
            raise UnknowResourcesException(error=u"请求的任务不存在")
        if project_id not in self.availabel_project(user):
            raise AuthenticationException(error=u"没有访问权限")

        # 3.获取任务记录
        records = parse_record(project, task=False)

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": records
        }
        return JsonResponse(res)
