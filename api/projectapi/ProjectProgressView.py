from django.views.generic import View
from django.http.response import JsonResponse
from user.models import User
from project.models.Project import Project
from task.models.SubTask import SubTask
from task.models.TaskParticipant import TaskParticipant
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from api.mixin.ProjectMixin import ProjectMixin
from api.mixin.TaskMixin import TaskMixin

# @class ProjectProgressView 项目进展视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/17 16:37:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjectProgressView(View, ProjectMixin, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName project_progress_get
        @api {GET} /teamwork/projects/{{project_id}}/progress/ Progress: Get Project Progress
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [任务管理]获取项目进展
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "user": "张三",
                    "sub_task": [
                        "state": "未完成",
                        "describe": "子任务一" 
                    ],
                },
            ]
        }
    """
    def get(self, request, project_id):
        # 1.获取参数
        user = request.user
        project_id = int(project_id)

        # 2.校验参数
        try:
            project = Project.objects.get(id=project_id)
        except Exception:
            raise UnknowResourcesException(error=u"请求的任务不存在")
        if project_id not in self.availabel_project(user):
            raise AuthenticationException(error=u"没有访问权限")

        # 3.获取子任务记录
        progress = list()
        task_ids = project.attached_task.filter(id__in=self.availabel_task(user))
        sub_tasks = SubTask.objects.filter(task__in=task_ids)
        participant_relations = TaskParticipant.objects.filter(subtask__in=sub_tasks)
        participants = set(User.objects.filter(taskparticipant__in=participant_relations))
        for participant in participants:
            exec('sub_%s = {"user": participant.username, "sub_task": []}' % participant.id)
            exec('progress.append(sub_%s)' % participant.id)

        for sub_task in sub_tasks:
            info = {
                "state": sub_task.get_state_display(),
                "describe": sub_task.describe
            }
            exec('sub_%s["sub_task"].append(info)' % sub_task.participant.participant.id)

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": progress
        }
        return JsonResponse(res)
