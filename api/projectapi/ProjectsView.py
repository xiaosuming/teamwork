from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.filt_valid_id import filt_valid_id
from utils.parse_brief import parse_brief
from user.models import User
from project.models.Project import Project
from project.models.ProjectRecord import ProjectRecord
from project.models.ProjectParticipant import ProjectParticipant
from project.models.ProjectVisitor import ProjectVisitor
from api.agendaapi.EventsView import set_event
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import datetime

# @class ProjectsView 项目视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/12 16:27:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjectsView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName projects_get
        @api {GET} /teamwork/projects/ Projects: Get Available Projects
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目管理]获取可访问项目列表
        @apiParam {String} state 项目进展 '进行中', '已完成'
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目简述列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'id': 1,
                'name': 团队协作项目,
                'start_time': "2019-10-15",
                'end_time': "2019-11-11",
                'director': '李大嘴',
                'supevisor': '方大同',
                'state': '验收中',
                'secret': True,
                'project_class': '软件开发',
                'star': True,
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取用户
        user = request.user
        project_state = request.GET.get('state')

        # 3 获取用户可见项目
        # 3.1 获取用户可见的私密任务
        secret_projects = list(ProjectVisitor.objects.filter(visitor_id=user.id).values_list('project', flat=True))
        # 3.2 获取所有可见项目
        visible_projects = Project.objects.filter(
            Q(secret=False) | Q(secret=True, id__in=secret_projects)
        ).order_by('created_time')
        if project_state == '已完成':
            visible_projects = visible_projects.filter(state='c')
        elif project_state == '未完成':
            visible_projects = visible_projects.exclude(state='c')
        # 3.3 获取项目简讯
        brief_list = parse_brief(visible_projects, user)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': brief_list,
                }
        return JsonResponse(res)

    """
        @apiName projects_post
        @api {POST} /teamwork/projects/ Projects: Create new Projects
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目管理]新建项目
        @apiParam {String} name 项目名称
        @apiParam {Date} start_time 开始时间 2019-10-12
        @apiParam {Date} end_time 结束时间 2019-11-11
        @apiParam {Int} supervisor_id 审核人id
        @apiParam {String} describe 项目描述
        @apiParam {String} project_class 项目类型
        @apiParam {Bool} secret 是否私密项目
        @apiParam {List} visitor_id 可见用户id
        @apiParam {List} participant_id 参与人id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 1,
        }
    """
    def post(self, request):
        # 1 获取参数
        director = request.user
        project_name = request.POST.get('name')
        start_time = request.POST.get('start_time')
        end_time = request.POST.get('end_time')
        supervisor_id = request.POST.get('supervisor_id')
        describe = request.POST.get('describe')
        project_class = request.POST.get('project_class')
        secret = request.POST.get('secret')
        visitor_ids = request.POST.get('visitor_id')
        participant_ids = request.POST.get('participant_id')

        # 2 校验参数
        if not director.id:
            raise AnonymousUserException()

        if not project_name:
            raise ParaMissingException(error=u'项目名不能为空')

        if not start_time:
            raise ParaMissingException(error=u'开始时间不能为空')
        try:
            start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d')
        except ValueError:
            raise ParaPatternException(error=u"开始时间格式应为'YYYY-mm-dd'")

        if not end_time:
            raise ParaMissingException(error=u'结束时间不能为空')
        try:
            end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        except ValueError:
            raise ParaPatternException(error=u"结束时间格式应为'YYYY-mm-dd'")

        if end_time < start_time:
            raise ParaPatternException(error=u'结束时间不能早于开始时间')
        if end_time < datetime.datetime.now():
            raise ParaPatternException(error=u'结束时间不能早于当前时间')

        if not supervisor_id:
            raise ParaMissingException(error=u'审核人id不能为空')
        try:
            supervisor = User.objects.get(id=supervisor_id, is_active=1)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请输入正确的审核人id')

        if project_class not in ('d',):
            raise ParaPatternException(error=u'项目类型必须为...')

        if not secret:
            raise ParaMissingException(error=u"sercret值必须为BOOL类型")
        elif secret == 'false':
            secret = False
        elif secret == 'true':
            secret = True

        if not participant_ids:
            participant_ids = []
        else:
            try:
                participant_ids = eval(participant_ids)
            except TypeError:
                raise ParaPatternException(error=u'参与人id格式错误')
            else:
                participant_ids.append(director.id)

        with transaction.atomic():
            # 3.1 新建项目
            project_obj = Project.objects.create(
                        director=director,
                        name=project_name,
                        start_time=start_time,
                        end_time=end_time,
                        supervisor=supervisor,
                        describe=describe,
                        project_class=project_class,
                        secret=secret,
                    )
            project_id = project_obj.id

            # 3.2 添加参与人
            participant_ids = filt_valid_id(participant_ids)[0]
            participant_names = filt_valid_id(participant_ids)[1]
            for participant_id in participant_ids:
                ProjectParticipant.objects.create(
                            project=project_obj,
                            participant_id=participant_id,
                        )
            participant_names = ';'.join(participant_names)    

            # 3.3 添加项目可见用户
            if secret:
                # 合并参与人和可见用户的ID
                if visitor_ids:
                    try:
                        visitor_ids = eval(visitor_ids)
                        visitor_ids = filt_valid_id(visitor_ids)[0]
                    except Exception:
                        raise ParaPatternException(error=u'visitor_id格式错误')
                    else:
                        visitor_ids = set(participant_ids + visitor_ids)
                else:
                    visitor_ids = set(participant_ids)

                visitor_ids.update({int(supervisor_id), int(director.id)})

                # 添加项目可见用户
                for visitor_id in visitor_ids:
                    ProjectVisitor.objects.create(
                            project=project_obj,
                            visitor_id=visitor_id,
                            )

            # 3.4 添加项目操作记录
            ProjectRecord.objects.create(
                    project=project_obj,
                    user=director,
                    record='创建了本项目'
                    )

            ProjectRecord.objects.create(
                    project=project_obj,
                    user=director,
                    record='添加参与人{}'.format(participant_names)
                    )

        # 3.5 通知在线的参与人
        channel_layer = get_channel_layer()
        for participant_id in participant_ids:
            async_to_sync(channel_layer.group_send)(
                    "notification_"+str(participant_id),
                    {
                        'type': 'get_notification',
                        'message': 'you have been invited to the project %s' % project_name,
                    }
                )
        # 3.6 通知审核人
        message = '%s had create a new project %s, please assess it in time' % (director.username, project_name)
        async_to_sync(channel_layer.group_send)(
                "notification_"+str(supervisor_id),
                {
                    'type': 'get_notification',
                    'message': message,
                }
            )

        # 3.7 添加入日程
        set_event(creator=director, content=project_name, start_time=start_time, 
                  expiration=end_time, repeat_type='d', remind_type='c',
                  remind_time=None, participant_ids=participant_ids, project_id=project_obj.id)

        # 4 返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": project_id,
                }
        return JsonResponse(res)
