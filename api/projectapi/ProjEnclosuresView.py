from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.FileMerge import FileManager
from project.models.Project import Project
from project.models.ProjectVisitor import ProjectVisitor
from project.models.ProjectEnclosure import ProjectEnclosure
from enclosure.models.Enclosure import Enclosure

# @class ProjEnclosuresView 项目附件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目附件视图
#   method:GET, POST, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/22 16:07:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjEnclosuresView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.file_manager = FileManager()

    """
        @apiName project_enclosures_get
        @api {GET} /teamwork/projects/enclosure1/ Enclosures: Get Project Enclosures
        @apiGroup Project
        @apiVersion 0.0.1
        @apiParam {Int} project_id 项目id
        @apiDescription [项目附件管理]获取全部项目附件列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'user': '李大嘴',
                'name': 'docker',
                'type': 'png',
                'size': '596928',
                'path': 'media/enclosure1/project/d00a2702d64e128b5b1ee86ffa2213cc',
                'created_time': '2019-10-22 22:00:12',
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        user = request.user,
        project_id = request.GET.get('project_id')

        # 2 校验参数
        try:
            project_id = int(project_id)
        except Exception:
            raise ParaPatternException(error=u'请输入正确的项目id')

        try:
            project_obj = Project.objects.get(id=project_id)
        except Exception:
            raise ParaMissingException(error=u'请求的项目不存在')

        if project_obj.secret and ProjectVisitor.objects.filter(
                project=project_id, visitor=user).count() == 0:
            raise AuthenticationException(error=u'请求的项目不可见')

        # 3 获取项目附件
        enclosures = list()
        for project_enclosure in ProjectEnclosure.objects.filter(project=project_obj):
            enclosure = project_enclosure.enclosure
            info = {
                'user': project_enclosure.user,
                'name': enclosure.name,
                'type': enclosure.file_type,
                'size': enclosure.size,
                'path': enclosure.path,
                'created_time': project_enclosure.created_time.strftime('%Y-%m-%d %H:%M:%S')
            }
            enclosures.append(info)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': enclosures,
                }
        return JsonResponse(res)

    """
        @apiName project_enclosures_post
        @api {POST} /teamwork/projects/enclosures/ enclosures: Post Project Enclosures
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目附件管理]添加项目附件
        @apiParam {Int} project_id 项目id
        @apiParam {String} name 文件名称
        @apiParam {Int} enclosure_id 附件id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件列表
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        project_id = request.POST.get('project_id')
        name = request.POST.get('name')
        enclosure_id = request.POST.get('enclosure_id')

        # 2 校验参数
        if not name:
            raise ParaMissingException(error=u'文件名为空')

        try:
            project_id = int(project_id)
        except Exception:
            raise ParaPatternException(error=u'参数格式错误')

        try:
            project_obj = Project.objects.get(id=project_id)
        except Exception:
            raise UnknowResourcesException(error=u'项目不存在')
        
        try:
            enclosure_id = int(enclosure_id)
        except ValueError:
            raise ParaPatternException(error=u"附件id必须为整数")
        if Enclosure.objects.filter(id=enclosure_id).count() == 0:
            raise UnknowResourcesException(error=u'附件不存在')

        # 3. 创建项目附件
        enclosure_obj = ProjectEnclosure.objects.create(
            user=user,
            name=name,
            project=project_obj,
            enclosure_id=enclosure_id,
        )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': enclosure_obj.id,
                }
        return JsonResponse(res)
