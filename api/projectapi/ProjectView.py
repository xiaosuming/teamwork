from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.filt_valid_id import filt_valid_id
from utils.parse_detail import parse_detail
from utils.update_project import update_project
from api.mixin.ProjectMixin import ProjectMixin
from project.models.Project import Project
import datetime
import json

# @class ProjectView 项目视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目视图
#   method:GET, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/14 16:27:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjectView(View, ProjectMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName project_get
        @api {GET} /teamwork/projects/{{id}}/ Project: Get Project Detail
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目管理]获取项目详情
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                'id': 1,
                'name': 团队协作项目,
                'start_time': "2019-10-15",
                'end_time': "2019-11-11",
                'user': '李大嘴',
                'supevisor': '方大同',
                'participant': ['张三', '里斯', '王五',]
                'state': '验收中',
                'secret': True,
                'project_class': '软件开发',
                'desc': '项目描述，描述项目...'，
            }
        }
    """
    def get(self, request, project_id):
        # 1 获取参数
        user = request.user
        project_id = int(project_id)

        # 2 校验参数
        if project_id not in self.availabel_project(user):
            raise AuthenticationException(error=u"没有访问权限")
        try:
            project_obj = Project.objects.get(id=project_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'项目不存在')

        # 3 获取项目详情
        detail = parse_detail(project_obj)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': detail,
                }
        return JsonResponse(res)

    """
        @apiName projectPost
        @api {PUT} /teamwork/projects/id/ Project: Update Project Details
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目管理]更新项目详情
        @apiParam {String} name 项目名称
        @apiParam {Date} start_time 开始时间 2019-10-12
        @apiParam {Date} end_time 结束时间 2019-11-11
        @apiParam {String} describe 项目描述
        @apiParam {String} project_class 项目类型
        @apiParam {Bool} secret 是否私密项目
        @apiParam {List} visitor_id 可见用户id
        @apiParam {List} participant_id 参与人id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 1,
        }
    """
    def put(self, request, project_id):
        # 1 获取参数
        user = request.user
        put_data = json.loads(request.body)
        project_name = put_data.get('name')
        start_time = put_data.get('start_time')
        end_time = put_data.get('end_time')
        describe = put_data.get('describe')
        project_class = put_data.get('project_class')
        secret = put_data.get('secret')
        visitor_ids = put_data.get('visitor_id')
        participant_ids = put_data.get('participant_id')
        project_id = int(project_id)

        project_type_choices = ['d']

        # 2 校验参数
        kwds = dict()
        if project_id not in self.can_apply_project(user):
            raise AuthenticationException(error=u"没有修改权限")
        try:
            project_obj = Project.objects.get(id=project_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'项目id必须为整数')

        if project_name:
            project_name = project_name.replace('"', '\"') 
            project_name = project_name.replace("'", '\'') 
            kwds['name'] = project_name

        if start_time:
            try:
                start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d').date()
            except Exception:
                raise ParaPatternException(error=u"开始时间格式应为'YYYY-mm-dd'")
            else:
                kwds['start_time'] = start_time

        if end_time:
            try:
                end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d').date()
            except Exception:
                raise ParaPatternException(error=u"结束时间格式应为'YYYY-mm-dd'")
            else:
                kwds['end_time'] = end_time

        if project_class:
            if project_class not in project_type_choices:
                raise ParaPatternException(error=u'项目类型必须为...')
            else:
                kwds['project_class'] = project_class

        if describe:
            describe = describe.replace('"', '\\\"')
            describe = describe.replace("'", "\\\'")
            kwds['describe'] = describe

        if participant_ids:
            try:
                participant_ids = filt_valid_id(participant_ids)[0]
            except Exception:
                raise ParaPatternException(error=u'请输入正确的参与人ID')
            else:
                kwds['participant_ids'] = participant_ids

        if visitor_ids:
            try:
                visitor_ids = filt_valid_id(visitor_ids)[0]
            except Exception:
                raise ParaPatternException(error=u'请输入正确的可见用户ID')
            else:
                visitor_ids = list(set(participant_ids+visitor_ids))
                kwds['visitor_ids'] = visitor_ids

        if secret is not None:
            if secret is False:
                kwds['secret'] = False
                kwds.pop('visitor_ids', None)
            elif secret is True:
                kwds['secret'] = True
            else:
                raise ParaPatternException(error=u'secret仅接受true或false')
        elif not project_obj.secret:
            kwds.pop('visitor_ids', None)

        # 3 更新项目信息
        update_project(user, project_obj, kwds)

        # 4 返回应答
        res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": project_id,
                }

        return JsonResponse(res)
