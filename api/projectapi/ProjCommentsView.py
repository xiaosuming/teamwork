from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from project.models.Project import Project
from project.models.ProjectParticipant import ProjectParticipant
from project.models.ProjectComment import ProjectComment
from project.models.ProjectVisitor import ProjectVisitor

# @class ProjComment 项目进展视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目进展视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/28 11:24:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjCommentsView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName projCommentsGet
        @api {GET} /teamwork/projects/{{project_id}}/comments/ Comments: Get Project Comments
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目进展管理]获取全部项目进展列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目进展列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'creater': '李大嘴',
                'seq': 12,
                'created_time': '2019-10-28 14:31:19',
                'content': '项目完成，功能完善',
                'type': '项目自述',
                'score': '5',
                },
            ]
        }
    """
    def get(self, request, project_id):
        # 1 获取参数
        user = request.user

        # 2 校验参数
        try:
            project_id = int(project_id)
        except Exception:
            raise ParaPatternException(error=u'请输入正确的项目id')

        try:
            project_obj = Project.objects.get(id=project_id)
        except Exception:
            raise ParaPatternException(error=u'请求项目不存在')

        if project_obj.secret:
            if ProjectVisitor.objects.filter(project=project_obj, visitor=user).count() == 0:
                raise AuthenticationException(error=u'请求项目不可见')

        if project_obj.supervisor == user:
            role = 'sup'
        else:
            role = None
        
        # 3 获取所有评论
        commentList = list()
        comments = ProjectComment.objects.filter(project=project_obj)
        id_list = list(comments.values_list("id", flat=True))
        id_list.insert(0, 1)
        for comment in comments:
            info = {
                'creater': comment.creater.username,
                'seq': id_list.index(comment.id),
                'created_time': comment.created_time.strftime('%Y-%m-%d %H:%M:%S'),
                'content': comment.content,
                'type': comment.get_comment_type_display(),
            }
            if info['type'] == '审核意见':
                if role: 
                    info['score'] = comment.score
                else:
                    info['score'] = None
            else:
                info['score'] = comment.score
            commentList.append(info)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': commentList,
        }

        return JsonResponse(res)


    """
        @apiName projCommentsPost
        @api {POST} /teamwork/projects/{{project_id}}/comments/ Comments: Create Project Comments
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目进展管理]提交项目进展
        @apiParam {Int} project_id 项目id
        @apiParam {Int} score 评分
        @apiParam {String} content 内容
        @apiParam {String} type 进展类型
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
        }
    """
    def post(self, request, project_id):
        # 1 获取参数
        user = request.user
        project_id = int(project_id)
        content = request.POST.get('content')
        score = request.POST.get('score')
        commentType = request.POST.get('type')
        COMMENT_TYPE = ('s', 'a', 'o')
        
        # 2 校验参
        try:
            project_obj = Project.objects.get(id=project_id)
        except Exception:
            raise ParaMissingException(error=u'项目不存在')

        if project_obj.secret:
            if ProjectVisitor.objects.filter(project=project_obj, visitor=user).count() == 0:
                raise AuthenticationException(error=u'请求项目不可见')

        if not commentType in COMMENT_TYPE:
            raise ParaPatternException(error=u'请输入正确的进展类型')
        elif commentType == 's' and project_obj.director != user:
            raise ParaPatternException(error=u'仅负责人可以提交项目自评')
        elif commentType == 'a' and project_obj.supervisor != user:
            raise ParaPatternException(error=u'仅审核人可以提交审核意见')

        if content == None or content == '':
            raise ParaMissingException(error=u'进展内容为空')

        try:
            score = int(score)
            print('score:', score)
        except Exception:
            raise ParaPatternException(error=u'评分只能为整数或None')

        if commentType != 'o':
            if score in range(0, 5):
                score = str(score)
            else:
                raise ParaPatternException(error=u'请输入5以内的整数评分')
        else:
            score = None
        
        # 3.1 存储数据
        ProjectComment.objects.create(
                project=project_obj,
                creater=user,
                score=score,
                content=content,
                comment_type=commentType,
                )

        # 3.2 通知项目参与人
        projectParticipant = ProjectParticipant.objects.filter(project=project_obj).values_list('participant', flat=True)
        projectParticipant = list(projectParticipant)
        projectParticipant.append(project_obj.supervisor.id)

        for id in projectParticipant:
            print(id)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
        }

        return JsonResponse(res)
