from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from project.models.Project import Project
from project.models.ProjectRecord import ProjectRecord
from user.models import User
from api.mixin.ProjectMixin import ProjectMixin
import json

# @class ProjectView 项目视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/14 16:27:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjectSupervisorView(View, ProjectMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName project_supervisor_put
        @api {PUT} /teamwork/projects/{{project_id}}/supervisor/ State: Update Project Supervisor
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目管理]更改项目审核人
        @apiParam {Int} supervisor_id 审核人id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "supervisor_name": "张三"
            }
        }
    """
    def put(self, request, project_id):
        # 1 获取参数
        user = request.user
        put_data = json.loads(request.body)
        supervisor_id = put_data.get("supervisor_id")
        project_id = int(project_id)

        # 2 校验参数
        if project_id not in self.can_verify_project(user):
            raise AuthenticationException(error=u"没有操作权限")
        try:
            project_obj = Project.objects.get(id=project_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"项目不存在")
        try:
            supervisor = User.objects.get(id=supervisor_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的审核人不存在")

        # 3.1. 修改审核人
        project_obj.supervisor = supervisor
        project_obj.save()
        # 3.2.添加项目记录
        ProjectRecord.objects.create(
            user=user,
            record=u"{}将项目审核人替换为{}".format(user.username, supervisor.username),
            project=project_obj,
        )

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': {
                "supervisor_name": supervisor.username
            },
        }

        return JsonResponse(res)
