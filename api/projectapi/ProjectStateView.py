from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from project.models.Project import Project
from project.models.ProjectRecord import ProjectRecord
from agenda.models.Event import Event
from api.mixin.ProjectMixin import ProjectMixin
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import json
import datetime

# @class ProjectView 项目视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/14 16:27:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ProjectStateView(View, ProjectMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName project_state_put
        @api {PUT} /teamwork/projects/{{project_id}}/state/ State: Update Project State
        @apiGroup Project
        @apiVersion 0.0.1
        @apiDescription [项目管理]更新项目
        @apiParam {String} state 项目状态' ['a', 'd', 'i', 'c', 's']
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": '进行中',
        }
    """
    def put(self, request, project_id):
        # 1 获取参数
        user = request.user
        put_data = json(request.body)
        state = put_data.get('state')
        state_choices = ['a', 'o', 'i', 'c', 's']
        project_id = int(project_id)

        # 2 校验参数
        try:
            project_obj = Project.objects.get(id=project_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"项目不存在")
        if state not in state_choices:
            raise ParaPatternException(error=u'无效的项目状态')

        # 3 修改状态
        # 通过项目审核, 状态改为进行中
        if project_obj.state == 'a' and state == 'o':
            if project_id not in self.can_verify_project(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                project_obj.state = state
                project_obj.save()
                ProjectRecord.objects.create(
                    project=project_obj,
                    user=user,
                    record=u'项目通过审核'
                )
        # 提交项目验收申请
        if project_obj.state == 'o' and state == 'i':
            if project_id not in self.can_apply_project(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                project_obj.state = state
                project_obj.save()
                ProjectRecord.objects.create(
                    project=project_obj,
                    user=user,
                    record=u'{}提交项目验收'.format(user.username)
                )
            # 通知审核人
            message = u'%s 申请项目"%s"完成验收' % user.username, project_obj.name
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                "notification_" + str(project_obj.supervisor_id),
                {
                    'type': 'get_notification',
                    'message': message,
                }
            )
        # 驳回项目验收申请
        elif project_obj.state == 'i' and state == 'o':
            if project_id not in self.can_verify_project(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                project_obj.state = state
                project_obj.save()
                ProjectRecord.objects.create(
                    project=project_obj,
                    user=user,
                    record=u'{}驳回项目验收申请'.format(user.username)
                )
        # 验收通过
        elif project_obj.state == 'i' and state == 'c':  # 审核通过
            if project_id not in self.can_verify_project(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                project_obj.state = state
                project_obj.save()
                ProjectRecord.objects.create(
                    project=project_obj,
                    user=user,
                    record=u'{}通过项目审核'.format(user.username)
                )
                # 删除当前日期后的日程
                Event.objects.filter(project_id=project_id, start_time__gt=datetime.date.today()).delete()
        elif state == 's':  # 取消项目
            if project_id not in self.can_suspend_project(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                project_obj.state = state
                project_obj.save()
                ProjectRecord.objects.create(
                    project=project_obj,
                    user=user,
                    record=u'{}取消项目'.format(user.username)
                )
                # 删除日程
                Event.objects.filter(project_id=project_id).delete()
        else:
            raise AuthenticationException(error=u"非法操作")

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': project_obj.get_state_display(),
                }

        return JsonResponse(res)
