from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.filt_valid_id import filt_valid_id
from utils.parse_detail import parse_detail
from utils.update_task import update_task
from user.models import User
from project.models.ProjectParticipant import ProjectParticipant
from task.models.Task import Task
import datetime
from api.mixin.TaskMixin import TaskMixin
import json

# @class TaskView 任务视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/16 10:54:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_get
        @api {GET} /teamwork/tasks/{{task_id}}/ Task: Get Task Detail
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]获取任务详情
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                'id': 1,
                'name': "团队协作使用帮助",
                'start_time': "2019-10-15",
                'end_time': "2019-11-11",
                'describe': '任务描述，描述任务...'，
                'secret': True,
                'director': '李大嘴',
                'supevisor': '方大同',
                'participant': ['张三', '里斯', '王五',]
                'state': '待审核',
                'task_type': '普通任务',
                'rate': '紧急',
                'project': (1, '团队协作项目'),
                'subtask': [
                    {
                    'participant': '张三'，
                    'brief': '这是一个子任务',
                    'end_time': '2019-10-18',
                    'state': '进行中',
                    },
                ]
            }
        }
    """
    def get(self, request, task_id):
        # 1 获取参数
        user = request.user
        task_id = int(task_id)

        # 2 校验参数
        try:
            task_obj = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请求的任务不存在')

        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u'请求任务不可见')

        # 3 获取任务详情
        detail = parse_detail(task_obj, task=True)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': detail,
                }

        return JsonResponse(res)

    """
        @apiName task_put
        @api {PUT} /teamwork/tasks/{{task_id}}/ Task: Update the Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]更新任务内容
        @apiParam {Int} project_id 关联项目id
        @apiParam {String} name 任务标题
        @apiParam {Date} start_time 开始时间 2019-10-12
        @apiParam {Date} end_time 结束时间 2019-11-11
        @apiParam {Int} supervisor_id 审核人id
        @apiParam {String} describe 项目描述
        @apiParam {String} task_type 任务类型 (('o', '普通'),('t','测试'),('f','缺陷'),('d','设计'),('r','需求'),('e','其他'))
        @apiParam {String} rate 优先级 ['o', 'i', 'e', 'u']
        @apiParam {Bool} secret 是否私密项目
        @apiParam {List} visitor_id 可见用户id [15, 16, 17]
        @apiParam {List} participant_id 参与人id [1, 12, 14, 18]
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request, task_id):
        # 1 获取参数
        user = request.user
        put_data = json.loads(request.body)
        project_id = put_data.get('project_id')
        name = put_data.get('name')
        start_time = put_data.get('start_time')
        end_time = put_data.get('end_time')
        supervisor_id = put_data.get('supervisor_id')
        task_type = put_data.get('task_type')
        describe = put_data.get('describe')
        rate = put_data.get('rate')
        secret = put_data.get('secret')
        visitor_ids = put_data.get('visitor_id')
        participant_ids = put_data.get('participant_id')
        task_id = int(task_id)

        rate_class = ['o', 'i', 'e', 'u']
        task_type_class = ("o", "t", "d", "f", "r", "e")

        # 2 校验参数
        kwds = dict()
        try:
            task_obj = Task.objects.get(id=task_id, director=user)
        except Exception:
            raise AuthenticationException(error=u'当前用户无权访问指定任务')
        else:
            project_obj = task_obj.project

        if project_id:
            try:
                project_obj = ProjectParticipant.objects.filter(
                        project_id=int(project_id),
                        participant=user
                        ).first().project
                print()
            except Exception:
                raise ParaPatternException(error=u'请输入正确的项目id')
            else:
                kwds['project'] = project_obj

        if name:
            name = name.replace('"', '\"')
            name = name.replace("'", "\'")
            kwds['name'] = name

        if start_time:
            try:
                start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d').date()
            except Exception:
                raise ParaPatternException(error=u'开始时间格式应为%s' % "'YYYY--mm-dd'")
            else:
                kwds['start_time'] = start_time

        if end_time:
            try:
                end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d').date()
            except Exception:
                raise ParaPatternException(error=u'结束时间格式应为%s' % "'YYYY--mm-dd'")
            else:
                if project_obj and (end_time > project_obj.end_time):
                    raise ParaPatternException(error=u'结束时间不能晚与项目时间')
                kwds['end_time'] = end_time

        if supervisor_id:
            try:
                supervisor = User.objects.get(id=supervisor_id, is_active=1)
            except Exception:
                raise ParaPatternException(error=u'指定的审核人不存在')
            else:
                kwds['supervisor'] = supervisor

        if describe:
            describe = describe.replace('"', '\"')
            describe = describe.replace("'", "\'")
            kwds['describe'] = describe

        if task_type:
            if task_type in task_type_class:
                kwds['task_class'] = task_type
            else:
                raise ParaPatternException(error=u'task_type值非法')

        if rate:
            if rate in rate_class:
                kwds['rate'] = rate
            else:
                raise ParaPatternException(error=u'rate值非法')

        if participant_ids:
            try:
                participant_ids = filt_valid_id(eval(participant_ids))[0]
            except Exception:
                raise ParaPatternException(error=u'请输入正确的参与人ID')

        if visitor_ids:
            try:
                visitor_ids = filt_valid_id(eval(visitor_ids))[0]
            except Exception:
                raise ParaPatternException(error=u'请输入正确的可见用户ID')
            else:
                visitor_ids = list(set(participant_ids+visitor_ids))
                kwds['visitor_ids'] = visitor_ids

        if secret is not None:
            kwds['secret'] = False
            kwds.pop('visitor_ids', None)

        # 3 更新任务
        update_task(user, task_obj, kwds)

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }

        return JsonResponse(res)
