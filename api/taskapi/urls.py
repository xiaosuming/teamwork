from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from api.taskapi.TasksView import TasksView
from api.taskapi.TaskView import TaskView
from api.taskapi.TaskCommentsView import TaskCommentsView
from api.taskapi.SubTasksView import SubTasksView
from api.taskapi.SubTaskStateView import SubTaskStateView
from api.taskapi.TaskStateView import TaskStateView
from api.taskapi.TaskRecordView import TaskRecordView
from api.taskapi.SubTaskView import SubTaskView
from api.taskapi.TaskFiltersView import TaskFiltersView
from api.taskapi.TaskFilterView import TaskFilterView
from api.taskapi.CommentEnclosuresView import CommentEnclosuresView
from api.taskapi.TaskEnclosuresView import TaskEnclosuresView
from api.taskapi.RemindersView import RemindersView
from api.taskapi.ReminderView import ReminderView
from api.taskapi.TaskRebootView import TaskRebootView
from api.taskapi.TaskCommentView import TaskCommentView

app_name = 'task'


urlpatterns = [
    url(r'^$', csrf_exempt(TasksView.as_view())),
    url(r'^(?P<task_id>\d+)/$', csrf_exempt(TaskView.as_view())),
    url(r'^(?P<task_id>\d+)/state/$', csrf_exempt(TaskStateView.as_view())),
    url(r'^(?P<task_id>\d+)/records/$', csrf_exempt(TaskRecordView.as_view())),
    url(r'^(?P<task_id>\d+)/comments/$', csrf_exempt(TaskCommentsView.as_view())),
    url(r'^(?P<task_id>\d+)/sub/$', csrf_exempt(SubTasksView.as_view())),
    url(r'^sub/(?P<sub_id>\d+)/$', csrf_exempt(SubTaskView.as_view())),
    url(r'^sub/(?P<sub_id>\d+)/state/$', csrf_exempt(SubTaskStateView.as_view())),
    url(r'^filters/$', csrf_exempt(TaskFiltersView.as_view())),
    url(r'^filters/(?P<filter_id>\d+)/$', csrf_exempt(TaskFilterView.as_view())),
    url(r'^comments/(?P<comment_id>\d+)/enclosures/$', csrf_exempt(CommentEnclosuresView.as_view())),
    url(r'^(?P<task_id>\d+)/enclosures/$', csrf_exempt(TaskEnclosuresView.as_view())),
    url(r'(?P<task_id>\d+)/reminders/$', csrf_exempt(RemindersView.as_view())),
    url(r'(?P<task_id>\d+)/reminders/(?P<reminder_id>\d+)/$', csrf_exempt(ReminderView.as_view())),
    url(r'(?P<task_id>\d+)/reboot/$', csrf_exempt(TaskRebootView.as_view())),
    url(r'comments/(?P<comment_id>\d+)/$', csrf_exempt(TaskCommentView.as_view())),
]
