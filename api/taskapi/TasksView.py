from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.filt_valid_id import filt_valid_id
from utils.parse_brief import parse_brief
from user.models import User
from project.models.Project import Project
from project.models.ProjectParticipant import ProjectParticipant
from project.models.ProjectRecord import ProjectRecord
from task.models.Task import Task
from task.models.TaskRecord import TaskRecord
from task.models.TaskParticipant import TaskParticipant
from task.models.TaskVisitor import TaskVisitor
from task.models.TaskFilter import TaskFilter
from api.agendaapi.EventsView import set_event
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import datetime
import json

# @class TasksView 任务视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/15 14:13:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TasksView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName tasksGet
        @api {GET} /teamwork/tasks/ Tasks:Get Available Tasks
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]获取可见任务列表
        @apiParam {String} tasks_type 任务类型 
        (('a','all'), ('i', 'issued'), ('p', 'participated'), ('s', 'supervise')), 默认'a'
        @apiParam {Int} filter_id 过滤器id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 任务掠影列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'id': 1,
                'director': '张三',
                'name': 'Team Cooperation使用帮助',
                'ddl': '2019-10-15',
                'describe': '风生水起啊，每天就爱穷开心阿，逍遥的魂儿啊，假不正经啊',
                'state': '未完成',
                'star': True,
                'rate': '紧急'
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        user = request.user
        user_id = user.id
        tasks_type = request.GET.get('tasks_type', 'a')
        filter_id = request.GET.get("filter_id")

        # 2 校验用户
        if not tasks_type:
            raise ParaMissingException(error=u'请输入任务类型')

        if tasks_type not in ('a', 'i', 'p', 's'):
            raise ParaPatternException(error=u'不能识别的任务列表类型')

        try:
            task_filter = TaskFilter.objects.get(id=filter_id, creator=user)
        except ObjectDoesNotExist:
            task_filter = None

        # 3.1 获取全部可见私密任务id
        # 3.2 获取所有可见任务
        if tasks_type == 'i':
            visible_tasks = Task.objects.filter(director=user).order_by('created_time')
        elif tasks_type == 'p':
            participated_tasks = TaskParticipant.objects.filter(participant=user).values_list('task', flat=True)
            visible_tasks = Task.objects.filter(id__in=participated_tasks)
        elif tasks_type == 's':
            visible_tasks = Task.objects.filter(supervisor=user).order_by('created_time')
        else:
            secret_task_ids = TaskVisitor.objects.filter(visitor_id=user_id).values_list('task', flat=True)
            visible_tasks = Task.objects.filter(Q(secret=False) | Q(id__in=secret_task_ids)).order_by('created_time')
        # 3.3 筛选符合条件的任务
        if task_filter:
            visible_tasks = filter_task(visible_tasks, task_filter)
        # 3.4 获取任务摘要
        brief_list = parse_brief(visible_tasks, user, task=True)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': brief_list,
        }

        return JsonResponse(res)

    """
        @apiName tasksPost
        @api {POST} /teamwork/tasks/ Tasks: Create Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]新建任务
        @apiParam {Int} project_id 关联项目id
        @apiParam {String} name 任务标题
        @apiParam {Date} start_time 开始时间 2019-10-12
        @apiParam {Date} end_time 结束时间 2019-11-11
        @apiParam {Int} supervisor_id 审核人id
        @apiParam {String} describe 项目描述
        @apiParam {String} task_type 任务类型 (('o', '普通'),('t','测试'),('f','缺陷'),('d','设计'),('r','需求'),('e','其他'))
        @apiParam {String} rate 优先级 ['o', 'i', 'e', 'u']
        @apiParam {Bool} secret 是否私密任务
        @apiParam {List} visitor_id 可见用户id [15, 16, 17]
        @apiParam {List} participant_id 参与人id [1, 12, 14, 18]
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 新建任务id
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 4,
        }    
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        user_id = user.id
        project_id = request.POST.get('project_id')
        name = request.POST.get('name')
        start_time = request.POST.get('start_time')
        end_time = request.POST.get('end_time')
        supervisor_id = request.POST.get('supervisor_id')
        describe = request.POST.get('describe')
        task_type = request.POST.get('task_type')
        rate = request.POST.get('rate')
        secret = request.POST.get('secret')
        visitor_id = request.POST.get('visitor_id')
        participant_id = request.POST.get('participant_id')
        rate_class = ['o', 'i', 'e', 'u']
        task_type_class = ['o', 't', 'd', 'f', 'r', 'e']

        # 2 校验参数
        if not user_id:
            raise AnonymousUserException(error=u'用户未登录')

        if project_id:
            try:
                project_obj = Project.objects.get(id=project_id)
            except Exception:
                raise ParaPatternException(error=u'关联项目id非法')

            if project_obj.secret:
                if ProjectParticipant.objects.filter(
                        project=project_obj,
                        participant=user
                ).count() == 0:
                    raise ParaMissingException(error=u'关联项目不存在')
        else:
            project_obj = None

        if not name:
            raise ParaMissingException(error=u'任务名不能为空')

        try:
            start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d')
        except Exception:
            raise ParaPatternException(error=u'请输入正确格式的开始时间%s' % "'YYYY-mm-dd'")

        try:
            end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d')
        except Exception:
            raise ParaPatternException(error=u'请输入正确格式的结束时间%s' % "'YYYY-mm-dd'")
        else:
            if end_time < start_time or end_time < datetime.datetime.now():
                raise ParaPatternException(error=u'结束时间不能早于开始时间或当前时间')

        if project_obj:
            if project_obj.start_time > start_time.date():
                raise ParaPatternException(error=u'开始时间不能早于项目时间')
            elif project_obj.end_time < end_time.date():
                raise ParaPatternException(error=u'结束时间不能晚于项目时间')

        if not supervisor_id:
            supervisor = user
        else:
            try:
                supervisor = User.objects.get(id=int(supervisor_id), is_active=1)
            except ObjectDoesNotExist:
                raise ParaPatternException(error=u'审阅人id错误或审阅人非法')

        if not describe:
            raise ParaMissingException(error=u'任务描述不能为空')

        if task_type not in task_type_class:
            raise ParaPatternException(error=u'请输入正确的任务类型')

        if rate not in rate_class:
            raise ParaPatternException(error=u'请输入正确的优先级')

        if secret == 'True':
            secret = True
            if visitor_id:
                print('visitor_id:', visitor_id)
                try:
                    visitor_id = filt_valid_id(eval(visitor_id))[0]
                    print('visiorID:', visitor_id)
                except Exception:
                    raise ParaPatternException(error=u'请输入正确的可见用户id列表')
        else:
            secret = False

        if participant_id:
            try:
                participant_id, participant_name = filt_valid_id(eval(participant_id))
                if supervisor_id in participant_id:
                    raise ParaPatternException(error=u'审核人不能参与任务')
            except Exception:
                raise ParaPatternException(error=u'请输入正确的参与人id列表')

        # 3 新建任务
        with transaction.atomic():
            # 3.1.新建任务
            task_obj = Task.objects.create(
                name=name,
                start_time=start_time,
                end_time=end_time,
                describe=describe,
                secret=secret,
                director=user,
                supervisor=supervisor,
                project=project_obj,
                task_class=task_type,
                rate=rate,
            )
            # 3.2.添加参与人
            for user_id in participant_id:
                TaskParticipant.objects.create(
                    task=task_obj,
                    participant_id=user_id
                )
            # 3.3.添加记录
            TaskRecord.objects.create(
                user=user,
                task=task_obj,
                record=u'创建了本任务',
            )
            if project_obj:
                ProjectRecord.objects.create(
                    user=user,
                    project=project_obj,
                    record=u'添加了任务%s' % task_obj.name
                )
            if participant_id:
                TaskRecord.objects.create(
                    user=user,
                    task=task_obj,
                    record=u'添加参与人{}'.format(";".join(participant_name))
                )
            # 3.4.添加可见用户
            if secret:
                print(visitor_id + participant_id)
                print(set(visitor_id + participant_id))
                print({int(supervisor.id), user_id})

                print(set(visitor_id + participant_id + [supervisor.id, user_id]))
                visitor_ids = set(visitor_id + participant_id + [supervisor.id, user_id])
                for user_id in visitor_ids:
                    TaskVisitor.objects.create(
                        task=task_obj,
                        visitor_id=user_id
                    )

        channel_layer = get_channel_layer()
        # 3.5.通知审核人
        if supervisor_id and supervisor_id != user_id:
            message = u'%s had created a new task %s, please access it in time' % (user.username, name)
            async_to_sync(channel_layer.group_send)(
                "notification_" + str(supervisor_id),
                {
                    'type': 'get_notification',
                    'message': message,
                }
            )
        # 3.6.通知参与人
        for user_id in participant_id:
            async_to_sync(channel_layer.group_send)(
                "notification_" + str(user_id),
                {
                    'type': 'get_notification',
                    'message': 'you have been invited to the task %s' % name,
                }
            )

        # 3.7 添加入日程
        set_event(creator=user, content=name, start_time=start_time,
                  expiration=end_time, repeat_type='d', remind_type='c',
                  remind_time="00:00:00", participant_ids=participant_id,
                  task_id=task_obj.id
                  )

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': task_obj.id
        }

        return JsonResponse(res)


def filter_task(tasks, task_filter):
    """
    更据筛选器筛选任务列表中符合条件的任务
    :param tasks: 任务query_list
    :param task_filter: 筛选器
    :return: 符合条件任务
    """
    conditions = json.loads(task_filter.conditions)
    '''
    {
        "participant": [1, 2, 3],
        "supervisor": [2, 3],
        "creator": [2, 3],
        "project": [12, 23],
        "rate": ["o", "i"],
        "phase": ["started"]
    }
    '''
    participant = conditions["participant"]
    supervisor = conditions["supervisor"]
    creator = conditions["creator"]
    project = conditions["project"]
    rate = conditions["rate"]
    phase = conditions["phase"]

    parsed_tasks = tasks.filter(
        Q(director_id__in=creator) |
        Q(supervisor_id__in=supervisor) |
        Q(project_id__in=project) |
        Q(rate__in=rate) |
        Q(task_participant__participant_id__in=participant)
    )
    if "started" in phase:
        parsed_tasks = parsed_tasks.filter(
            start_time__lt=datetime.date.today()
        )
    if "terminated" in phase:
        parsed_tasks = parsed_tasks.filter(
            state__in=("c", "s")
        )

    return parsed_tasks
