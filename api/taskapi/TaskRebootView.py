from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from api.mixin.TaskMixin import TaskMixin
from task.models.Task import Task
from task.models.TaskRecord import TaskRecord
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

# @class TaskRebootView 任务重启视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/07 11:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskRebootView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_reboot
        @api {PUT} /teamwork/tasks/{{task_id}}/reboot/ Reboot: Reboot Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]任务重启
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request, task_id):
        # 1 获取参数
        user = request.user
        task_id = int(task_id)

        # 2 校验参数
        try:
            task_obj = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的任务不存在")
        if task_obj.supervisor_id != user.id:  # 仅审核人可以重启任务
            raise AuthenticationException(error="没有操作权限")
        if task_obj.state != 's':
            raise AuthenticationException(error=u"请求的任务正在进行中")

        # 3 修改状态
        with transaction.atomic():
            task_obj.state = 'o'
            task_obj.save()
            TaskRecord.objects.create(
                task=task_obj,
                user=user,
                record=u'{}重启任务'.format(user.username)
            )
        # 通知任务参与人
        message = u'%s重启了任务%s"完成审核' % (user.username, task_obj.name)
        channel_layer = get_channel_layer()
        for participant_id in self.task_participant(task_obj):
            async_to_sync(channel_layer.group_send)(
                "notification_" + str(participant_id),
                {
                    'type': 'get_notification',
                    'message': message,
                }
            )

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }

        return JsonResponse(res)
