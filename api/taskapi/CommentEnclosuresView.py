from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from task.models.TaskComment import TaskComment
from task.models.CommentEnclosure import CommentEnclosure
from enclosure.models.Enclosure import Enclosure

# @class CommentEnclosuresView 任务评论附件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目附件视图
#   method:GET, POST, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/03 20:07:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class CommentEnclosuresView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_c_enclosures_post
        @api {POST} /teamwork/tasks/comments/{{comment_id}}/enclosures/ CommentEnclosures: Attach Enclosure to Comment
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务评论附件]合并评论附件切片
        @apiParam {String} name 文件名称
        @apiParam {Int} enclosure_id 附件id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件列表
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
        }
    """
    def post(self, request, comment_id):
        # 1 获取参数
        user = request.user
        name = request.POST.get('name')
        enclosure_id = request.POST.get('enclosure_id')
        comment_id = int(comment_id)

        # 2 校验参数
        if not name:
            raise ParaMissingException(error=u'文件名为空')

        try:
            comment_obj = TaskComment.objects.get(id=comment_id)
        except Exception:
            raise UnknowResourcesException(error=u'评论不存在')

        try:
            enclosure_id = int(enclosure_id)
        except ValueError:
            raise ParaPatternException(error=u"附件id必须为整数")
        if Enclosure.objects.filter(id=enclosure_id).count() == 0:
            raise UnknowResourcesException(error=u'附件不存在')

        # 3 创建项目附件
        enclosure_obj = CommentEnclosure.objects.create(
            user=user,
            name=name,
            task_comment=comment_obj,
            enclosure_id=enclosure_id,
        )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': enclosure_obj.id,
                }
        return JsonResponse(res)
