from django.http.response import JsonResponse
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from task.models.TaskFilter import TaskFilter
from task.models.Task import Task
from user.models import User
from project.models.Project import Project
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
import json

# @class TaskFilterView 任务过滤器视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/03 14:02:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskFilterView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_filter_get
        @api {GET} /teamwork/tasks/filters/{{filter_id}}/ Filter: Get Filter Conditions
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]获取任务过滤器
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 过滤条件
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "participant": [
                    {"id": 3, "username": "张三"},
                ],
                "supervisor": [
                    {"id": 1, "username": "李四"},
                ],
                "creator": [
                    {"id": 3, "username": "张三"},
                ],
                "rate": [
                    {"code": "e", "name": "紧急"},
                ], 
                "project": [
                    {"id": 3, "name": "花间集"},
                ],
                "phase": ["started", "terminated"]
            }
        }
    """
    def get(self, request, filter_id):
        # 1.获取参数
        user = request.user
        rate_choices = dict(Task().RATE_CHOICES)
        filter_id = int(filter_id)

        # 2.获取数据
        try:
            task_filter = TaskFilter.objects.get(id=filter_id, creator=user)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的过滤器不存在")
        # 3.组织数据
        conditions = json.loads(task_filter.conditions)
        '''
        {
            "participant": [1, 2, 3],
            "supervisor": [2, 3],
            "creator": [2, 3],
            "project": [12, 23],
            "rate": ["o", "i"],
            "phase": ["started"]
        }
        '''
        participant = conditions["participant"]
        participant = list(
            User.objects.filter(id__in=participant).values("id", "username"))
        supervisor = conditions["supervisor"]
        supervisor = list(
            User.objects.filter(id__in=supervisor).values("id", "username"))
        creator = conditions["creator"]
        creator = list(
            User.objects.filter(id__in=creator).values("id", "username"))
        project = conditions["project"]
        project = list(
            Project.objects.filter(id__in=project).values("id", "name"))
        rates = conditions["rate"]
        rate = list()
        for char in rates:
            rate.append(
                {
                    "code": char,
                    "rate": rate_choices[char],
                }
            )
        res_data = {
            "participant": participant,
            "supervisor": supervisor,
            "creator": creator,
            "project": project,
            "rate": rate,
            "phase": conditions["phase"]
        }
        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": res_data,
        }
        return JsonResponse(res)

    """
        @apiName task_filter_delete
        @api {DELETE} /teamwork/tasks/filters/{{filter_id}}/ Filter: Delete Task Filter
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]删除任务过滤器
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request, filter_id):
        # 1.获取参数
        user = request.user
        filter_id = int(filter_id)

        # 2.校验参数
        try:
            task_filter = TaskFilter.objects.get(id=filter_id, creator=user)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的过滤器不存在")
        # 3.删除筛选器
        task_filter.delete()

        # 4.返回响应
        res = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res)
