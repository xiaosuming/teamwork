from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from api.mixin.TaskMixin import TaskMixin
from api.taskapi.SubTaskStateView import check_task_complete
from task.models.Task import Task
from task.models.TaskRecord import TaskRecord
from agenda.models.Event import Event
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import json
import datetime

# @class TaskView 任务视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/16 10:54:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskStateView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName taskPut
        @api {PUT} /teamwork/tasks/{{task_id}}/state/ TaskState: Update Task State
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]任务的审核与取消
        @apiParam {String} state 任务状态 ['o', 'i', 'c', 's'] 提交审核: i, 审核通过: c, 驳回审核: o, 取消任务: s
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request, task_id):
        # 1 获取参数
        user = request.user
        put_data = json.loads(request.body)
        state = put_data.get('state')
        state_class = ['o', 'i', 'c', 's']
        task_id = int(task_id)

        # 2 校验参数
        if state not in state_class:
            raise ParaPatternException(error=u'请输入正确的state')
        try:
            task_obj = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的任务不存在")

        # 3 修改状态
        if task_obj.state == 'o' and state == 'i':  # 提交任务审核
            if task_id not in self.can_apply_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                task_obj.state = state
                task_obj.save()
                TaskRecord.objects.create(
                    task=task_obj,
                    user=user,
                    record=u'{}提交任务审核'.format(user.username)
                )
            # 通知审核人
            message = u'%s 申请任务"%s"完成审核' % user.username, task_obj.name
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                "notification_" + str(task_obj.supervisor_id),
                {
                    'type': 'get_notification',
                    'message': message,
                }
            )
        elif task_obj.state == 'i' and state == 'o':  # 驳回任务审核
            if task_id not in self.can_verify_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                task_obj.state = state
                task_obj.save()
                TaskRecord.objects.create(
                    task=task_obj,
                    user=user,
                    record=u'{}驳回任务审核'.format(user.username)
                )
        elif task_obj.state == 'i' and state == 'c':  # 审核通过
            if task_id not in self.can_verify_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            if not check_task_complete(task_obj):
                raise AuthenticationException(error=u"当前任务仍有未完成的子任务")
            with transaction.atomic():
                task_obj.state = state
                task_obj.save()
                TaskRecord.objects.create(
                    task=task_obj,
                    user=user,
                    record=u'{}通过任务审核'.format(user.username)
                )
                # 删除当前日期后的日程
                Event.objects.filter(task_id=task_id, start_time__gt=datetime.date.today()).delete()
        elif state == 's':  # 取消任务
            if task_id not in self.can_suspend_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                task_obj.state = state
                task_obj.save()
                TaskRecord.objects.create(
                    task=task_obj,
                    user=user,
                    record=u'{}取消任务'.format(user.username)
                )
            # 删除日程
            Event.objects.filter(task_id=task_id).delete()
        else:
            raise AuthenticationException(error=u"无效的请求")

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }

        return JsonResponse(res)
