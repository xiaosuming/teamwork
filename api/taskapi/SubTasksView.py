from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.ParaPatternException import ParaPatternException
from task.models.Task import Task
from task.models.TaskParticipant import TaskParticipant
from task.models.TaskRecord import TaskRecord
from task.models.SubTask import SubTask
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from api.mixin.TaskMixin import TaskMixin
import datetime

# @class SubTasksView 子任务视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	子任务视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/17 16:26:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class SubTasksView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName subtasks_get
        @api {GET} /teamwork/tasks/{{task_id}}/sub/ SubTask: Get SubTasks Belong to Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [子任务管理]获取当前任务的子任务列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 子任务列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'id': 1,
                'director': '张三',
                'end_time': '2019-10-15',
                'describe': '风生水起啊，每天就爱穷开心阿，逍遥的魂儿啊，假不正经啊',
                'state': '未完成',
                },
            ]
        }
    """
    def get(self, request, task_id):
        # 1 获取参数
        user = request.user
        task_id = int(task_id)

        # 2 校验参数
        try:
            task = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请求的任务不存在')

        if int(task_id) not in self.availabel_task(user):
            raise AuthenticationException(error=u'请求的任务不可见')

        # 3 获取子任务列表
        sub_task_list = SubTask.objects.filter(task=task)

        info_list = list()
        for sub_task in sub_task_list:
            info = {
                'id': sub_task.id,
                'end_time': sub_task.end_time.strftime('%Y-%m-%d'),
                'describe': sub_task.describe,
                'state': sub_task.get_state_display()

            }
            if sub_task.participant:
                info['participant'] = sub_task.participant.participant.username
            info_list.append(info)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': info_list,
        }

        return JsonResponse(res)

    """
        @apiName subTasksPost
        @api {POST} /teamwork/tasks/{{task_id}}/sub/ SubTask: Post a new SubTask
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [子任务管理]添加子任务
        @apiParam {Int} participant_id 参与人id
        @apiParam {String} describe 子任务描述
        @apiParam {Date} start_time 开始时间
        @apiParam {Date} end_time 结束时间
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Int} data 子任务id
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
            'data': 15,
        }
    """
    def post(self, request, task_id):
        # 1 获取参数
        user = request.user
        participant_id = request.POST.get('participant_id')
        describe = request.POST.get('describe')
        start_time = request.POST.get('start_time')
        end_time = request.POST.get('end_time')
        task_id = int(task_id)

        # 2 校验参数
        try:
            task = Task.objects.get(id=task_id, director=user)
        except Exception:
            raise AuthenticationException(error=u'当前用户没有操作权限')

        try:
            participant_id = int(participant_id)
        except Exception:
            raise ParaPatternException(error=u'请输入正确的参与人id')

        try:
            task_participant = TaskParticipant.objects.get(task=task, participant_id=participant_id)
            participant = task_participant.participant
        except Exception:
            raise ParaPatternException(error=u'请从任务参与人中选择子任务参与人')

        try:
            start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d').date()
        except Exception:
            raise ParaPatternException(error=u'开始时间格式必须为%s' % "'YYYY-mm-dd'")
        else:
            if start_time > task.end_time:
                raise ParaPatternException(error=u'子任务开始时间不能晚于任务结束时间')

        try:
            end_time = datetime.datetime.strptime(end_time, '%Y-%m-%d').date()
        except Exception:
            raise ParaPatternException(error=u'结束时间格式必须为%s' % "'YYYY-mm-dd'")
        else:
            if end_time < start_time:
                raise ParaPatternException(error=u'结束时间不能早于开始时间')
            elif end_time > task.end_time:
                raise ParaPatternException(error=u'子任务结束时间不能晚于任务结束时间')

        # 3 创建子任务
        with transaction.atomic():
            # 3.1 创建子任务
            subtask_obj = SubTask.objects.create(
                                task=task,
                                participant=task_participant,
                                start_time=start_time,
                                end_time=end_time,
                                describe=describe
                            )

            # 3.2 创建任务操作记录
            TaskRecord.objects.create(
                    user=user,
                    record=u'创建了子任务{}-{}'.format(task_participant.participant.username, describe),
                    task=task
                    )

        # 3.3 通知负责人
        channel_layer = get_channel_layer()

        async_to_sync(channel_layer.group_send)(
                "notification_"+str(participant.id),
                {
                    'type': 'get_notification',
                    'message': 'a new subtask of task %s has been assigned to you' % task.name,
                }
            )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': subtask_obj.id 
                }
        return JsonResponse(res)
