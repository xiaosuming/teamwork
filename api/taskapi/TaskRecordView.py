from django.views.generic import View
from django.http.response import JsonResponse
from task.models.Task import Task
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.parse_record import parse_record
from api.mixin.TaskMixin import TaskMixin

# @class TaskRecordView 任务操作记录视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/03 08:51:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskRecordView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_record_get
        @api {GET} /teamwork/tasks/{{task_id}}/records/ Record: Get Task Records
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]获取任务操作记录
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                'records':[
                    '2019-10-16 李大嘴创建了本任务',
                    '2019-10-16 李大嘴创建了子任务'，
                ]
            }
        }
    """
    def get(self, request, task_id):
        # 1.获取参数
        task_id = int(task_id)

        # 2.校验参数
        user = request.user
        try:
            task = Task.objects.get(id=task_id)
        except Exception:
            raise UnknowResourcesException(error=u"请求的任务不存在")
        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u"没有访问权限")

        # 3.获取任务记录
        records = parse_record(task, task=True)

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "records": records
            }
        }
        return JsonResponse(res)
