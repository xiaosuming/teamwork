from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from task.models.Task import Task
from task.models.TaskParticipant import TaskParticipant
from task.models.TaskComment import TaskComment
from task.models.CommentEnclosure import CommentEnclosure
from task.models.SubTask import SubTask
from api.mixin.TaskMixin import TaskMixin
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

# @class TaskComment 任务进展视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务进展视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/28 13:25:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskCommentsView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_comments_get
        @api {GET} /teamwork/tasks/{{task_id}}/comments/ Comment: Get Task Comment
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务进展管理]获取全部任务进展列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 任务进展列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    'id': 66,
                    'seq': 11,
                    'creater': '李大嘴',
                    'created_time': '2019-10-28 14:31:19',
                    'content': '任务完成，功能完善',
                    'type': '任务自述',
                    'subtask': '子任务一',
                    'score': '5',
                    'response_to': '张三-9楼',
                    'enclosures': [
                        {
                            'name': '文件1',
                            'size': 2048,
                            'uri': 'media/enclosure/20/xxx'
                        },
                    ]
                },
            ]
        }
    """
    def get(self, request, task_id):
        # 1 获取参数
        user = request.user
        task_id = int(task_id)

        # 2 校验参数
        try:
            task = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请求任务不存在')

        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u'请求任务不可见')

        if task.supervisor == user:
            role = 'sup'
        else:
            role = None

        # 3 获取所有评论
        comment_list = list()
        comments = TaskComment.objects.filter(task=task).order_by('created_time')
        id_list = list(comments.values_list("id", flat=True))
        id_list.insert(0, 1)
        for comment in comments:
            info = {
                'id': comment.id,
                'seq': id_list.index(comment.id),
                'creater': comment.creater.username,
                'created_time': comment.created_time.strftime('%Y-%m-%d %H:%M:%S'),
                'content': comment.content,
                'type': comment.get_comment_type_display(),
                'subtask': comment.subtask.describe if comment.subtask else None,
            }
            # 评论回复对象
            response_to = None
            if comment.response_to:  # 评论有回复对象
                response_to = "{},{}楼".format(
                    comment.response_to.creater.username,
                    id_list.index(comment.response_to_id),
                )
            info["response_to"] = response_to
            # 评论附件
            attach_enclosure = list()
            enclosures = CommentEnclosure.objects.filter(task_comment=comment)
            for enclosure in enclosures:
                enclosure_info = {
                    "name": enclosure.name,
                    "size": enclosure.enclosure.size,
                    "uri": enclosure.enclosure.path,
                }
                attach_enclosure.append(enclosure_info)
            info["enclosures"] = attach_enclosure
            # 是否显示评分
            if info['type'] == '审核意见':
                if role:
                    info['score'] = comment.score
                else:
                    info['score'] = None
            else:
                info['score'] = comment.score
            comment_list.append(info)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': comment_list,
        }
        return JsonResponse(res)

    """
        @apiName taskCommentsPost
        @api {POST} /teamwork/tasks/{{task_id}}/comments/ Comment: Post Task Comment
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务评论]提交任务评论
        @apiParam {Int} score 评分
        @apiParam {Int} subtask_id 子任务id, 子任务评分时必需
        @apiParam {String} content 内容 s,任务自述; a,审核意见; r,退回理由; o,常规评论
        @apiParam {String} type 进展类型
        @apiParam {Int} response_to 回复评论的楼层, 可不填
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 任务进展列表
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
        }
    """
    def post(self, request, task_id):
        # 1 获取参数
        user = request.user
        content = request.POST.get('content')
        score = request.POST.get('score')
        subtask_id = request.POST.get('subtask_id')
        response_to = request.POST.get("response_to", None)
        comment_type = request.POST.get('type')
        comment_type_choices = ('s', 'a', 'r', 'o')
        task_id = int(task_id)

        # 2 校验参数
        try:
            task = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise ParaMissingException(error=u'任务不存在')

        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u'请求任务不可见')

        if comment_type not in comment_type_choices:
            raise ParaPatternException(error=u'请输入正确的进展类型')
        elif comment_type == 's' and task.director != user:
            raise ParaPatternException(error=u'仅负责人可以提交任务自评')
        elif comment_type == 'a' and task.supervisor != user:
            raise ParaPatternException(error=u'仅审核人可以提交审核意见')

        if not content:
            raise ParaMissingException(error=u'进展内容为空')

        if response_to:
            try:
                response_to = TaskComment.objects.get(id=response_to, task=task)
            except ObjectDoesNotExist:
                raise UnknowResourcesException(error=u"回复对象不存在")

        if score:
            try:
                score = int(score)
            except Exception:
                raise ParaPatternException(error=u'评分只能为整数或None')

        if subtask_id:
            try:
                subtask = SubTask.objects.get(id=subtask_id, task_id=task_id)
            except ObjectDoesNotExist:
                raise UnknowResourcesException(error=u"请求的子任务不存在")

            if comment_type in ('s', 'a'):  # 子任务自述或审核意见
                if score in range(0, 6):
                    score = str(score)
                else:
                    raise ParaPatternException(error=u'请输入5以内的整数评分')
            else:
                score = None
            if comment_type in ('a', 'r'):  # 子任务审核意见或退回原因
                if response_to.comment_type != 's':
                    raise AuthenticationException(error=u"请选择任务自述作为回复对象")
        else:
            subtask = None
            score = None

        # 3.1 存储数据
        TaskComment.objects.create(
            task=task,
            subtask_id=subtask_id,
            creater=user,
            subtask=subtask,
            score=score,
            content=content,
            comment_type=comment_type,
            response_to=response_to,
        )

        # 3.2 通知任务参与人
        task_participant = TaskParticipant.objects.filter(task=task).values_list('participant', flat=True)
        task_participant = list(task_participant)
        task_participant.append(task.supervisor.id)

        channel_layer = get_channel_layer()
        for participant_id in task_participant:
            async_to_sync(channel_layer.group_send)(
                "notification_" + str(participant_id),
                {
                    'type': 'get_notification',
                    'message': '收到新的任务评论-%s' % task.name,
                }
            )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
        }
        return JsonResponse(res)
