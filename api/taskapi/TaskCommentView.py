from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from task.models.TaskComment import TaskComment
from api.mixin.TaskMixin import TaskMixin

# @class TaskComment 任务进展删除视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务进展视图
#   method:DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/08 11:28:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskCommentView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_comment_delete
        @api {DELETE} /teamwork/tasks/comments/{{comment_id}} Comment: Delete Task Comment
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务进展管理]获取全部任务进展列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request, comment_id):
        # 1 获取参数
        user = request.user
        comment_id = int(comment_id)

        # 2 校验参数
        try:
            comment = TaskComment.objects.get(id=comment_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请求评论不存在')
        if comment.creater != user:
            raise AuthenticationException(error=u'没有操作权限')

        # 3 删除评论
        comment.delete()

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }
        return JsonResponse(res)
