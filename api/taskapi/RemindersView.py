from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import JsonResponse
from api.mixin.TaskMixin import TaskMixin
from task.models.Task import Task
from reminder.models.Reminder import Reminder
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.Reminder import reminder_scheduler
import datetime

# @class TaskReminder 任务提示视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务进展视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/05 16:45:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class RemindersView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_reminders_get
        @api {GET} /teamwork/tasks/{{task_id}}/reminders/ Reminders: Get Task Reminder
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务提醒]获取未读的任务提醒
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 任务进展列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "reminder": [
                {
                "id": 12,  
                "dest_time": "2020-01-05",  提醒日期
                "message": "距离截至日期还剩3天"  提醒内容
                }
            ]
        }
    """
    def get(self, request, task_id):
        # 1.获取参数
        user = request.user
        task_id = int(task_id)

        # 2.校验参数
        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u"没有访问权限")
        try:
            Task.objects.get(id="task_id")
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的任务不存在")

        # 3.1.获取提醒
        reminders = Reminder.objects.filter(
            user=user,
            related_obj="T",
            related_id=task_id,
            dest_time__lt=datetime.datetime.now(),
        )
        # 3.2.提取数据
        res_data = reminders.values("id", "dest_time", "message")

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": res_data
        }
        return JsonResponse(res)

    """
        @apiName task_reminders_post
        @api {POST} /teamwork/tasks/{{task_id}}/reminders/ Reminders: Set Task Reminder
        @apiGroup Task
        @apiVersion 0.0.1
        @apiParam {List} user_ids 被提醒人员id列表
        @apiParam {String} remind_time 提醒时间
        @apiDescription [任务提醒]设置任务提醒
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 任务进展列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request, task_id):
        # 1.获取参数
        user = request.user
        user_ids = request.POST.get("user_ids")
        remind_time = request.POST.get("remind_time")
        task_id = int(task_id)

        # 2.校验参数
        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u"没有访问权限")
        try:
            task_obj = Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的任务不存在")

        try:
            user_ids = eval(user_ids)
        except (ValueError, NameError, SyntaxError):
            raise ParaPatternException(error=u'无效的用户id列表')

        try:
            remind_time = datetime.datetime.fromisoformat(remind_time)
        except ValueError:
            raise ParaPatternException(error=u"提醒时间必须为iso标准格式")

        # 3.1.设置提醒
        delta_month = task_obj.end_time.month - remind_time.month
        delta_day = task_obj.end_time.day - remind_time.day
        message = u"距离任务结束还有{}个月零{}天".format(delta_month, delta_day)
        reminder_scheduler(trigger_time=remind_time, user_ids=user_ids, message=message)
        # 3.2.记录提醒
        for user_id in user_ids:
            Reminder.objects.create(
                user_id=user_id,
                creator=user,
                related_obj="T",
                related_id=task_id,
                message=message,
                dest_time=remind_time,
            )

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS"
        }
        return JsonResponse(res)
