from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.FileMerge import FileManager
from task.models.Task import Task
from task.models.TaskEnclosure import TaskEnclosure
from api.mixin.TaskMixin import TaskMixin
from enclosure.models.Enclosure import Enclosure

# @classTaskEnclosuresView 任务附件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	项目附件视图
#   method:GET, POST, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/04 14:07:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskEnclosuresView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.file_manager = FileManager()

    """
        @apiName task_enclosures_get
        @api {GET} /teamwork/tasks/{{task_id}}/enclosures/ Enclosures: Get Enclosures attached to Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务附件]获取任务附件列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件id
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
            'data': [
                {
                    "name": "文件1",
                    "size": 100k,
                    "uri": "media/enclosure/20/xxx"
                },
            ]
        }
    """
    def get(self, request, task_id):
        # 1 获取参数
        user = request.user
        task_id = int(task_id)

        # 2 校验参数
        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u"没有访问权限")

        try:
            Task.objects.get(id=task_id)
        except Exception:
            raise ParaMissingException(error=u'任务不存在')

        # 3 获取数据
        enclosures = TaskEnclosure.objects.filter(task_id=task_id)
        res_data = list()
        for enclosure in enclosures:
            res_data.append(
                {
                    "name": enclosure.name,
                    "size": enclosure.enclosure.size,
                    "uri": enclosure.enclosure.path,
                }
            )
        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            "data": res_data,
                }
        return JsonResponse(res)

    """
        @apiName task_enclosures_post
        @api {POST} /teamwork/tasks/{{task_id}}/enclosures/ Enclosures: Attach Enclosure to Task
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务附件]合并附件切片
        @apiParam {String} name 文件名称
        @apiParam {Int} enclosure_id 附件id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目附件id
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
            'data': 13,
        }
    """
    def post(self, request, task_id):
        # 1 获取参数
        user = request.user
        name = request.POST.get('name')
        enclosure_id = request.POST.get('enclosure_id')
        task_id = int(task_id)

        # 2 校验参数
        if not name:
            raise ParaMissingException(error=u'文件名为空')

        try:
            task_obj = Task.objects.get(id=task_id)
        except Exception:
            raise UnknowResourcesException(error=u'任务不存在')

        try:
            enclosure_id = int(enclosure_id)
        except ValueError:
            raise ParaPatternException(error=u"附件id必须为整数")
        if Enclosure.objects.filter(id=enclosure_id).count() == 0:
            raise UnknowResourcesException(error=u'附件不存在')

        # 3. 创建项目附件
        enclosure_obj = TaskEnclosure.objects.create(
            user=user,
            name=name,
            task=task_obj,
            enclosure_id=enclosure_id,
        )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                'data': enclosure_obj.id,
                }
        return JsonResponse(res)
