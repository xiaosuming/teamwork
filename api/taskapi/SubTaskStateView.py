from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from task.models.TaskRecord import TaskRecord
from task.models.SubTask import SubTask
from api.mixin.TaskMixin import TaskMixin
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import json

# @class SubTasksView 子任务状态视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	子任务视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/17 16:26:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class SubTaskStateView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName subTaskStatePut
        @api {PUT} /teamwork/tasks/sub/{{sub_id}}/state/ SubTask: Update SubTask State 
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [子任务管理]子任务的审核与取消
        @apiParam {Int} state 子任务状态 ['o', 'i', 'c', 's'] 提交审核: i, 审核通过: c, 驳回审核: o, 取消任务: s
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 子任务列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request, sub_id):
        # 1 获取参数
        user = request.user
        data = json.loads(request.body)
        state = data.get('state', None)
        state_class = ('o', 'i', 'c', 's')
        sub_id = int(sub_id)

        # 2 校验参数
        try:
            sub_task = SubTask.objects.get(id=sub_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'子任务不存在')
        else:
            task = sub_task.task

        if state not in state_class:
            raise ParaPatternException(error=u'子任务状态仅接受%s' % "'o', 'i', 'c', 's'")

        # 3 修改子任务状态
        if sub_task.state == 'o' and state == 'i':  # 提交任务审核
            if sub_id not in self.can_apply_sub_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                sub_task.state = state
                sub_task.save()
                TaskRecord.objects.create(
                    task=task,
                    user=user,
                    record=u'{}提交任务审核'.format(user.username)
                )
            # 通知审核人
            message = u'%s 申请子任务"%s"完成审核' % (user.username, sub_task.describe[:10])
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                "notification_" + str(sub_task.task.director_id),
                {
                    'type': 'get_notification',
                    'message': message,
                }
            )
        elif sub_task.state == 'i' and state == 'o':  # 驳回任务审核
            if sub_id not in self.can_verify_sub_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                sub_task.state = state
                sub_task.save()
                TaskRecord.objects.create(
                    task=task,
                    user=user,
                    record=u'{}驳回子任务{}审核'.format(user.username, sub_task.describe[:10])
                )
        elif sub_task.state == 'i' and state == 'c':  # 审核通过
            if sub_id not in self.can_verify_sub_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            task_completed = check_task_complete(task)  # 所属任务是否已完成
            with transaction.atomic():
                sub_task.state = state
                sub_task.save()
                if task_completed:
                    task.state = 'c'
                    task.save()
                TaskRecord.objects.create(
                    task=task,
                    user=user,
                    record=u'{}通过子任务{}审核'.format(user.username, sub_task.describe[:10])
                )
                TaskRecord.objects.create(
                    task=task,
                    user=user,
                    record=u'任务已完成'
                )
        elif state == 's':  # 取消子任务
            if sub_id not in self.can_suspend_sub_task(user):
                raise AuthenticationException(error=u"没有操作权限")
            with transaction.atomic():
                sub_task.state = state
                sub_task.save()
                TaskRecord.objects.create(
                    task=task,
                    user=user,
                    record=u'{}取消子任务{}'.format(user.username, sub_task.describe[:10])
                )
        else:
            raise AuthenticationException(error=u"请求非法")

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }

        return JsonResponse(res)


def check_task_complete(task_obj):
    """
    take a task object, return whether all related subtask have completed
    :param task_obj: Task object
    :return: Bool
    """
    all_completed = True
    for subtask in task_obj.subtask.all():
        if subtask.state not in ('c', 's'):  # 已完成或已取消
            all_completed = False
            break
    return all_completed
