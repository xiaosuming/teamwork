from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import JsonResponse
from api.mixin.TaskMixin import TaskMixin
from task.models.Task import Task
from reminder.models.Reminder import Reminder
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException

# @class TaskReminder 任务提示视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务进展视图
#   method:PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/05 16:45:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class ReminderView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_reminder_put
        @api {PUT} /teamwork/tasks/{{task_id}}/reminder/{{reminder_id}} Reminder: Set Task Reminder readed
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务提醒]标记任务提醒为已读
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 任务进展列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request, task_id, reminder_id):
        # 1.获取参数
        user = request.user
        task_id = int(task_id)
        reminder_id = int(reminder_id)

        # 2.校验参数
        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u"没有访问权限")
        try:
            Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的任务不存在")

        try:
            reminder = Reminder.objects.get(id=reminder_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的提醒不存在")

        # 3.1.删除提醒
        reminder.delete()

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res)

    """
        @apiName task_reminder_put
        @api {DELETE} /teamwork/tasks/{{task_id}}/reminder/{{reminder_id}} Reminder: Remove Task Reminder
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务提醒]删除自己发出的任务提醒
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 任务进展列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def deletet(self, request, task_id, reminder_id):
        # 1.获取参数
        user = request.user
        task_id = int(task_id)
        reminder_id = int(reminder_id)

        # 2.校验参数
        if task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u"没有访问权限")
        try:
            Task.objects.get(id=task_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的任务不存在")

        try:
            reminder = Reminder.objects.get(id=reminder_id, creator=user)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的提醒不存在")

        # 3.1.删除提醒
        reminder.delete()

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res)
