from django.views.generic import View
from django.http import JsonResponse
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.ParaMissingException import ParaMissingException
from task.models.TaskComment import TaskComment
from task.models.TaskRecord import TaskRecord
from task.models.SubTask import SubTask
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from api.mixin.TaskMixin import TaskMixin
import datetime
import json

# @class SubTasksView 子任务视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	子任务视图
#   method:GET, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/03 11:40:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class SubTaskView(View, TaskMixin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName sub_task_get
        @api {GET} /teamwork/tasks/sub/{{sub_id}}/ SubTask: Get SubTask Detail
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [子任务管理]获取子任务详情
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 子任务列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                'participant': '张三',
                'start_time': 2019-12-20
                'end_time': '2020-01-15',
                'describe': '风生水起啊，每天就爱穷开心阿，逍遥的魂儿啊，假不正经啊',
                'state': '未完成',
                'comments': [
                    {
                        "id": 13,
                        "type": "任务自述",
                        "score": 4,
                        "content": "任务已完成"
                    },
                    {
                        "id": 16,
                        "type": "审核意见",
                        "score": 0,
                        "content": "任务不合格",
                    }
                ]
            },
        }
    """
    def get(self, request, sub_id):
        # 1 获取参数
        user = request.user
        sub_id = int(sub_id)

        # 2 校验参数
        try:
            sub_task = SubTask.objects.get(id=sub_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请求的子任务不存在')

        if sub_task.task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u'请求的子任务不可见')

        # 3 获取子任务列表
        info = {
            'start_time': sub_task.start_time.strftime('%Y-%m-%d'),
            'end_time': sub_task.end_time.strftime("%Y-%m-%d"),
            'describe': sub_task.describe,
            'state': sub_task.get_state_display()
        }
        if sub_task.participant:
            info['participant'] = sub_task.participant.participant.username
        related_comment = list()
        comments = TaskComment.objects.filter(subtask=sub_task)
        for comment in comments:
            com_info = {
                "id": comment.id,
                "type": comment.get_comment_type_display(),
                "score": None,
                "content": comment.content,
            }
            if comment.type in ('a', 'r'):
                if sub_task.task.director == user:
                    com_info["score"] = comment.score
            else:
                com_info["score"] = comment.score
            related_comment.append(com_info)
        info["comments"] = related_comment

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': info,
        }

        return JsonResponse(res)

    """
        @apiName sub_task_put
        @api {PUT} /teamwork/tasks/sub/{{sub_id}}/ SubTask: Update SubTask
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [子任务管理]更新子任务
        @apiParam {String} describe 子任务描述
        @apiParam {Date} start_time 开始时间
        @apiParam {Date} end_time 结束时间
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {Int} data 子任务id
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': "SUCCESS",
        }
    """
    def put(self, request, sub_id):
        # 1 获取参数
        user = request.user
        put_data = json.loads(request.body)
        describe = put_data.get('describe')
        start_time = put_data.get('start_time')
        end_time = put_data.get('end_time')
        sub_id = int(sub_id)

        # 2 校验参数
        try:
            sub_task = SubTask.objects.get(id=sub_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的子任务不存在")
        if sub_task.task_id not in self.availabel_task(user):
            raise AuthenticationException(error=u'当前用户没有操作权限')
        else:
            task = sub_task.task

        if not (start_time or end_time or describe):
            raise ParaMissingException(error=u"没有修改内容")

        if start_time:
            try:
                start_time = datetime.date.fromisoformat(start_time)
            except Exception:
                raise ParaPatternException(error=u'开始时间格式必须为%s' % "'YYYY-mm-dd'")
            else:
                if start_time > task.end_time:
                    raise ParaPatternException(error=u'子任务开始时间不能晚于任务结束时间')

        if end_time:
            try:
                end_time = datetime.date.fromisoformat(end_time)
            except Exception:
                raise ParaPatternException(error=u'结束时间格式必须为%s' % "'YYYY-mm-dd'")
            else:
                if start_time and end_time < start_time:
                    raise ParaPatternException(error=u'结束时间不能早于开始时间')
                elif end_time > task.end_time:
                    raise ParaPatternException(error=u'子任务结束时间不能晚于任务结束时间')

        # 3 创建子任务
        with transaction.atomic():
            # 3.1 修改子任务
            if start_time:
                sub_task.start_time = start_time
            if end_time:
                sub_task.end_time = end_time
            if describe:
                sub_task.describe = describe
            sub_task.save()

            # 3.2 创建任务操作记录
            TaskRecord.objects.create(
                    user=user,
                    record=u'修改子任务{}-{}'.format(user.username, describe),
                    task=task
                    )

        # 3.3 通知负责人
        channel_layer = get_channel_layer()
        participant_id = sub_task.participant.participant.id
        async_to_sync(channel_layer.group_send)(
                "notification_"+str(participant_id),
                {
                    'type': 'get_notification',
                    'message': 'a subtask of task %s has been updated' % task.name,
                }
            )

        # 4 返回应答
        res = {
                'code': 0,
                'msg': 'SUCCESS',
                }
        return JsonResponse(res)
