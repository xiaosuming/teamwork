from django.http.response import JsonResponse
from django.views.generic import View
from task.models.TaskFilter import TaskFilter
from utils.errorcode.ParaMissingException import ParaMissingException
import json

# @class TaskFilterView 任务过滤器视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	任务视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/03 14:02:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TaskFiltersView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName task_filters_get
        @api {GET} /teamwork/tasks/filters/ Filters: Get Task Filter
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]获取任务过滤器
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "id": 1
                    "name": "过滤器1",
                },
            ]
        }
    """
    def get(self, request):
        # 1.获取参数
        user = request.user
        # 2.获取数据
        filters = list(
            TaskFilter.objects.filter(creator=user).values("id", "name"))
        # 3.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": filters,
        }
        return JsonResponse(res)

    """
        @apiName task_filters_post
        @api {POST} /teamwork/tasks/filters/ Filters: Create Task Filter
        @apiGroup Task
        @apiVersion 0.0.1
        @apiDescription [任务管理]创建任过滤器
        @apiParam {String} name 过滤器名称
        @apiParam {List} participant 参与人id列表
        @apiParam {List} supervisor 审核人id列表
        @apiParam {List} creator 创建人id列表
        @apiParam {List} rate 分级code列表 (o 普通, i 重要, e 紧急, u 重要紧急)
        @apiParam {List} project 关联项目id列表
        @apiParam {List} phase 时间列表
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 任务详情
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "id": 3  过滤器id
            }
        }
    """
    def post(self, request):
        # 1.获取参数
        user = request.user
        name = request.POST.get("name")
        participant = request.POST.get("participant")
        supervisor = request.POST.get("supervisor")
        creator = request.POST.get("creator")
        rate = request.POST.get("rate")
        project = request.POST.get("project")
        phase = request.POST.get("phase")

        # 2.校验参数
        if not name:
            raise ParaMissingException(error=u"过滤器名称为空")

        conditions = {
            "participant": filter_id_list(participant),
            "supervisor": filter_id_list(supervisor),
            "creator": filter_id_list(creator),
            "project": filter_id_list(project),
            "rate": filter_list(rate, ("o", "i", "e", "u")),
            "phase": filter_list(phase, ("started", "terminated"))
        }
        conditions = json.dumps(conditions)

        # 3.储存筛选器
        task_filter = TaskFilter.objects.create(
            creator=user,
            name=name,
            conditions=conditions,
        )

        # 4.返回响应
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "id": task_filter.id
            }
        }
        return JsonResponse(res)


def filter_id_list(raw_list):
    """
    接受一个id列表, 过滤掉其中的重复和非数字的元素
    :param raw_list: 待过滤的id列表
    :return: id_list: 过滤后的列表
    """
    id_set = set(raw_list)
    id_list = list()
    for item in id_set:
        try:
            item = int(item)
            id_list.append(item)
        except ValueError:
            continue
    return id_list


def filter_list(raw_list, allowed_item):
    """
    接受一个列表和允许的值, 剔除列表中重复和不允许的值
    :param raw_list: 原始列表
    :param allowed_item: 允许的值
    :return: 筛选后的列表
    """
    res_set = set()
    for item in raw_list:
        if item in allowed_item:
            res_set.add(item)
    res_list = list(res_set)
    return res_list
