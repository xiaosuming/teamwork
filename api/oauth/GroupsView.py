from django.views.generic import View
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from utils.OperateToken import remove_access_token, get_access_token
from starxteam.settings import HOST, SSO_CONFIG, OAUTH_URL
import requests

# @class UsersView 群组列表视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	群组列表视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/02/24 17:32:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class GroupsView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName groups_get
        @api {GET} /teamwork/oauth/groups/ Get All Groups
        @apiGroup OAuth
        @apiVersion 0.0.1
        @apiDescription [工作台]获取用户列表
        @apiParam {String} contains 搜索信息
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccess {json} data 实验列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data":[
                {
                    "id": 2,
                    "name": "群组一",
                    "users": [
                        {
                            "id": 6,
                            "username": "hwx"
                        },
                        {
                            "id": 5,
                            "username": "tdy"
                        },
                        {
                            "id": 7,
                            "username": "zzp"
                        }
                    ]
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        uat = request.headers.get("UAT")
        contains = request.GET.get("contains")

        # 2.向CAS请求用户信息
        # 2.1.获取本地access_token
        res = {
            'code': 1,
            'msg': 'ERROR'
        }
        access_token = get_access_token(uat)
        if access_token:
            # 请求CAS群组
            headers = {"Authorization": access_token, "UAT": uat}
            host = HOST['CAS_HOST']
            users_url = OAUTH_URL["GROUPS"]
            url = host + users_url
            group_res = requests.get(
                url=url,
                headers=headers
            )
            group_list = list()
            if group_res.status_code == 200:
                group_dict = group_res.json()
                # group_dict = {
                #   "data": [
                #       {
                #            "id": 2,
                #            "name": "群组一",
                #            "users": ["konki", "guangji"]
                #       },
                #   ]
                # }
                groups = group_dict["data"]
                for group in groups:
                    info = {
                        "id": group["id"],
                        "name": group['name'],
                    }
                    if contains and contains not in group['name']:
                        continue
                    group_list.append(info)
                    user_names = group['users']
                    create_all_user(user_names)
                    inner_users = User.objects.filter(username__in=user_names).values("id", "username")
                    info.update({"users": list(inner_users)})

                res = {
                    'code': 0,
                    'msg': 'SUCCESS',
                    'data': group_list,
                }
            else:  # access token 无效
                remove_access_token(uat)

        # 2 返回数据
        return res


def create_all_user(user_name_list):
    """
    接受一个用户名列表, 将其中的所有用户加入数据库
    :param user_name_list:
    :return:
    """
    for username in user_name_list:
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            User.objects.create_user(username=username, password=SSO_CONFIG['INIT_PWD'])
