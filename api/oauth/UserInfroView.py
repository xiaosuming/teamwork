from django.views.generic import View
from django.utils.decorators import method_decorator
from django.http.response import JsonResponse

class UserInfoView(View):
    def __inti__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName userinfo_get
        @api {GET} /workbench/oauth/userinfo/ User: Get User Name
        @apiGroup OAuth
        @apiVersion 0.0.1
        @apiDescription [工作台]用户信息
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccess {List} data 用户信息
        @apiSuccess {String} user_name 用户名称
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "user_name": "admin",
            }
        }
    """
    def get(self, request):
        # 1.获取参数
        login_user = request.user

        # 3. 返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "user_name": login_user.username
            }
        }
        return JsonResponse(res)
