from django.views.generic import View
from django.http.response import JsonResponse
from utils.OperateToken import operate_token
from starxteam.settings import REDIS, SSO_CONFIG
import redis
import requests

# @class TokenView access_token视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	用户列表视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/02/24 16:28:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class TokenView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName change_token
        @api {GET} /teamwork/oauth/token/ Exchange Access Token
        @apiGroup OAuth
        @apiVersion 0.0.1
        @apiDescription [工作台]请求AccessToken
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccess {json} data 实验列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def get(self, request):
        # 1 获取参数
        code = request.GET.get("code")

        # 2.向CAS请求token
        post_data = {
            "code": code,
            "client_id": SSO_CONFIG["CLIENT_ID"],
            "client_secret": SSO_CONFIG["CLIENT_SECRET"],
            "grant_type": "authorization_code",
            "redirect_uri": SSO_CONFIG["REDIRECT_URI"],
        }
        token_res = requests.post(
            SSO_CONFIG["TOKEN_URI"], data=post_data,
        )
        if token_res.status_code == 200:
            token_dict = token_res.json()
            conn = redis.Redis(
                host=REDIS['HOST'],
                port=REDIS['PORT'],
                password=REDIS['PASSWORD'],
                db=REDIS["DB"]
            )
            access_token = operate_token(token_dict, conn)
            res = {
                "code": 0,
                "msg": "SUCCESS",
                "data": {
                    "access_token": access_token,
                }
            }
        else:
            res = {
                "code": 1,
                "msg": "ERROR",
            }
        return JsonResponse(res)
