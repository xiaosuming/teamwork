from django.views.generic import View
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import JsonResponse
from utils.OperateToken import remove_access_token, get_access_token, request_token
from utils.errorcode.TokenException import TokenException
from starxteam.settings import SSO_CONFIG, HOST, OAUTH_URL
import requests

# @class UsersView 用户列表视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	用户列表视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/12/03 20:27:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class UsersView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName users_get
        @api {GET} /workbench/oauth/users/ Get All Users
        @apiGroup OAuth
        @apiVersion 0.0.1
        @apiDescription [工作台]获取用户列表
        @apiParam {String} contains 搜索信息
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccess {json} data 实验列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data":[
                {
                    "id": 5,
                    "real_name": "tdy"
                },
                {
                    "id": 6,
                    "real_name": "hwx"
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        uat = request.headers.get("UAT")
        contains = request.GET.get("contains")

        # 2.向CAS请求用户信息
        # 2.1.获取本地access_token
        access_token = get_access_token(uat)
        if access_token is None:
            access_token = request_token(uat)

        if access_token:
            # 请求资源
            headers = {"Authorization": access_token, "UAT": uat}
            host = HOST['CAS_HOST']
            users_url = OAUTH_URL["USERS"]
            url = host + users_url
            user_res = requests.get(
                url=url,
                headers=headers
            )
            if user_res.status_code == 200:
                user_list = list()
                user_dict = user_res.json()
                users = user_dict["data"]
                for user in users:
                    username = user.get('username')
                    first_name = user.get('real_name')
                    try:
                        user_obj = User.objects.get(username=username)
                    except ObjectDoesNotExist:
                        user_obj = User.objects.create_user(
                            username=username, password=SSO_CONFIG['INIT_PWD'])
                    if contains and contains not in first_name:
                        continue
                    user_list.append(
                        {"id": user_obj.id, "real_name": first_name}
                    )
                res = {
                    'code': 0,
                    'msg': 'SUCCESS',
                    'data': user_list,
                }
            else:  # access token 无效
                access_token = remove_access_token(uat)
                raise TokenException(error=u"token实效，请重试")
        else:
            raise TokenException(error=u'请求用户信息失败')

        # 2 返回数据
        return JsonResponse(res)
