# -*- coding:utf-8 -*-
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from api.oauth.UsersView import UsersView
from api.oauth.TokenView import TokenView
from api.oauth.UserInfroView import UserInfoView
from api.oauth.GroupsView import GroupsView


urlpatterns = [
    url(u'^token/$', csrf_exempt(TokenView.as_view())),
    # url(u'^users/$', csrf_exempt(UsersView.as_view())),
    # url(u'^userinfo/$', csrf_exempt(UserInfoView.as_view())),
    # url(u'^groups/$', csrf_exempt(GroupsView.as_view())),
]
