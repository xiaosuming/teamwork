from django.urls import path, re_path
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from api.attendanceapi.AttendancesView import AttendancesView
from api.attendanceapi.LeaveApplicationsView import LeaveApplicationsView
from api.attendanceapi.LeaveApplicationView import LeaveApplicationView
from api.attendanceapi.MakeUpsView import MakeUpsView
from api.attendanceapi.MakeUpView import MakeUpView


app_name = 'attendance'


urlpatterns = [
        re_path(r'^$', csrf_exempt(AttendancesView.as_view())),
        path('leave_applications/', csrf_exempt(LeaveApplicationsView.as_view())),
        url(r"leave_applications/(?P<application_id>\d+)/$", csrf_exempt(LeaveApplicationView.as_view())),
        url(r"makeups/$", csrf_exempt(MakeUpsView.as_view())),
        url(r"makeups/(?P<makeup_id>\d+)/$", csrf_exempt(MakeUpView.as_view())),
        ]
