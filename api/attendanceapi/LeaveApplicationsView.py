from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AnonymousUserException import AnonymousUserException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from api.mixin.LeaveApplicationMixin import LeaveApplicationMixin
from user.models import User
from attendance.models.Attendance import Attendance
from attendance.models.LeaveApplication import LeaveApplication
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import datetime
import redis

# @class LeaveApplicationView 请假申请视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	请假申请视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/30 11:40:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class LeaveApplicationsView(View, LeaveApplicationMixin):
    APP_TYPE, APP_CHAR = zip(*LeaveApplication.TYPE_CHOICES)
    conn = redis.Redis('127.0.0.1', 6379, db=3, decode_responses=True)
    channel_layer = get_channel_layer()

    """
        @apiName leaveApplicationsGet
        @api {GET} /teamwork/attendances/leave_applications/ LeaveApplications: Get Leave Application
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [请假申请管理]获取提交的请假/外出申请
        @apiParam {String} start_date iso标准格式起始日期字符串 "2020-12-20 06:00:00"
        @apiParam {String} end_time iso标准格式截止日期字符串 "2020-12-12 18:00:00"
        @apiParam {Bool} approved 是否批准, 全选则不传
        @apiParam {Int} offset 返回数据起点, default 0
        @apiParam {Int} limit 返回数据条数, default 7
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 请假/外出申请列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "projects": [
                {
                'key': 1，
                'applicant': '子房',
                'supervisor': '荀况',
                'start_time: "2019-10-15",
                'end_time': "8:58:00",
                'content': '抱恙，请休一日'
                'type': '病假',
                'approved': '待审核',
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        user = request.user
        start_time = request.GET.get("start_time")
        end_time = request.GET.get("end_time")
        approved = request.GET.get("approved")
        offset = request.GET.get("offset", 0)
        limit = request.GET.get("limit", 7)

        # 2 校验参数
        if start_time:
            try:
                start_time = datetime.datetime.fromisoformat(start_time)
            except ValueError:
                raise ParaPatternException(error='"start_time"必须是iso标准格式的时间字符串')
            except TypeError:
                raise ParaPatternException(error='"start_time"必须是iso标准格式的时间字符串')
        if end_time:
            try:
                end_time = datetime.datetime.fromisoformat(end_time)
            except ValueError:
                raise ParaPatternException(error='"end_time"必须是iso标准格式的时间字符串')
            except TypeError:
                raise ParaPatternException(error='和"end_time"必须是iso标准格式的时间字符串')
        try:
            offset, limit = int(offset), int(limit)
        except ValueError:
            raise ParaPatternException(error='"offset"/"limit"必须是整数')

        # 3.获取数据
        applications = self.applied_applications(
            user, start_time, end_time, approved
        )[offset: offset+limit]
        res_data = list()
        for application in applications:
            info = {
                "id": application.id,
                "applicant": application.applicant.username,
                "supervisor": application.supervisor.username,
                "start_time": application.start_time.strftime("%Y-%m-%d %H:%M:%S"),
                "end_time": application.end_time.strftime("%Y-%m-%d %H:%M:%S"),
                "content": application.content,
                "approved": application.get_approved_display(),
                "type": application.get_leave_type_display(),
            }
            res_data.append(info)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': res_data,
        }
        return JsonResponse(res)

    """
        @apiName leaveApplicationsPost
        @api {POST} /teamwork/attendances/leave_applications/ Post Leave Application
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [请假申请管理]发起请假/外出申请
        @apiParam {Int} supervisor_id 审核人id
        @apiParam {Date} start_time 开始时间
        @apiParam {Date} end_time 结束时间
        @apiParam {String} content 申请原因
        @apiParam {String} type 申请类型 (
                                        ('c', '事假'),('s', '病假'),('a', '年假'),('w', '婚假'),
                                        ('m','产假'),('p', '陪产假'),('f', '丧假'),('o', '公出'),
                                        ('b', '调休'),('e', '其他')
                                        )
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {String} data 申请编号
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': 'SUCCESS',
            'data': 'user_13_2019-10-29',
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        supervisor_id = request.POST.get('supervisor_id')
        start_time = request.POST.get('start_time')
        end_time = request.POST.get('end_time')
        content = request.POST.get('content')
        application_type = request.POST.get('type') 

        # 2 校验参数
        try:
            supervisor_id = int(supervisor_id)
        except Exception:
            raise ParaPatternException(error=u'请输入正确格式的审核人id')
        
        try:
            supervisor = User.objects.get(id=supervisor_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'审核人不存在')

        try:
            start_time = datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            raise ParaPatternException(error=u'请输入"%Y-%m-%d %H:%M:%S"格式的开始时间')
        except TypeError:
            raise ParaPatternException(error=u'请输入"%Y-%m-%d %H:%M:%S"格式的开始时间')

        try:
            end_time = datetime.datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            raise ParaPatternException(error=u'请输入"%Y-%m-%d %H:%M:%S"格式的开始时间')
        except TypeError:
            raise ParaPatternException(error=u'请输入"%Y-%m-%d %H:%M:%S"格式的开始时间')

        if application_type not in self.APP_TYPE:
            raise ParaPatternException(error=u'请选择正确的申请类型')

        # 3.1.储存数据
        application = LeaveApplication.objects.create(
            applicant=user,
            supervisor=supervisor,
            start_time=start_time,
            end_time=end_time,
            content=content,
            leave_type=application_type,
        )
        # 3.2.通知审核人
        async_to_sync(self.channel_layer.group_send)(
            'notification_'+str(supervisor_id),
            {
                'type': 'get_notification',
                'message': 'A new leave application need to be confirmed'
            }
        )

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': application.id,
        }
        return JsonResponse(res)
