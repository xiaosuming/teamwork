from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from attendance.models.MakeUp import MakeUp
from attendance.models.Attendance import Attendance
from starxteam.settings import ATTENDANCE
import datetime

# @class MakeUpView 补签视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	考勤视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/07 13:39:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class MakeUpView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.TODAY = datetime.date.today()
        self.FORBIDDEN_START = datetime.datetime(
            self.TODAY.year, self.TODAY.month, self.TODAY.day,
            *ATTENDANCE["WORK_START"]
        )
        self.FORBIDDEN_END = datetime.datetime(
            self.TODAY.year, self.TODAY.month, self.TODAY.day,
            * ATTENDANCE["WORK_END"]
        )
        self.BREAK_TIME = datetime.timedelta(
            **dict(
                zip(["hours", "minutes", "seconds"], ATTENDANCE["BREAK_TIME"])
            )
        )

    """
        @apiName makeup_put
        @api {PUT} /teamwork/attendances/makeups/{{makeup_id}}/ MakeUp: Verify MakeUp Application
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [考勤管理]通过补签申请
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 补签申请id
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 12
        }
    """
    def put(self, request, makeup_id):
        # 1 获取参数
        user = request.user
        makeup_id = int(makeup_id)

        # 2 校验参数
        try:
            makeup = MakeUp.objects.get(id=makeup_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"offset/limit必须是整数")
        if makeup.supervisor_id != user.id:
            raise AuthenticationException(error=u"没有操作权限")
        if makeup.verify:
            raise AuthenticationException(error=u"该申请已通过")

        # 3.修改记录
        sign_date = makeup.sign_date
        in_time = makeup.in_time
        out_time = makeup.out_time
        applicant = makeup.user
        attendance = Attendance.objects.get_or_create(
            user=applicant, sign_date=sign_date)
        with transaction.atomic():
            if in_time:
                attendance.in_time = in_time
                attendance.sign_in_state = 'm'
            if out_time:
                attendance.out_time = out_time
                attendance.sign_out_state = 'm'
            attendance.save()
            makeup.verify = True
            makeup.save()

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": makeup.id
        }
        return JsonResponse(res)

    """
        @apiName makeup_delete
        @api {DELETE} /teamwork/attendances/makeups/{{makeup_id}}/ MakeUp: Callback MakeUp
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [考勤管理]撤回补签申请
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目简述列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "makeup_id": 12
            }
        }
    """
    def deletet(self, request, makeup_id):
        # 1.获取参数
        user = request.user
        makeup_id = int(makeup_id)

        # 2.校验参数
        try:
            makeup = MakeUp.objects.get(id=makeup_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"offset/limit必须是整数")
        if makeup.user_id != user.id:
            raise AuthenticationException(error=u"没有操作权限")
        if makeup.verify:
            raise  AuthenticationException(error=u"申请已通过")

        # 3.删除补签申请
        makeup.delete()

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "makeup_id": makeup_id
            }
        }
        return JsonResponse(res)
