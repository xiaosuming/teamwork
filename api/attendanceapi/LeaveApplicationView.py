from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from api.mixin.LeaveApplicationMixin import LeaveApplicationMixin
from attendance.models.LeaveApplication import LeaveApplication
import datetime

# @class LeaveApplicationView 请假申请视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	请假申请视图
#   method:PUT, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/06 15:36:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class LeaveApplicationView(View, LeaveApplicationMixin):
    APP_TYPE, APP_CHAR = zip(*LeaveApplication.TYPE_CHOICES)

    """
        @apiName leave_application_put
        @api {DELETE} /teamwork/attendances/leave_applications/{{application_id}}/ 
        LeaveApplication: Callback Leave Application
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [请假申请管理]撤回请假申请
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {String} data 审核结果 
        @apiSuccessExample {json} 返回样例:
        {
            'code': 0,
            'msg': 'SUCCESS',
        }
    """
    def delete(self, request, application_id):
        # 1 获取参数
        user = request.user
        application_id = int(application_id)

        # 2 校验参数
        try:
            application = LeaveApplication.objects.get(id=application_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的申请不存在")
        if application not in self.applied_applications(user):
            raise AuthenticationException(error=u"没有操作权限")
        if application.start_time < datetime.datetime.now():
            raise AuthenticationException(error=u"申请已生效，不能撤回")

        # 3 撤回申请
        application.delete()

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }
        return JsonResponse(res)
