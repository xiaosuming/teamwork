from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from attendance.models.MakeUp import MakeUp
from attendance.models.Attendance import Attendance
from user.models import User
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import datetime

# @class MakeUpView 补签视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	考勤视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/01/07 10:03:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class MakeUpsView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName makeups_get
        @api {GET} /teamwork/attendances/makeups/ MakeUp: Get MakeUp Applications
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [考勤管理]获取补签记录
        @apiParam {Bool} supervise 提交给自己的补签申请, default: false
        @apiParam {Int} offset 记录起点, default 0
        @apiParam {Int} limit 记录条数, default 8
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 补签申请列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'id': 1,
                'username': '子房',
                'supervisor': '荀子',
                'sign_date: "2019-10-15",
                'in_time': "8:58:00",
                'out_time': null,
                'comment': '备注',
                'verify': false
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        user = request.user
        supervise = request.GET.get("supervise", False)
        offset = request.GET.get("offset", 0)
        limit = request.GET.get("limit", 8)

        # 2 校验参数
        try:
            offset = int(offset)
            limit = int(limit)
        except ValueError:
            raise ParaPatternException(error=u"offset/limit必须是整数")

        # 2.获取参数
        if supervise:
            makeups = MakeUp.objects.filter(supervisor=user)[offset: offset+limit]
        else:
            makeups = MakeUp.objects.filter(user=user)[offset: offset+limit]

        res_data = list()
        for makeup in makeups:
            info = {
                "id": makeup.id,
                "username": makeup.user.username,
                "supervisor": makeup.supervisor.username,
                "sign_date": makeup.sign_date.isoformat,
                "in_time": makeup.in_time.strftime("%H:%M:%S"),
                "out_time": makeup.out_time.strftime("%H:%M:%S"),
                "comment": makeup.comment,
                "verify": makeup.verify,
            }
            res_data.append(info)

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": res_data
        }
        return JsonResponse(res)

    """
        @apiName makeups_post
        @api {POST} /teamwork/attendances/makeups/ MakeUp: Apply MakeUp
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [考勤管理]提交补签申请
        @apiParam {Int} supervisor_id 申请人id
        @apiParam {String} sign_date 补签日期字符串, '2020-01-02'
        @apiParam {String} in_time 签到日期, '09:00:00'
        @apiParam {String} out_time 签退日期, '18:00:00'
        @apiParam {String} comment 备注
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 不前申请id
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "makeup_id": 12,
            }
        }
    """
    def post(self, request):
        # 1.获取参数
        user = request.user
        supervisor_id = request.POST.get("supervisor_id")
        sign_date = request.POST.get("sign_date")
        in_time = request.POST.get("in_time")
        out_time = request.POST.get("out_time")
        comment = request.POST.get("comment")

        # 2.校验参数
        if not supervisor_id:
            raise ParaMissingException(error=u"没有选择审核人")
        if not User.objects.filter(id=supervisor_id).count():
            raise UnknowResourcesException(error=u"审核人不存在")

        if not sign_date:
            raise ParaMissingException(error=u"没有签到日期")
        try:
            sign_date = datetime.datetime.fromisoformat(sign_date)
        except TypeError:
            raise ParaPatternException(error=u"请输入iso格式'2020-01-01'的日期字符串")
        except ValueError:
            raise ParaPatternException(error=u"请输入iso格式'2020-01-01'的日期字符串")

        if not (in_time or out_time):
            raise ParaMissingException(error=u"签到时间、签退时间至少提交一个")
        if in_time:
            try:
                in_time = datetime.time.fromisoformat(in_time)
                hour, minute, second = in_time.hour, in_time.minute, in_time.second
                in_time = datetime.datetime(
                    sign_date.year, sign_date.month, sign_date.day,
                    hour, minute, second,
                )
            except ValueError:
                raise ParaPatternException(error=u"请输入'HH:MM:SS'格式的时间字符串")
            # 检查是否有冲突的补签申请
            if MakeUp.objects.filter(
                    user=user, sign_date=sign_date, in_time__exact=None
            ).count():
                raise AuthenticationException(error=u"已有同一日期的补签申请")
            # 检查是否已签到
            if Attendance.objects.filter(user=user, sign_date=sign_date) \
                    .exclude(in_time=None).count():
                raise AuthenticationException(error=u"已签到，不能补签")
        if out_time:
            try:
                out_time = datetime.time.fromisoformat(out_time)
                hour, minute, second = out_time.hour, out_time.minute, out_time.second
                out_time = datetime.datetime(
                    sign_date.year, sign_date.month, sign_date.day,
                    hour, minute, second,
                )
            except ValueError:
                raise ParaPatternException(error=u"请输入'HH:MM:SS'格式的时间字符串")
            # 检查是否有冲突的补签申请
            if MakeUp.objects.filter(
                    user=user, sign_date=sign_date, out_time__exact=None
            ).count():
                raise AuthenticationException(error=u"已有同一日期的补签申请")
            # 检查是否已签退
            if Attendance.objects.filter(user=user, sign_date=sign_date) \
                    .exclude(out_time=None).count():
                raise AuthenticationException(error=u"已签退，不能补签")

        # 3.1.新建补签申请
        makeup = MakeUp.objects.create(
            user=user, supervisor_id=supervisor_id, sign_date=sign_date,
            in_time=in_time, out_time=out_time, comment=comment
        )
        # 3.2.通知审核人
        channel_layer = get_channel_layer()
        message = u"{}向您提交了补签申请".format(user.username)
        async_to_sync(channel_layer.group_send)(
            "notification_" + str(supervisor_id),
            {
                'type': 'get_notification',
                'message': message,
            }
        )

        # 4.返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "makeup_id": makeup.id
            }
        }
        return JsonResponse(res)
