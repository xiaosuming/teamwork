from django.db import transaction
from attendance.models.Attendance import Attendance
from attendance.models.LeaveApplication import LeaveApplication
from user.models import User
from starxteam.settings import ATTENDANCE
import datetime


TODAY = datetime.date.today()
NOW = datetime.datetime.now()
WORK_END = datetime.datetime(
    TODAY.year, TODAY.month, TODAY.day, *ATTENDANCE["WORK_END"]
)


def check_sign_in():
    """
    12点签到检查
    """
    # 获取所有用户
    users = User.objects.filter(is_active=1)
    with transaction.atomic():
        for user in users:
            attendance, created = Attendance.objects.get_or_create(
                user=user, sign_date=TODAY,
            )
            if created:
                # 当前用户未签到，检查是否有请假记录
                application = LeaveApplication.objects.filter(
                    applicant=user,
                    start_time__lte=NOW,
                    end_time__gte=NOW,
                    approved='a'
                ).first()
                if application:  # 有请假记录则记录为请假
                    attendance.sign_in_state = 's'  # 正常签到状态
                    if application.leave_type == 'o':  # 公出
                        attendance.state = 'o'
                    if application.leave_type == 'b':  # 调休
                        attendance.state = 'b'
                    else:  # 请假
                        attendance.state = 'l'
                    attendance.application = application
                else:  # 未签到，未请假，漏签
                    attendance.state = 'm'  # 漏签
                    attendance.sign_in_state = 'l'  # 迟到
                attendance.save()


def check_sign_out():
    """
    零点签退检查
    """
    # 获取所有用户
    users = User.objects.filter(is_active=1)
    with transaction.atomic():
        for user in users:
            sign_date = TODAY-datetime.timedelta(days=1)
            attendance, _ = Attendance.objects.get_or_create(
                user=user, sign_date=sign_date
            )
            if not attendance.out_time:
                # 当前用户未签退，检查是否有结束时间晚于当日下班时间的请假记录
                application = LeaveApplication.objects.filter(
                    applicant=user,
                    start_time__lte=WORK_END,
                    end_time__gte=WORK_END,
                    approved='a'
                ).first()
                if application:  # 有请假记录则记录为请假
                    attendance.sign_out_state = 's'  # 正常签退状态
                    if application.leave_type == 'o':  # 公出
                        attendance.state = 'o'
                    if application.leave_type == 'b':  # 调休
                        attendance.state = 'b'
                    else:  # 请假
                        attendance.state = 'l'
                    attendance.application = application
                else:  # 未签到，未请假，漏签
                    attendance.state = 'm'
                    attendance.sign_out_state = 'l'  # 早退
                attendance.save()
