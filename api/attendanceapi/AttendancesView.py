from django.views.generic import View
from django.http import JsonResponse
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from attendance.models.Attendance import Attendance
from starxteam.settings import ATTENDANCE
import datetime


# @class AttendiancesView 考勤视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	考勤视图
#   method:GET, POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/10/29 16:24:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class AttendancesView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.TODAY = datetime.date.today()
        self.FORBIDDEN_START = datetime.datetime(
            self.TODAY.year, self.TODAY.month, self.TODAY.day,
            *ATTENDANCE["WORK_START"]
        )
        self.FORBIDDEN_END = datetime.datetime(
            self.TODAY.year, self.TODAY.month, self.TODAY.day,
            *ATTENDANCE["WORK_END"]
        )
        self.BREAK_TIME = datetime.timedelta(
            **dict(
                zip(["hours", "minutes", "seconds"], ATTENDANCE["BREAK_TIME"])
            )
        )

    """
        @apiName attendancesGet
        @api {GET} /teamwork/attendances/ Attendance: Get Attendances
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [考勤管理]获取考勤记录
        @apiParam {Int} offset 记录起点, default 0
        @apiParam {Int} limit 记录条数, default 8
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目简述列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                'id': 1,
                'username': 子房,
                'sign_date: "2019-10-15",
                'in_time': "8:58:00",
                'out_time': '18:02:00',
                'state': '正常',
                'overtime': '3h',  加班时长
                'location': '打卡地点,
                'comment': '备注',
                'official_out': false, 公出
                },
            ]
        }
    """
    def get(self, request):
        # 1 获取参数
        user = request.user
        offset = request.GET.get("offset", 0)
        limit = request.GET.get("limit", 8)

        # 2 参数校验
        try:
            offset = int(offset)
            limit = int(limit)
        except ValueError:
            raise ParaPatternException(error=u"offset/limit均为整数")

        # 3 获取数据
        record_list = list()
        sign_records = Attendance.objects.filter(user=user).order_by(
            '-sign_date')[offset: offset + limit]
        for sign_record in sign_records:
            info = {
                "id": sign_record.id,
                "username": sign_record.user.username,
                "sign_date": sign_record.sign_date,
                "in_time": sign_record.in_time.strftime("%H:%M:%S") \
                    if sign_record.in_time else sign_record.get_sign_in_state_display(),
                "out_time": sign_record.out_time.strftime("%H:%M:%S") \
                    if sign_record.out_time else sign_record.get_sign_out_state_display(),
                "state": sign_record.get_state_display(),
                "comment": sign_record.comment,
                "official_out": sign_record.state == 'o',
            }
            overtime = 0
            if info["in_time"]:
                overtime = sign_record.out_time - self.FORBIDDEN_END if sign_record.out_time else None
                overtime = int(overtime.seconds / 3600) if overtime and overtime.days >=0 else 0
            info['overtime'] = overtime
            if info["state"] in ("正常", "外勤"):
                info["location"] = info["state"]
            else:
                info["location"] = None
            record_list.append(info)

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'data': record_list,
        }
        return JsonResponse(res)

    """
        @apiName attendancesPost
        @api {POST} /teamwork/attendances/ Attendance: Sign In / Sign Out
        @apiGroup Attendances
        @apiVersion 0.0.1
        @apiDescription [考勤管理]签到、签退
        @apiParam {String} option 考勤操作 ("in", "out")
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 项目简述列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "state": "迟到",
            }
        }
    """

    def post(self, request):
        # 1 获取参数
        user = request.user
        option = request.POST.get('option')
        time_now = datetime.datetime.now()

        # 2 校验参数
        if not option:
            raise ParaMissingException(error=u'考勤操作选项为空')

        if option not in ('in', 'out'):
            raise ParaPatternException(error=u'请输入正确的考勤操作选项: "in" or "out"')

        # 3 处理数据
        sign_record, _ = Attendance.objects.get_or_create(
            user=user, sign_date=self.TODAY
        )
        sign_record.state = 's'
        if option == 'in':  # 签到
            sign_state = '已签到'
            if not sign_record.in_time:  # 当日签到记录中没有签到时间
                sign_record.sign_in_state = 's'
                sign_record.in_time = time_now
                if time_now > self.FORBIDDEN_START:  # 迟到
                    sign_record.state = 'l'
                    sign_record.sign_in_state = 'l'
                    sign_state = u"迟到"
        else:  # 签退
            sign_state = u'已签退'
            if not sign_record.out_time:  # 当日签到记录中没有签退记录
                sign_record.out_time = time_now
                sign_record.sign_out_state = 's'
                if time_now < self.FORBIDDEN_END:  # 早退
                    if sign_record.state == 'l':  # 迟到/早退
                        sign_record.state = 'b'
                    else:
                        sign_record.state = 'e'
                    sign_record.sign_out_state = 'e'
                    sign_state = u"早退"
        sign_record.save()

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'date': {
                'state': sign_state
            }
        }

        return JsonResponse(res)
