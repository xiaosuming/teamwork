from django.views.generic import View
from django.http import JsonResponse
from django.db.transaction import atomic
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.filt_valid_id import filt_valid_id
from utils.caculate_event_time import calculate_event_time
from utils.AgendaRemind import AgendaRemind
from agenda.models.Event import Event
from api.agendaapi.EventsUtils import events_filter
from starxteam.settings import SQL_CONFIG
import datetime
import uuid

# @class EventsView 日程视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	日程视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/11/1 11:16:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


REPEAT_TYPE = ('n', 'd', 'w', 'm', 'a')
REMIND_TYPE = ('f', 'h', 'd', 'c')


class EventsView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @api {GET} /teamwork/agenda/events/ Events:GetEvents
        @apiGroup Agenda
        @apiName GetEvents
        @apiVersion 0.0.1
        @apiDescription [日程管理]获取指定时间段的日程
        @apiParam {Date} start_time 查询开始时间 "YYYY-MM-DD"
        @apiParam {Date} end_time 查询结束时间 "YYYY-MM-DD"
        @apiParam {Bool} need_event 显示日程
        @apiParam {Bool} need_task 显示任务
        @apiParam {Bool} need_project 显示项目
        @apiParam {List} state 状态((o, 未完成), (i, 审核中), (c, 已完成))
        @apiParam {Bool} self_direct 负责的
        @apiParam {Bool} self_participant 参与的
        @apiParam {Bool} self_supervise 审核的
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 日程列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "events": [
                    {
                    'id': 1,
                    'content': '送快递',
                    'start_time': '2019-10-15 11:00:00',
                    'rate': 1,
                    },
                    {
                    'id': 2,
                    'content': '送外卖',
                    'start_time': '2019-10-15 11:01:00',
                    'rate': 2,
                    },
                ]
            }
        }
    """
    def get(self, request):
        # 1.获取参数
        user = request.user
        time_1 = request.GET.get('start_time')
        time_2 = request.GET.get('end_time')
        need_event = True if request.GET.get('need_event') == 'true' else False
        need_task = True if request.GET.get('need_task') == 'true' else False
        need_project = True if request.GET.get('need_project') == 'true' else False
        state = request.GET.get('state')
        self_direct = True if request.GET.get('self_direct') == 'true' else False
        self_participant = True if request.GET.get('self_participant') == 'true' else False
        self_supervise = True if request.GET.get('self_supervise') == 'true' else False

        # 2.校验参数
        try:
            time_1 = datetime.date.fromisoformat(time_1)
            time_2 = datetime.date.fromisoformat(time_2)
        except ValueError and TypeError:
            raise ParaPatternException(error=u"请输入%s格式的查询时间" % 'MMMM-YY-DD')

        events = events_filter(time_1, time_2, user, user, need_event,
                                     need_task, need_project, state,
                                     self_direct, self_participant,
                                     self_supervise)

        # 4.返回应答
        res_data = {
            "events": events
        }
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": res_data
        }
        return JsonResponse(res)

    """
        @api {POST} /teamwork/agenda/events/ Events:CreateEvent
        @apiGroup Agenda
        @apiName PostEvents
        @apiVersion 0.0.1
        @apiDescription [日程管理]创建日程
        @apiParam {DateTime} start_time 日程开始时间
        @apiParam {String} content 日程内容
        @apiParam {List} participant 参与人 [1, 3, 5, 7, 9]
        @apiParam {String} repeat_type 重复类型 (('n', '永不'), ('d', '按天'), ('w', '按周'), ('m', ' 按月'), ('a', '按年'))
        @apiParam {Date} expiration 重复截止日期
        @apiParam {String} remind_type 提醒类型 (('f', '前五分钟'), ('h', '前半小时'), ('d', '前一天'), ('c', '自定义'))
        @apiParam {DataTime} remind_time 提醒时间
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def post(self, request):
        # 1 获取参数
        user = request.user
        content = request.POST.get('content')
        start_time = request.POST.get('start_time')
        participant_ids = request.POST.get('participant', [])
        repeat_type = request.POST.get('repeat_type')
        expiration = request.POST.get('expiration')
        remind_type = request.POST.get('remind_type')
        remind_time = request.POST.get('remind_time')

        # 2.记录日程
        try:
            start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
        except Exception:
            raise ParaPatternException(error=u'请输入符合%s格式的开始时间' % "'%Y-%m-%d %H:%M:%S'")

        if repeat_type not in REPEAT_TYPE:
            raise ParaPatternException(error=u'请输入正确的重复类型')

        if repeat_type == 'n':
            expiration = start_time
        else:
            try:
                expiration = datetime.date.fromisoformat(expiration)
            except Exception:
                raise ParaPatternException(error=u'请输入符合%s格式的截止日期' % "'%Y-%m-%d'")
            else:
                if expiration < start_time.date():
                    raise ParaPatternException(error=u'截止日期早于开始日期')

        try:
            participant_ids = eval(participant_ids)
        except Exception:
            raise ParaPatternException(error=u'请输入正确格式的参与人ID')
        else:
            participant_ids = filt_valid_id(participant_ids)[0]

        if remind_type == 'c':
            try:
                remind_time = datetime.datetime.strptime(remind_time, "%H:%M:%S")
            except Exception:
                raise ParaPatternException(error=u'请输入符合%s格式的自定义提醒时间' % "'%H:%M:%S'")

        # 3.1.设置日程
        event_u_id = set_event(
            creator=user, content=content, start_time=start_time, expiration=expiration, repeat_type=repeat_type,
            remind_type=remind_type, remind_time=remind_time, participant_ids=participant_ids
        )

        # 3.2.设置任务提醒
        # 3.2.1.任务存储路径
        job_store_url = "mysql://{}:{}@{}:{}/{}".format(
            SQL_CONFIG["USER"], SQL_CONFIG["PASSWORD"], SQL_CONFIG["HOST"],
            SQL_CONFIG["PORT"], SQL_CONFIG["NAME"])
        agenda_remind = AgendaRemind(job_store_url)
        # 3.2.2.存储任务
        participant_ids.append(user.id)
        agenda_remind.set_remind(
            start_time, expiration, participant_ids,
            content, event_u_id, repeat_type, remind_type, remind_time)

        # 4 返回应答
        res = {
            'code': 0,    
            'msg': 'SUCCESS',
        }
        return JsonResponse(res)


def set_event(creator, content, start_time, expiration, repeat_type,
              remind_type, remind_time, participant_ids, project_id=None, task_id=None):
    # 1.校验参数
    if not content:
        raise ParaMissingException(error=u'请输入日程内容')

    if repeat_type not in REPEAT_TYPE:
        raise ParaPatternException(error=u'请输入正确的重复类型')

    if type(expiration) is datetime.datetime:
        expiration = expiration.date()

    if remind_type not in REMIND_TYPE:
        raise ParaPatternException(error=u'请输正确的提醒类型')

    if remind_type == 'c' and type(remind_time) is str:
        try:
            remind_time = datetime.time.fromisoformat(remind_time)
        except Exception:
            raise ParaPatternException(error=u'请输入符合%s格式的自定义提醒时间' % "'%H:%M:%S'")

    # 2. 新建日程
    # 2.1.计算所有需要添加的日期
    start_time_list = calculate_event_time(
        start_time, repeat_type, expiration, remind_type, remind_time)

    # 2.2.生成唯一标识符
    now_str = datetime.datetime.now().isoformat()
    event_u_id = ''.join(str(
        uuid.uuid5(uuid.NAMESPACE_DNS, (now_str + str(creator.id)))
    ).split('-'))

    # 2.3.新建日程
    for event_time in start_time_list:
        with atomic():
            # 2.3.1.存入数据库
            event_obj = Event.objects.create(
                creator=creator,
                content=content,
                start_time=event_time,
                repeat_type=repeat_type,
                expiration=expiration,
                remind_type=remind_type,
                uuid=event_u_id,
                project_id=project_id,
                task_id=task_id
            )

            # 2.3.2.建立参与者和日程的联系
            for participant_id in participant_ids:
                event_obj.participant.add(participant_id)

    return event_u_id
