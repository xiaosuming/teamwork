from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from api.agendaapi.EventsView import EventsView
from api.agendaapi.EventView import EventView
from api.agendaapi.EventRateView import EventRateView
from api.agendaapi.SharedEventsView import SharedEventsView
from api.agendaapi.SharedUsersView import SharedUsersView
from api.agendaapi.VisibleUsersView import VisibleUsersView


app_name = 'agenda'


urlpatterns = [
        url('^events/$', csrf_exempt(EventsView.as_view())),
        url(r'^events/(?P<event_id>\d+)/$', csrf_exempt(EventView.as_view())),
        url(r'^events/rate/$', csrf_exempt(EventRateView.as_view())),
        url(r'^events/share/$', csrf_exempt(SharedEventsView.as_view())),
        url(r'events/share_users/$', csrf_exempt(SharedUsersView.as_view())),
        url(r'events/visible_users/$', csrf_exempt(VisibleUsersView.as_view())),
        ]
