from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.filt_valid_id import filt_valid_id
from utils.AgendaRemind import AgendaRemind, cal_remind_time, update_job_kwargs
from agenda.models.Event import Event
from agenda.models.EventShared import EventShared
import datetime
import json

# @class EventView 日程修改视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	日程视图
#   method:GET, PUT, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/02/13 18:18:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


REPEAT_TYPE = ('n', 'd', 'w', 'm', 'a')
REMIND_TYPE = ('f', 'h', 'd', 'c')


class EventView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.agenda_remind = AgendaRemind()

    """
        @api {GET} /teamwork/agenda/events/{{event_id}}/ Event: Get Event Detail
        @apiGroup Agenda
        @apiName GetEvent
        @apiVersion 0.0.1
        @apiDescription [日程管理]获取指定日程详情
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 日程列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    'id': 1,
                    'content': '送快递',
                    'start_time': '2019-10-15 11:00:00',
                    'remind_type': "前五分钟",
                    'remind_time': "18:07:00",
                    'repeat_type': "按天",
                    'participants': [
                        {
                            "id": 1,
                            "username": "starx"
                        },
                        {
                            "id": 2,
                            "username": "李寻欢"
                        }
                    ]
                },
            ]
        }
    """
    def get(self, request, event_id):
        # 1.获取参数
        user = request.user
        event_id = int(event_id)

        # 2.校验参数
        try:
            event = Event.objects.get(id=event_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请求的日程不存在')

        participants = event.participant.all()
        shared_obj, _ = EventShared.objects.get_or_create(owner=user)
        visible_users = shared_obj.shared.all()

        if user != event.creator and user not in participants and user not in visible_users:
            raise AuthenticationException(error=u'没有访问权限')

        # 3.返回应答
        res_data = {
            "id": event_id,
            "start_time": event.start_time.strftime("%Y-%m-%d %H:%M:%S"),
            "content": event.content,
            "remind_type": event.get_remind_type_display(),
            "repeat_type": event.get_repeat_type_display(),
            "expiration": event.expiration.strftime("%Y-%m-%d"),
            "participants": list(participant for participant in participants.values_list("id", "username")),
        }
        event_u_id = event.uuid
        remind_job = self.agenda_remind.job_scheduler.get_job(job_id=event_u_id)
        try:
            remind_time = remind_job.next_run_time.strftime("%H:%M:%S")
        except AttributeError:
            remind_time = None
        res_data.update({"remind_time": remind_time})

        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": res_data
        }
        return JsonResponse(res)

    """
        @api {PUT} /teamwork/agenda/events/{{event_id}}/ Event: Modify an Event
        @apiGroup Agenda
        @apiName PutEvent
        @apiVersion 0.0.1
        @apiDescription [日程管理]修改日程
        @apiParam {DateTime} start_time 日程开始时间
        @apiParam {DateTime} expiration 日程结束时间
        @apiParam {String} content 日程内容
        @apiParam {List} participant 参与人 [1, 3, 5, 7, 9]
        @apiParam {DataTime} remind_time 提醒时间
        @apiParam {String} repeat_type 重复类型 (('n', '永不'), ('d', '按天'), ('w', '按周'), ('m', ' 按月'), ('a', '按年'))
        @apiParam {String} remind_type 提醒类型 (('f', '前五分钟'), ('h', '前半小时'), ('d', '前一天'), ('c', '自定义'))
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request, event_id):
        # 1 获取参数
        user = request.user
        event_id = int(event_id)
        put_data = json.loads(request.body)
        content = put_data.get('content')
        start_time = put_data.get('start_time')
        expiration = put_data.get('expiration')
        participant_ids = put_data.get('participant')
        repeat_type = put_data.get('repeat_type')
        remind_type = put_data.get('remind_type')
        remind_time = put_data.get('remind_time')

        # 2 校验参数
        try:
            event = Event.objects.get(id=event_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u'请求的日程不存在')
        if user != event.creator:
            raise AuthenticationException(error=u"没有修改权限")
        if event.task_id is not None or event.project_id is not None:
            raise AuthenticationException(error=u"任务或项目事件不可直接修改")

        # 任务原参数
        # 原有参与者id
        former_par_ids = set(event.participant.all().values_list('id', flat=True))
        # 初始化任务参数
        event_kwargs = {
            "participant_ids": former_par_ids,
        }

        if start_time:
            try:
                start_time = datetime.datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
            except Exception:
                raise ParaPatternException(error=u'请输入符合%s格式的开始时间' % "'%Y-%m-%d %H:%M:%S'")
            else:
                if start_time < datetime.datetime.now():
                    raise ParaPatternException(error=u'开始时间晚于当前时间')
            event.start_time = start_time
        else:
            start_time = event.start_time
        event_kwargs.update({"start_date": start_time})

        if expiration:
            try:
                expiration = datetime.date.fromisoformat(expiration)
            except Exception:
                raise ParaPatternException(error=u'请输入符合%s格式的截止日期' % "'%Y-%m-%d'")
            else:
                if expiration < start_time.date():
                    raise ParaPatternException(error=u'截止日期早于开始日期')
            event.expiration = expiration
            event_kwargs.update({"end_date": expiration})
        else:
            expiration = event.expiration
        event_kwargs.update({"end_time": expiration})

        if content:
            event.content = content
        else:
            content = event.content
        event_kwargs.update({"content": content})

        if repeat_type:
            if repeat_type not in REPEAT_TYPE:
                raise ParaPatternException(error=u'请输入正确的重复类型')
            event.repeat_type = repeat_type
        else:
            repeat_type = event.repeat_type
        event_kwargs.update({"repeat_type": repeat_type})

        if remind_type:
            if remind_type not in REMIND_TYPE:
                raise ParaPatternException(error=u'请输正确的提醒类型')
            if remind_type == 'c':
                try:
                    remind_time = datetime.time.fromisoformat(remind_time)
                except Exception:
                    raise ParaPatternException(error=u'请输入符合%s格式的自定义提醒时间' % "'%H:%M:%S'")
                event.remind_time = remind_time
            event.remind_type = remind_type
            remind_time = cal_remind_time(remind_type. start_time, remind_time)
        else:
            remind_type = event.remind_type
            if remind_type == 'c':
                event_u_id = event.uuid
                remind_job = self.agenda_remind.job_scheduler.get_job(job_id=event_u_id)
                try:
                    remind_time = remind_job.next_run_time.strftime("%H:%M:%S")
                except AttributeError:
                    remind_time = None
        remind_time = cal_remind_time(remind_type, start_time, remind_time)
        event_kwargs = update_job_kwargs(repeat_type, remind_time, event_kwargs)

        if participant_ids:
            participant_ids = set(filt_valid_id(participant_ids)[0])
            to_remove = former_par_ids.difference(participant_ids)
            to_add = participant_ids.difference(former_par_ids)
            # 修改事件参与人
            event.participant.remove(user.objects.filter(id__in=to_remove))
            event.participant.add(user.objects.filter(id__in=to_add))
            event_kwargs.update({"participant_ids": participant_ids})

        # 3 更新事件
        # 3.1.更新事件
        event.save()
        # 3.2.更新日程提醒
        job_id = event.uuid
        self.agenda_remind.remove_remind(job_id=job_id)
        self.agenda_remind.set_remind(**event_kwargs)

        # 4 返回应答
        res = {
            'code': 0,    
            'msg': 'SUCCESS',
        }

        return JsonResponse(res)

    """
        @api {DELETE} /teamwork/agenda/event/{{event_id}}/ Event: Delete an Event
        @apiGroup Agenda
        @apiName DeleteEvent
        @apiVersion 0.0.1
        @apiDescription [日程管理]删除日程
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request, event_id):
        # 1 获取参数
        user = request.user
        event_id = int(event_id)

        # 2.删除日程
        delete_event(user, event_id, self.agenda_remind)

        # 3.返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }
        return JsonResponse(res)


def modify_event(uuid, **kwargs):
    Event.objects.filter(uuid=uuid).update(kwargs)


def delete_event(user, event_id, agenda_remind):
    """
    删除日程
    :param user: 操作用户, user object
    :param event_id: 日程id
    :param agenda_remind: 日程提醒类
    :return:
    """
    # 1 校验参数
    try:
        event = Event.objects.get(id=event_id)
    except ObjectDoesNotExist:
        raise UnknowResourcesException(error=u'请求的日程不存在')
    if user != event.creator:
        raise AuthenticationException(error=u"没有修改权限")

    # 2.1.删除提醒
    agenda_remind.remove_remind(event.uuid)
    # 2.2.删除事件
    event.delete()
