from django.views.generic import View
from django.http import JsonResponse
from agenda.models.EventShared import EventShared

# @class SharedEventsView 分享用户视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	日程视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/11/1 11:16:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class SharedUsersView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @api {GET} /teamwork/agenda/events/share_users/ SharedEvents:Get_Users_Events_Shared_With
        @apiGroup Agenda
        @apiName GetSharedUsers
        @apiVersion 0.0.1
        @apiDescription [日程管理]获取分享的用户
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 日程列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "users": [
                    {
                    'id': 1,
                    'username': '送快递',
                    },
                    {
                    'id': 2,
                    'username': '送外卖',
                    },
                ]
            }
        }
    """
    def get(self, request):
        # 1.获取参数
        user = request.user

        # 2.获取分享的用户
        shared_obj, _ = EventShared.objects.get_or_create(owner=user)
        visible_users = shared_obj.shared.all().values_list("id", "username")

        # 4.返回应答
        users = [user_info for user_info in visible_users]
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "users": users
            }
        }
        return JsonResponse(res)
