from django.db.models import Q
from agenda.models.Event import Event
from operator import itemgetter
from utils.errorcode.ParaPatternException import ParaPatternException
import datetime


def events_filter(time_1, time_2, request_user, check_user, need_event=True,
                  need_task=True, need_project=True, state='o',
                  self_direct=True, self_participant=True, self_supervise=True):
    """
    筛选符合条件的日历事件
    :param time_1: 开始或结束时间
    :param time_2: 开始或结束时间
    :param request_user: 发起请求的用户对象
    :param check_user:查询的用户
    :param need_event: 显示日程
    :param need_task: 显示任务
    :param need_project: 显示项目
    :param state: 任务或项目的状态
    :param self_direct: 查询对象负责的
    :param self_participant: 查询对象参与的
    :param self_supervise: 查询对象审核的
    :return: 事件query对象
    """
    # 1.计算起止时间
    start_time, end_time = (time_1, time_2) if time_1 < time_2 else (time_2, time_1)
    end_time += datetime.timedelta(days=1)

    try:
        state = eval(state)
    except Exception:
        raise ParaPatternException(error=u"请输入符合要求的状态列表")

    # 2.1.查询日程
    events_query = Event.objects.filter(
        Q(creator_id=check_user) | Q(participant=check_user),
        start_time__range=(start_time, end_time)
    ).values_list(
        "id", "content", "start_time", "project_id", "task_id", "eventrate__rate",
    ).distinct()

    # 2.2.筛选日程
    if not need_event:
        # 不显示日历
        events_query = events_query.exclude(task_id=None, project_id=None)
    # 2.3.筛选任务
    if not need_task:
        # 不显示任务
        events_query = events_query.filter(task_id=None)
    else:
        events_query = events_query.filter(Q(task_id=None) | Q(task__state__in=state))
        if not self_direct:
            # 移除负责的任务
            events_query = events_query.exclude(task__director=request_user)
        if not self_participant:
            # 移除参与的任务
            events_query = events_query.exclude(task__task_participant__participant=request_user)
        if not self_supervise:
            # 移除审核的任务
            events_query = events_query.exclude(task__supervisor=request_user)
    # 2.4.筛选项目
    if not need_project:
        # 不现实项目
        events_query = events_query.filter(project_id=None)
    else:
        events_query = events_query.filter(Q(project_id=None) | Q(project__state__in=state))
        if not self_direct:
            # 移除负责的任务
            events_query = events_query.exclude(project__director=request_user)
        if not self_participant:
            # 移除参与的任务
            events_query = events_query.exclude(project__projectparticipant__participant=request_user)
        if not self_supervise:
            # 移除审核的任务
            events_query = events_query.exclude(project__supervisor=request_user)

    events = [
        dict(zip(['id', 'content', 'start_time', 'project_id', 'task_id', 'rate'], event)) for event in events_query
    ]
    for event in events:
        event.update({"exe_date": event['start_time'].date()})
    events = sorted(events, key=itemgetter("exe_date", "rate"))

    # 3返回查询对象
    return events
