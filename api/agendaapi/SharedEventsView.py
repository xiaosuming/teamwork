from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.filt_valid_id import filt_valid_id
from agenda.models.EventShared import EventShared
from user.models import User
from api.agendaapi.EventsUtils import events_filter
import datetime
import json

# @class SharedEventsView 分享日程管理视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	日程视图
#   method:GET, POST, PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/11/1 11:16:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class SharedEventsView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @api {GET} /teamwork/agenda/events/share/ SharedEvents:GetEventsSharedWith
        @apiGroup Agenda
        @apiName GetSharedEvents
        @apiVersion 0.0.1
        @apiDescription [日程管理]获取指定用户在指定时间段的日程
        @apiParam {Int} user_id 查看用户id
        @apiParam {Date} start_time 查询开始时间 "YYYY-MM-DD"
        @apiParam {Date} end_time 查询结束时间 "YYYY-MM-DD"
        @apiParam {Bool} need_event 显示日程
        @apiParam {Bool} need_task 显示任务
        @apiParam {Bool} need_project 显示项目
        @apiParam {Str} state 状态((o, 未完成), (i, 审核中), (c, 已完成))
        @apiParam {Bool} self_direct 负责的
        @apiParam {Bool} self_participant 参与的
        @apiParam {Bool} self_supervise 审核的
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {array} data 日程列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "events": [
                    {
                    'id': 1,
                    'content': '送快递',
                    'start_time': '2019-10-15 11:00:00',
                    'rate': 1,
                    },
                    {
                    'id': 2,
                    'content': '送外卖',
                    'start_time': '2019-10-15 11:01:00',
                    'rate': 2,
                    },
                ]
            }
        }
    """
    def get(self, request):
        # 1.获取参数
        user = request.user
        user_id = request.GET.get('user_id')
        time_1 = request.GET.get('start_time')
        time_2 = request.GET.get('end_time')
        need_event = True if request.GET.get('need_event') == 'true' else False
        need_task = True if request.GET.get('need_task') == 'true' else False
        need_project = True if request.GET.get('need_project') == 'true' else False
        state = request.GET.get('state')
        self_direct = True if request.GET.get('self_direct') == 'true' else False
        self_participant = True if request.GET.get('self_participant') == 'true' else False
        self_supervise = True if request.GET.get('self_supervise') == 'true' else False

        # 2.校验参数
        try:
            time_1 = datetime.date.fromisoformat(time_1)
            time_2 = datetime.date.fromisoformat(time_2)
        except ValueError and TypeError:
            raise ParaPatternException(error=u"请输入%s格式的字符串" % 'MMMM-YY-DD')

        try:
            check_user = User.objects.get(id=user_id)
        except ObjectDoesNotExist:
            raise UnknowResourcesException(error=u"请求的用户不存在")

        try:
            shared_users = EventShared.objects.get(owner=check_user).shared.all()
            if user not in shared_users:
                raise AuthenticationException(error=u"没有访问权限")
        except ObjectDoesNotExist:
            raise AuthenticationException(error=u"没有访问权限")

        # 3.获取日历事件
        events = events_filter(time_1, time_2, user, check_user, need_event,
                               need_task, need_project, state,
                               self_direct, self_participant,
                               self_supervise)

        # 4.返回应答
        res_data = {
            "events": events
        }
        res = {
            "code": 0,
            "msg": "SUCCESS",
            "data": res_data
        }
        return JsonResponse(res)

    """
        @api {PUT} /teamwork/agenda/events/share/ SharedEvents:Share Agenda to other
        @apiGroup Agenda
        @apiName Share_Events_with_Other
        @apiVersion 0.0.1
        @apiDescription [日程管理]分享日程
        @apiParam {List} shared_ids 分享的用户id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request):
        # 1 获取参数
        user = request.user
        put_data = json.loads(request.body)
        shared_ids = put_data.get("shared_ids")

        # 2.校验参数
        if type(shared_ids) is not list:
            try:
                shared_ids = eval(shared_ids)
            except Exception:
                raise ParaPatternException(error=u'请输入正确格式的分享ID')
        shared_ids = filt_valid_id(shared_ids)[0]

        # 3.分享
        shared_obj, _ = EventShared.objects.get_or_create(owner=user)
        while user.id in shared_ids:
            shared_ids.remove(user.id)
        shared_obj.shared.add(*shared_ids)

        # 4.返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }

        return JsonResponse(res)

    """
        @api {DELETE} /teamwork/agenda/events/share/ SharedEvents:Stop Sharing Agenda to other
        @apiGroup Agenda
        @apiName Stop_Share_Events_with_Other
        @apiVersion 0.0.1
        @apiDescription [日程管理]停止分享日程
        @apiParam {List} cancelled_user_ids 停止分享的用户id
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def delete(self, request):
        # 1 获取参数
        user = request.user
        # delete_data = json.loads(request.body)
        # cancelled_user_ids = delete_data.get("cancelled_ids")
        cancelled_user_ids = request.GET.get("cancelled_user_ids")

        # 2.校验参数
        try:
            cancelled_user_ids = eval(cancelled_user_ids)
        except Exception:
            raise ParaPatternException(error=u'请输入正确格式的取消分享ID')
        else:
            cancelled_user_ids = filt_valid_id(cancelled_user_ids)[0]

        # 3.分享
        shared_obj, _ = EventShared.objects.get_or_create(owner=user)
        shared_obj.shared.remove(cancelled_user_ids)

        # 4.返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }

        return JsonResponse(res)
