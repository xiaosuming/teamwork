from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, Max, Min
from utils.errorcode.ParaPatternException import ParaPatternException
from utils.errorcode.UnknowResourcesException import UnknowResourcesException
from utils.errorcode.AuthenticationException import AuthenticationException
from utils.filt_valid_id import filt_valid_id
from utils.AgendaRemind import AgendaRemind, cal_remind_time, update_job_kwargs
from agenda.models.Event import Event
from agenda.models.EventRate import EventRate
import datetime
import json

# @class EventRateView 日程顺序修改视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	日程视图
#   method:PUT
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/02/17 11:14:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class EventRateView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @api {PUT} /teamwork/agenda/events/rate/ Event: Modify Events Rate
        @apiGroup Agenda
        @apiName Put_Event_Rate
        @apiVersion 0.0.1
        @apiDescription [日程管理]修改日程顺序
        @apiParam {Str} mod_date 修改的事件的日期,YYYY-MM-DD
        @apiParam {List} rate_ids 按新顺序排列的事件id, [1, 5, 6, 3, 4, 2]
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """
    def put(self, request):
        # 1 获取参数
        user = request.user
        put_data = json.loads(request.body)
        mod_date = put_data.get('mod_date')
        rate_ids = put_data.get('rate_ids')

        # 2.校验参数
        events = Event.objects.filter(
            Q(creator=user) | Q(participant=user),
            id__in=rate_ids,
        )
        if events.count() < len(rate_ids):
            raise AuthenticationException(error=u"没有修改权限")

        for event in events:
            if event.start_time.strftime("%Y-%m-%d") != mod_date:
                raise AuthenticationException(error=u'所选事件不符合指定日期')

        # 3.修改排序
        for event_id in rate_ids:
            event_rate, _ = EventRate.objects.get_or_create(event_id=event_id)
            event_rate.rate = rate_ids.index(event_id)
            event_rate.save()

        # 4 返回应答
        res = {
            'code': 0,
            'msg': 'SUCCESS',
        }

        return JsonResponse(res)
