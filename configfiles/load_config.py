from starxteam.settings import BASE_DIR
import json
import os


class LoadConfig:
    configPath = os.path.join(BASE_DIR, 'configfiles')
    config_file_name = os.environ.get("ConfigPath", "debug")
    # config_file_name = os.environ.get("ConfigPath", "DEV")
    allow_host = ["*"]

    def __init__(self):
        self.SQL = None
        self.REDIS = None
        self.CHANNEL = None
        self.ATTENDANCE = None
        self.HOST = dict()
        self.SSO = dict()
        self.WHITE_LIST = list()
        self.OAUTH_URL = dict()
        self.read_configfile()

    def read_configfile(self):
        # 拼接配置文件路径
        config_file = os.path.join(self.configPath, (self.config_file_name+'.json'))
        # 读取配置文件
        with open(config_file, "r") as f:
            config_json = json.load(f)
        # 数据库配置
        database_conf = config_json['DATABASES']
        self.SQL = database_conf['SQL']  # SQL数据库配置
        self.REDIS = database_conf['REDIS']  # REDIS配置
        # CHANNELS_LAYER配置
        channel = database_conf["CHANNEL"]
        self.CHANNEL = {
            "BACKEND": channel["BACKEND"]
        }
        if 'PASSWORD' in channel.keys():
            channel_host = [
                "redis://:" + channel["PASSWORD"] + "@" + channel["HOST"]
                + ":" + str(channel["PORT"]) + "/" + channel["DB"]
            ]
        else:
            channel_host = [(channel["HOST"], channel["PORT"])]

        self.CHANNEL['HOSTS'] = channel_host

        # 签到时间
        self.ATTENDANCE = config_json["ATTENDANCE"]

        # 系统内其他服务IP
        self.HOST = config_json["HOST"]

        # 单点登录路径
        self.SSO = config_json["SSO"]

        # 开发人员白名单
        self.WHITE_LIST = config_json["WHITE_LIST"]

        # OAUTH路由
        self.OAUTH_URL = config_json["OAUTH_URL"]
