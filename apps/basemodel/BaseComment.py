from django.db import models
from user.models import User


class BaseComment(models.Model):
    """
    进展、交流抽象模型类
    """
    TYPE_CHOICES = (
        ('s', '自我评价'),
        ('a', '审核意见'),
        ('r', '退回理由'),
        ('o', '常规报告'),
    )

    creater = models.ForeignKey(
            to=User,
            on_delete=models.CASCADE,
            verbose_name=u'创建人',
            )

    created_time = models.DateTimeField(
            auto_now_add=True,
            verbose_name=u'创建时间',
            )

    score = models.CharField(
            max_length=1,
            null=True,
            default=None,
            verbose_name=u'评分',
            )

    content = models.CharField(
            max_length=1024,
            null=True,
            verbose_name=u'内容',
            )

    comment_type = models.CharField(
            max_length=1,
            choices=TYPE_CHOICES,
            default='o',
            verbose_name=u'进展类型',
            )

    response_to = models.ForeignKey(
        to="self",
        default=None,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=u"回复对象",
        related_name="response"
    )

    class Meta:
        abstract = True
        ordering = ['-created_time']
