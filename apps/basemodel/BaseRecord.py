from django.db import models
from user.models import User


class BaseRecord(models.Model):
    """
    操作记录抽象模型
    """
    user = models.ForeignKey(
                to=User,
                on_delete=models.CASCADE,
                verbose_name=u'操作人',
            )

    created_time = models.DateTimeField(
                auto_now_add=True,
                verbose_name=u'创建时间',
            )

    record = models.CharField(
                max_length=100,
                verbose_name=u'操作记录',
            )

    class Meta:
        abstract = True
        ordering = ['-created_time']
