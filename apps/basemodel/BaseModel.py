from django.db import models
from user.models import User


class BaseModel(models.Model):
    """
    Base model for Project and Task
    """
    name = models.CharField(
            max_length=200,
            verbose_name=u'名称'
            )

    created_time = models.DateTimeField(
            auto_now_add=True,
            verbose_name=u'创建时间',
            )

    updated_time = models.DateTimeField(
            auto_now=True,
            verbose_name=u'修改时间',
            )

    start_time = models.DateField(
            verbose_name=u'开始时间',
            )

    end_time = models.DateField(
            verbose_name=u'结束时间',
            )

    describe = models.TextField(
            verbose_name=u'描述',
            )

    secret = models.BooleanField(
            default=False,
            verbose_name=u'私密',
            )

    director = models.ForeignKey(
            to=User,
            on_delete=models.PROTECT,
            related_name='+',
            verbose_name=u'负责人',
                    )

    supervisor = models.ForeignKey(
            to=User,
            on_delete=models.PROTECT,
            related_name='+',
            verbose_name=u'审核人',
            )

    class Meta:
        abstract = True,
        ordering = ['created_time']
