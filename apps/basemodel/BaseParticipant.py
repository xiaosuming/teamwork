from django.db import models
from user.models import User


class BaseParticipant(models.Model):
    """
    项目/任务参与人抽象模型类
    """
    participant = models.ForeignKey(
                to=User,
                on_delete=models.CASCADE,
                verbose_name=u'参与人',
            )

    created_time = models.DateTimeField(
                auto_now_add=True,
                verbose_name=u'加入时间',
            )

    class Meta:
        abstract = True
