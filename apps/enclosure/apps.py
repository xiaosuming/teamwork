from django.apps import AppConfig


class EnclosureConfig(AppConfig):
    name = 'apps.enclosure'
