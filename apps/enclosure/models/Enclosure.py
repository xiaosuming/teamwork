from django.db import models


class Enclosure(models.Model):
    """
    Enclosure Model
    """
    name = models.CharField(
            verbose_name=u'文件名',
            max_length=200,
            )

    size = models.IntegerField(
            verbose_name=u'文件大小',
            )

    file_type = models.CharField(
            max_length=20,
            verbose_name=u'文件类型',
            )

    path = models.CharField(
        max_length=256,
        verbose_name=u'存储路径',
    )

    md5 = models.CharField(
        max_length=32,
        verbose_name=u"文件唯一标识符"
    )

    created_time = models.DateTimeField(
            auto_now_add=True,
            verbose_name=u'创建时间',
            )

    class Meta:
        ordering = ['-created_time']
