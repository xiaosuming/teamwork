from utils.get_all_users import get_all_users
import redis


def autoSign():
    userIDs,_ = zip(*get_all_users())
    print(userIDs)
    conn = redis.Redis('127.0.0.1', 6379, db=2, decode_responses=True)

    for userID in userIDs:
        key = 'user_' + str(userID)
        print(key)
        if not conn.hget(key, 'in_time'):
            conn.hset(key, 'in_time', 'Meta')
