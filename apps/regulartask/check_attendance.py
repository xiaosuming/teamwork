from user.models import User
from utils.record_sign import record_sign
import redis
import datetime


def check_attendance():
    today = datetime.datetime.today()
    conn = redis.Redis('127.0.0.1', 6379, db=2, decode_responses=True)
    keys = conn.keys('*')
    for key in keys:
        userID = key[5:]
        print(userID)
        user = User.objects.get(id=userID)
        if conn.hgetall(key)['in_time']=='Meta':
            in_time = None
        else:
            in_time = conn.hgetall(key)['in_time']
        print('in_time:', in_time)

        out_time = None

        record_sign(conn, user, today, in_time, out_time)
