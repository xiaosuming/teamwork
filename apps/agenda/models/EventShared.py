from django.db import models
from user.models import User


class EventShared(models.Model):
    """
    日程参与者模型类
    """
    owner = models.ForeignKey(
                to=User,
                on_delete=models.CASCADE,
                verbose_name=u'用户',
            )

    shared = models.ManyToManyField(
        to=User,
        related_name="get_shared",
        verbose_name=u'分享的用户',
    )

    class Meta:
        verbose_name = u'日程分享'
        verbose_name_plural = verbose_name

