from django.db import models
from user.models import User
from project.models.Project import Project
from task.models.Task import Task
import datetime


class Event(models.Model):
    """
    日程模型类
    """
    REPEAT_TYPE = (
                ('n', u'永不'),
                ('d', u'按天'),
                ('w', u'按周'),
                ('m', u'按月'),
                ('a', u'按年'),
            )

    REMIND_TYPE = (
                ('f', u'前五分钟'),
                ('h', u'前半小时'),
                ('d', u'前一天'),
                ('c', u'自定义'),
            )

    creator = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='created_event',
    )

    content = models.CharField(
                max_length=1024,
                verbose_name=u'日程内容',
            )

    start_time = models.DateTimeField(
                verbose_name=u'日程开始时间',
            )

    repeat_type = models.CharField(
                max_length=1,
                choices=REPEAT_TYPE,
            )

    expiration = models.DateField(
                auto_now_add=True,
                verbose_name=u'重复截止时期',
            )

    remind_type = models.CharField(
                max_length=1,
                choices=REMIND_TYPE,
                verbose_name=u'提醒类型',
            )

    participant = models.ManyToManyField(
        to=User,
        related_name='related_event',
    )

    uuid = models.CharField(
        null=True,
        max_length=32,
        verbose_name=u'日程唯一标识符',
    )

    project = models.ForeignKey(
        to=Project,
        on_delete=models.CASCADE,
        null=True,
        related_name='project_on_agenda'
    )

    task = models.ForeignKey(
        to=Task,
        on_delete=models.CASCADE,
        null=True,
        related_name='task_on_agenda'
    )

    class Meta:
        verbose_name = u'日程'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s' % (self.content, self.start_time)
