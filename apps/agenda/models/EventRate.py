from django.db import models
from agenda.models.Event import Event


class EventRate(models.Model):
    """
    日程参与者模型类
    """
    event = models.ForeignKey(
                to=Event,
                on_delete=models.CASCADE,
                verbose_name=u'关联日程',
            )

    rate = models.PositiveSmallIntegerField(
        null=True,
        verbose_name=u'排名',
    )

    class Meta:
        verbose_name = u'日程参与者'
        verbose_name_plural = verbose_name

