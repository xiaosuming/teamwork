from django.contrib import admin
from agenda.models.Event import Event
from agenda.models.EventRate import EventRate


admin.site.register(Event)
admin.site.register(EventRate)
