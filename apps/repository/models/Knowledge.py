from django.db import models
from repository.models.Topic import Topic
from user.models import User


class Knowledge(models.Model):
    """
    知识点模型类
    """
    creater = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        verbose_name=u'创建者',
    )

    created_time = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u'创建时间',
    )

    topic = models.ForeignKey(
        to=Topic,
        on_delete=models.CASCADE,
        verbose_name=u'所属分类',
    )

    content = models.CharField(
        max_length=128,
        verbose_name=u'知识点内容',
    )

    class Meta:
        verbose_name = u'知识点'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s'%(self.topic.title, self.content)
