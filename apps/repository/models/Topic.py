from django.db import models


class Topic(models.Model):
    """
    知识点分类模型类
    """
    title = models.CharField(
        max_length=20,
        verbose_name=u'知识点类别',
    )

    parent = models.ForeignKey(
        to='self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
        verbose_name=u'父知识点类别',
    )

    depth = models.SmallIntegerField(
        verbose_name=u'节点层级',
    )

    class Meta:
        verbose_name = u'知识点分类'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title
