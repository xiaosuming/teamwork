from django.contrib import admin
from repository.models.Topic import Topic
from repository.models.Knowledge import Knowledge


admin.site.register(Topic)
admin.site.register(Knowledge)
