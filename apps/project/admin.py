from django.contrib import admin
from project.models.Project import Project
from project.models.ProjectRecord import ProjectRecord
from project.models.ProjectParticipant import ProjectParticipant
from project.models.ProjectVisitor import ProjectVisitor
from project.models.ProjectEnclosure import ProjectEnclosure
from project.models.ProjectComment import ProjectComment


admin.site.register(Project)
admin.site.register(ProjectRecord)
admin.site.register(ProjectParticipant)
admin.site.register(ProjectVisitor)
admin.site.register(ProjectEnclosure)
admin.site.register(ProjectComment)
