from django.db import models
from basemodel.BaseRecord import BaseRecord
from project.models.Project import Project


class ProjectRecord(BaseRecord):
    """
    项目操作记录模型类
    """
    project = models.ForeignKey(
            to=Project,
            on_delete=models.CASCADE,
            verbose_name=u'所属项目'
            )

    class Meta:
        verbose_name = u'操作记录'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u"%s  %s  %s"%(self.user.username, self.created_time.strftime('%Y-%m-%d %H:%M:%S'), self.record)
