from django.db import models
from project.models.Project import Project
from basemodel.BaseComment import BaseComment


class ProjectComment(BaseComment):
    """
    项目进展交流模型类
    """
    project = models.ForeignKey(
            to=Project,
            on_delete=models.CASCADE,
            verbose_name=u'所属项目',
            )


    class Meta:
        verbose_name = u'项目进展与交流'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s'%(self.creater.username, self.content)
