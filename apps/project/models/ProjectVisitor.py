from django.db import models
from project.models.Project import Project
from user.models import User


class ProjectVisitor(models.Model):
    """
    项目可见用户模型类
    """
    project = models.ForeignKey(
                to=Project,
                on_delete=models.CASCADE,
                verbose_name=u'项目',
            )

    visitor = models.ForeignKey(
                to=User,
                on_delete=models.CASCADE,
                verbose_name=u'用户',
            )

    class Meta:
        verbose_name = u'项目可见用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return ('%s-%s')%(self.project.name, self.visitor.username)
