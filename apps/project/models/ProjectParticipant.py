from django.db import models
from basemodel.BaseParticipant import BaseParticipant
from project.models.Project import Project


class ProjectParticipant(BaseParticipant):
    """
    项目参与人模型类
    """
    project = models.ForeignKey(
                to=Project,
                on_delete=models.CASCADE,
                verbose_name=u'项目',
            )

    class Meta:
        verbose_name = u'项目参与人'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'{}-{}'.format(self.project.name, self.participant.username)
