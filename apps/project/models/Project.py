from django.db import models
from basemodel.BaseModel import BaseModel


class Project(BaseModel):
    """
    项目模型类
    """
    CLASS_CHOICES = (
                ('d', u'软件开发'),
            )

    STATE_CHOICES = (
                ('a', u'审核中'),
                ('o', u'进行中'),
                ('i', u'验收中'),
                ('c', u'已完成'),
                ('s', u'已取消'),
            )

    project_class = models.CharField(
                max_length=1,
                choices=CLASS_CHOICES,
                default='d',
                verbose_name=u'项目类型',
            )

    state = models.CharField(
                max_length=1,
                choices=STATE_CHOICES,
                default='a',
                verbose_name=u'项目进度',
            )
    

    class Meta:
        verbose_name = u'项目'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u"%s-%s"%(self.name, self.get_state_display())
