from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json


class Consumers(WebsocketConsumer):
    def connect(self):
        # 1.1 服务器收到连接请求，获取scope中的userID
        self.userID = self.scope['url_route']['kwargs']['userID']
        print(self.userID)
        # 1.2 添加示例属性
        self.group_name = 'notification_%s'%self.userID

        # 2 Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )

        # 3 接受连接
        self.accept()  # accept the websocket connection

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

        # Receive Notification
    def get_notification(self, event):
        # 1 get the message
        message = event['message']

        # 2 send message to WebSocket
        self.send(
            text_data=json.dumps(
                {
                    'id': message['id'],
                    'user': message['user'],
                    'work_date': message['work_date'],
                    'date': message['date'],
                    'report_type': message['report_type'],
                    'plain_text': message['plain_text'],
                }
            )
        )

    # Receive Report
    def get_report(self, event):
        # 1 get the message
        message = event['message']

        # 2 send message to WebSocket
        self.send(
            text_data=json.dumps(
                {
                    'id': message['id'],
                    'user': message['user'],
                    'work_date': message['work_date'],
                    'date': message['date'],
                    'report_type': message['report_type'],
                    'plain_text': message['plain_text'],
                }
            )
        )

    # Receive Notification
    def send_remind(self, event):
        # 1 get the message
        message = event['message']

        # 2 send message to WebSocket
        self.send(
            text_data=json.dumps(
                {
                    'id': message['id'],
                    'user': message['user'],
                }
            )
        )
