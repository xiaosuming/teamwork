# myproject/consumer/routing.py

from django.urls import re_path
from channels.auth import AuthMiddlewareStack
from consumer.Consumers import Consumers


websocket_urlpatterns = [
    re_path(r'ws/notifications/(?P<userID>\w+)/$', Consumers), 
]
