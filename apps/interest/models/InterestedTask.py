from django.db import models
from task.models.Task import Task
from user.models import User


#def limited_tasks(user):
#    taskIDs = list(TaskVisitor.objects.filter(visitor=user).values_list('task', flat=True))
#    return {'id__in': taskIDs}


class InterestedTask(models.Model):
    """
    关注任务模型类
    """
    task = models.ForeignKey(
            to=Task,
            on_delete=models.CASCADE,
            verbose_name=u'关注的任务',
            )

    user = models.ForeignKey(
            to=User,
            on_delete=models.CASCADE,
            verbose_name=u'关注人',
            )

    class Meta:
        verbose_name = u'关注任务'
        verbose_name_plural = verbose_name

    def __str__(self):
        return ('%s in interested in the task -- %s')%(self.user.visitor.username, self.task.name)
