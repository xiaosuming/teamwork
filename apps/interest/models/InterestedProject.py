from django.db import models
from project.models.Project import Project
from user.models import User


#def limited_projects(user):
#    projectIDs = list(ProjectVisitor.objects.filter(visitor=user).values_list('project', flat=True))
#    return {'id__in': projectIDs}


class InterestedProject(models.Model):
    """
    关注项目模型类
    """
    project = models.ForeignKey(
            to=Project,
            on_delete=models.CASCADE,
            verbose_name=u'关注的项目',
            )

    user = models.ForeignKey(
            to=User,
            on_delete=models.CASCADE,
            verbose_name=u'关注人',
            )

    class Meta:
        verbose_name = u'关注项目'
        verbose_name_plural = verbose_name

    def __str__(self):
        return ('%s is interested in the project -- %s')%(self.user.visitor.username, self.project.name)
