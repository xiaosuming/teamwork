from django.contrib import admin
from interest.models.InterestedProject import InterestedProject
from interest.models.InterestedTask import InterestedTask


admin.site.register(InterestedProject)
admin.site.register(InterestedTask)
