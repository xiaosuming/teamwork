from django.apps import AppConfig


class PrivategroupConfig(AppConfig):
    name = 'privategroup'
