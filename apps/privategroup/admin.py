from django.contrib import admin
from privategroup.models.PrivateGroup import PrivateGroup
from privategroup.models.PrivateGroupUser import PrivateGroupUser


admin.site.register(PrivateGroup)
admin.site.register(PrivateGroupUser)
