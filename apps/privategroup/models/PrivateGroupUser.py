from django.db import models
from user.models import User
from privategroup.models.PrivateGroup import PrivateGroup


class PrivateGroupUser(models.Model):
    """私有分组关联用户模型类"""
    privategroup = models.ForeignKey(
            to=PrivateGroup,
            on_delete=models.CASCADE,
            verbose_name=u'私有模型',
            )

    user = models.ForeignKey(
            to=User,
            on_delete=models.CASCADE,
            verbose_name=u'组成员',
            )

    class Meta:
        verbose_name = u'私有分组关联模型'
        verbose_name_plural = verbose_name
