from django.db import models
from user.models import User


class PrivateGroup(models.Model):
    """私有分组模型类"""
    user = models.ForeignKey(
            to=User,
            on_delete=models.CASCADE,
            verbose_name=u'创建者',
            )

    name = models.CharField(
            max_length=20,
            verbose_name=u'分组名'
            )

    class Meta:
        unique_together = [['user', 'name']]
        verbose_name = u'私有分组'
        verbose_name_plural = verbose_name
