from django.db import models
from user.models import User


class profile(models.Model):
    MALE = 'm'
    FEMALE = 'f'
    GENDER_CHOICES = (
            (MALE, '男'),
            (FEMALE, '女')
            )

    ID_TYPE_CHOICES = (
        ('id', u'身份证'),    
        ('pa', u'护照'),    
        ('hp', u'港澳居民来往内地通行证'),    
        ('tp', u'台湾居民来往大陆通行证'),
        ('fi', u'外国人永久居留身份证'),
        ('hr', u'港澳台居民居住证'),
    )

    EDUCATION_TYPE = (
        ('b', u'学士'),    
        ('m', u'硕士'),    
        ('d', u'博士'),    
        ('a', u'博士后'),    
        ('e', u'其他'),    
    )
    
    user = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        verbose_name=u'用户',
    )

    gender = models.CharField(
            max_length=1, 
            choices=GENDER_CHOICES, 
            default=MALE,
            verbose_name=u'性别',
        )

    birthday = models.DateField(
        verbose_name=u'出生日期',
        )

    IDtype = models.CharField(
            max_length=2,
            choices=ID_TYPE_CHOICES,
            default='id',
            verbose_name=u'证件类型',
        )

    IDnumber = models.CharField(
            max_length=20,
            verbose_name=u'证件号',
        )

    education = models.CharField(
            max_length=1,
            choices=EDUCATION_TYPE,
            verbose_name=u'学历',
            default='b',
        )

    college = models.CharField(
        max_length=128,
        verbose_name=u'毕业院校',
    )

    class Meta:
        verbose_name = u'用户信息'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.user.username
