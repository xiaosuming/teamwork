from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin
from user.models import User
from user.profile import profile


class CostumeUserAdmin(UserAdmin):
    '''
    自定义admin后台中显示的字段
    '''
    fieldsets = (
            (None, {'fields': ('username', 'password', 'email', 'gender', 'date_joined')}),
            )


admin.site.register(User, CostumeUserAdmin)
admin.site.register(profile)
