from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """自定义用户模型"""

    nickname = models.CharField(
            max_length=256, 
            verbose_name=u'姓名',
        )

    themis_username = models.CharField(
        max_length=256,
        null=True,
        default=None,
        verbose_name=u'云桌面用户名'
    )

    colour = models.CharField(
            max_length=7,
            verbose_name=u'头像颜色',
        )

    class Meta:
        verbose_name = u'用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username
