from django.db import models
from user.models import User


class Reminder(models.Model):
    RELATED_CHOICES = (
        ('P', '项目'),
        ('T', '任务'),
        ('R', '日报'),
        ('E', '其它'),
    )

    user = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        verbose_name=u'用户'
    )

    creator = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name="created_reminder",
        verbose_name=u"创建者",
        null=True,
    )

    related_obj = models.CharField(
        max_length=1,
        choices=RELATED_CHOICES,
        verbose_name=u'关联对象'
    )

    related_id = models.PositiveSmallIntegerField(
        verbose_name=u"关联对象id",
        null=True,
    )

    message = models.TextField(
        verbose_name=u"提醒内容"
    )

    dest_time = models.DateTimeField(
        verbose_name=u"目标时间"
    )

    created_time = models.DateTimeField(
        verbose_name=u"创建时间",
        auto_now_add=True,
    )

    class Meta:
        verbose_name = u'提醒'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s.%s.%s'%(self.user.username, self.message, self.dest_time.isoformat())
