from django.db import models
from user.models import User
from attendance.models.LeaveApplication import LeaveApplication


class Attendance(models.Model):
    """
    考勤记录模型类
    """
    STANDARD = 's'
    EARLY = 'e'
    TARDY = 't'
    BOTH = 'b'
    MISSED = 'm'
    OUTSIDE = 'o'
    LEAVE = 'l'
    BREAK = 'b'
    STATE_CHOICES = (
        (STANDARD, u'正常'),
        (TARDY, u'迟到'),
        (EARLY, u'早退'),
        (BOTH, u'迟到/早退'),
        (MISSED, u'漏签'),
        (OUTSIDE, u'外勤'),
        (LEAVE, u'请假'),
        (BREAK, u'调休')
    )
    SIGN_STATE = (
        ('e', u'早退'),
        ('l', u'迟到'),
        ('m', u'补签'),
        ('s', u'正常'),
    )

    user = models.ForeignKey(
            to=User,
            on_delete=models.CASCADE,
            verbose_name=u'用户'
            )

    sign_date = models.DateField(
            auto_now_add=True,
            verbose_name=u'签到日期',
            )

    in_time = models.DateTimeField(
            null=True,
            verbose_name=u'签到时间',
            )

    sign_in_state = models.CharField(
        null=True,
        max_length=1,
        choices=SIGN_STATE,
        verbose_name=u'签到状态',
        default=None,
    )

    out_time = models.DateTimeField(
            null=True,
            verbose_name=u'签退时间',
            )

    sign_out_state = models.CharField(
        null=True,
        max_length=1,
        choices=SIGN_STATE,
        verbose_name=u'签到状态',
        default=None,
    )

    state = models.CharField(
            max_length=1,
            choices=STATE_CHOICES,
            verbose_name=u'状态',
            )

    comment = models.CharField(
        max_length=128,
        null=True,
        default=None,
        verbose_name=u"备注",
    )

    application = models.ForeignKey(
            to=LeaveApplication,
            on_delete=models.CASCADE,
            null=True,
            default=None,
            verbose_name=u'请假申请',
            )

    class Meta:
        verbose_name = u'考勤记录'
        verbose_name_plural = verbose_name
        unique_together = [['user', 'sign_date']]

    def __str__(self):
        return u'%s-%s' % (self.sign_date, self.user.username)
