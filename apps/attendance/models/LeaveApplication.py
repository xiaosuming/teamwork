from django.db import models
from user.models import User


class LeaveApplication(models.Model):
    """
    请假申请模型类
    """
    COMPASSION = 'c'
    SICK = 's'
    ANNUAL = 'a'
    WEDDING = 'w'
    MATERNITY = 'm'
    PATERNITY = 'p'
    FUNERAL = 'f'
    OFFICIAL = 'o'
    BREAK = 'b'
    ELSE = 'e'

    TYPE_CHOICES = (
        (COMPASSION, u'事假'),
        (SICK, u'病假'),
        (ANNUAL, u'年假'),
        (WEDDING, u'婚假'),
        (MATERNITY, u'产假'),
        (PATERNITY, u'陪产假'),
        (FUNERAL, u'丧假'),
        (OFFICIAL, u'公出'),
        (BREAK, u'调休'),
        (ELSE, u'其他'),
    )

    APPROVED_CHOICES = (
        ('u', u'待审核'),
        ('a', u'已通过'),
        ('d', u'未通过'),
    )

    applicant = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='applicant',
        verbose_name=u'申请人',
        )

    supervisor = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='supervisor',
        verbose_name=u'审核人',
        )

    created_time = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u'申请时间',
        )

    start_time = models.DateTimeField(
        verbose_name=u'开始时间',
        )

    end_time = models.DateTimeField(
        verbose_name=u'结束时间',    
        )

    content = models.CharField(
        max_length=256,
        verbose_name=u'内容',
        )

    leave_type = models.CharField(
        max_length=1,
        choices=TYPE_CHOICES,
        verbose_name=u'类型',
        )

    approved = models.CharField(
        max_length=1,
        default='u',
        choices=APPROVED_CHOICES,
        verbose_name=u"是否批准",
    )

    class Meta:
        verbose_name = u'请假申请'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s-%s'%(self.applicant.username, self.start_time, self.get_leave_type_display())
