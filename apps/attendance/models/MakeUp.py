from django.db import models
from user.models import User


class MakeUp(models.Model):
    """考勤补签模型类"""
    user = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        verbose_name=u"申请人",
        related_name="makeup",
    )

    supervisor = models.ForeignKey(
        to=User,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=u'审核人',
        related_name="makeup_apply",
    )

    sign_date = models.DateField(
        auto_now_add=True,
        verbose_name=u'签到日期',
    )

    in_time = models.DateTimeField(
        null=True,
        verbose_name=u'签到时间',
    )

    out_time = models.DateTimeField(
        null=True,
        verbose_name=u'签退时间',
    )

    comment = models.CharField(
        max_length=128,
        null=True,
        default=None,
        verbose_name=u"备注",
    )

    verify = models.BooleanField(
        default=False,
        verbose_name=u'是否通过',
    )

    class Meta:
        verbose_name = u'补签申请'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s' % (self.user.username, self.sign_date)
