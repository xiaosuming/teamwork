from django.contrib import admin
from attendance.models.Attendance import Attendance
from attendance.models.LeaveApplication import LeaveApplication
from attendance.models.MakeUp import MakeUp


admin.site.register(Attendance)
admin.site.register(LeaveApplication)
admin.site.register(MakeUp)
