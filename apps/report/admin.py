from django.contrib import admin
from report.models.Report import Report
from report.models.ReportUser import ReportUser
from report.models.ReportEnclosure import ReportEnclosure


admin.site.register(Report)
admin.site.register(ReportUser)
admin.site.register(ReportEnclosure)
