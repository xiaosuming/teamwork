from django.db import models
from report.models.Report import Report
from enclosure.models.Enclosure import Enclosure


class ReportEnclosure(models.Model):
    """
    工作总结附件模型类
    """
    name = models.CharField(
        max_length=128,
        verbose_name=u"文件名",
    )

    enclosure = models.ForeignKey(
        to=Enclosure,
        on_delete=models.CASCADE,
        verbose_name=u"附件",
        related_name="attached_report",
    )

    report = models.ForeignKey(
        to=Report,
        on_delete=models.CASCADE,
        verbose_name=u'工作总结',
        related_name="enclosure",
    )

    created_time = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u'创建时间',
    )

    class Meta:
        verbose_name = u'工作总结附件'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s' % (self.report.user.username, self.enclosure.name)
