from django.db import models
from report.models.Report import Report
from user.models import User


class ReportUser(models.Model):
    """工作总结关联用户模型"""
    report = models.ForeignKey(
            to=Report,
            on_delete=models.SET_NULL,
            null=True,
            verbose_name=u'工作总结id'
            )

    related_user = models.ForeignKey(
            to=User,
            on_delete=models.SET_NULL,
            null=True,
            verbose_name=u'关联用户id'
            )

    relation_type = models.CharField(
            max_length=1,
            choices=(
                ('c', u'抄送'),
                ('s', u'审阅'),
            ),
            verbose_name=u'关联类型'
        )

    readed = models.BooleanField(
            default=False,
            verbose_name=u'已读'
            )

    class Meta:
        verbose_name = u'工作总结关联用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s.%s.%s.%s'%(self.report.user.username, self.report.created_time, self.relation_type, self.readed)
