from django.db import models
from user.models import User


class Report(models.Model):
    """工作总结模型类"""
    DAILY = 'd'
    WEEKLY = 'w'
    MONTHLY = 'm'
    TYPE_CHOICES = (
            (DAILY, '日报'),
            (WEEKLY, '周报'),
            (MONTHLY, '月报'),
            )

    user = models.ForeignKey(
            to=User, 
            on_delete=models.SET_NULL,
            null=True,
            related_name='user',
            verbose_name=u'作者',
            )

    created_time = models.DateTimeField(
            auto_now_add=True,
            verbose_name=u'创建时间'
            )

    related_date = models.DateField(
            auto_now_add=True,
            verbose_name=u'对应日期'
            )

    report_type = models.CharField(
            max_length=1,
            choices=TYPE_CHOICES,
            default=DAILY,
            )

    plain_text = models.TextField(
            verbose_name = u'纯文本'    
        )

    rich_text = models.TextField(
            verbose_name = u'富文本'
            )

    diagram = models.TextField(
            verbose_name = u'结构图'
            )

    class Meta:
        verbose_name = u'工作总结'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s.%s.%s'%(self.user.username, self.created_time, self.report_type)
