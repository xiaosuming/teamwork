from django.shortcuts import render
from django.utils.safestring import mark_safe
import json


def index(request):
    return render(request, 'comment/index.html', {})


def room(request, room_name):
    print(request.user)
    return render(request, 'comment/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name)),
        'user': request.user.username,
    })
