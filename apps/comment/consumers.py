from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json


class CommentConsumer(WebsocketConsumer):
    def connect(self):
        # 1.1 服务器收到连接请求，获取scopez中的userID
        self.userID = self.scope['url_route']['kwargs']['userID']
        print(self.userID)
        # 1.2 添加参数
        self.room_group_name = 'reportMsg_%s'%self.userID

        # 2 Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        # 3 接受连接
        self.accept()  # accept the websocket connection

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)  # turn a json to a dict
        message = text_data_json['message']  # get received message

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type':'chat_message',
                'message':message
            }
        )

    # Receive message from group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message  
        }))

    # Receive Report
    def get_report(self, event):
        message = event['message']

        # send message to WebSocket
        self.send(
            text_data=json.dumps(
                {
                    'message': message 
                }
            )
        )
