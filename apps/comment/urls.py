from django.urls import path
from . import CommentView


urlpatterns = [
    path('', CommentView.index, name='index'),
    path('<str:room_name>/', CommentView.room, name='room'),
]
