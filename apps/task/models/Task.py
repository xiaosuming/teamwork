from django.db import models
from basemodel.BaseModel import BaseModel
from project.models.Project import Project


class Task(BaseModel):
    """
    任务模型类
    """
    RATE_CHOICES = (
        ('o', '普通'),
        ('i', '重要'),
        ('e', '紧急'),
        ('u', '重要紧急')
    )

    STATE_CHOICES = (
        ('o', u'未完成'),
        ('i', u'待审核'),
        ('c', u'已完成'),
        ('s', u'已取消'),
    )

    TASK_CLASS_CHOICES = (
        ('o', u'普通任务'),    
        ('t', u'测试任务'),    
        ('d', u'设计任务'),    
        ('f', u'缺陷任务'),    
        ('r', u'需求任务'),    
        ('e', u'其他任务'),    
    )

    project = models.ForeignKey(
            to=Project,
            on_delete=models.SET_NULL,
            verbose_name=u'所属项目',
            null=True,
            related_name='attached_task'
            )

    task_class = models.CharField(
        max_length=1,
        choices=TASK_CLASS_CHOICES,
        default='o',
        verbose_name=u'任务类型',
    )

    rate = models.CharField(
            max_length=1,
            choices=RATE_CHOICES,
            default='n',
            verbose_name=u'优先级',
            )

    state = models.CharField(
            max_length=1,
            choices=STATE_CHOICES,
            default='o',
            verbose_name=u'状态'
            )

    class Meta:
        verbose_name = u'任务'
        verbose_name_plural = verbose_name

    def __str__(self):
        return (u"%s-%s"%(self.name, self.get_state_display()))
