from django.db import models
from user.models import User


class TaskFilter(models.Model):
    creator = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        verbose_name=u"创建者",
        related_name="task_filter",
    )

    name = models.CharField(
        max_length=20,
        verbose_name=u"名称",
    )

    conditions = models.TextField(
        verbose_name=u"筛选条件json"
    )

    class Meta:
        verbose_name = u'任务筛选条件'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
