from django.db import models
from user.models import User
from task.models.Task import Task
from enclosure.models.Enclosure import Enclosure


class TaskEnclosure(models.Model):
    """
    任务评论文档模型类
    """
    user = models.ForeignKey(
        to=User,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=u'上传用户',
    )

    name = models.CharField(
        max_length=128,
        verbose_name=u"文件名",
    )

    enclosure = models.ForeignKey(
        to=Enclosure,
        on_delete=models.CASCADE,
        verbose_name=u"附件",
        related_name="attached_task",
        null=True,
    )

    task = models.ForeignKey(
        to=Task,
        on_delete=models.CASCADE,
        verbose_name=u'所属任务',
        related_name="enclosure",
    )

    created_time = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u'创建时间',
        null=True,
    )

    class Meta:
        verbose_name = u'任务评论附件'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s' % (self.task.name, self.enclosure.name)
