from django.db import models
from task.models.Task import Task
from task.models.SubTask import SubTask
from basemodel.BaseComment import BaseComment


class TaskComment(BaseComment):
    """
    任务进展交流模型类
    """
    task = models.ForeignKey(
            to=Task,
            on_delete=models.CASCADE,
            verbose_name=u'所属任务',
            )

    subtask = models.ForeignKey(
        to=SubTask,
        on_delete=models.CASCADE,
        verbose_name=u'所属子任务',
        null=True,
        default=None,
    )

    class Meta:
        verbose_name = u'任务进展与交流'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s'%(self.creater.username, self.content)
