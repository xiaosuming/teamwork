from django.db import models
from basemodel.BaseRecord import BaseRecord
from task.models.Task import Task


class TaskRecord(BaseRecord):
    """
    任务操作记录模型类
    """
    task = models.ForeignKey(
            to=Task,
            on_delete=models.CASCADE,
            verbose_name=u'从属任务',
            )

    class Meta:
        verbose_name = u'任务操作记录'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u"%s-%s"%(self.record, self.task.name)
