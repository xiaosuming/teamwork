from django.db import models
from task.models.Task import Task
from task.models.TaskParticipant import TaskParticipant


class SubTask(models.Model):
    """
    子任务模型类
    """
    STATE_CHOICES=(
            ('o', '进行中'),
            ('i', '审核中'),
            ('c', '已完成'),
            ('s', '已取消'),
            )

    task = models.ForeignKey(
            to = Task,
            on_delete=models.CASCADE,
            verbose_name=u'所属任务',
            )

    created_time = models.DateTimeField(
            auto_now_add=True,
            verbose_name=u'创建时间',
            )

    participant = models.ForeignKey(
            to=TaskParticipant,
            on_delete=models.SET_NULL,
            null=True,
            verbose_name=u'子任务参与人',
            )

    describe = models.CharField(
            max_length=100,
            verbose_name=u'子任务描述',
            )

    start_time = models.DateField(
            verbose_name=u'开始时间',
            )

    end_time = models.DateField(
            verbose_name=u'结束时间',
            )

    state = models.CharField(
            max_length=1,
            choices=STATE_CHOICES,
            default='o',
            verbose_name=u'状态',
            )

    class Meta:
        verbose_name = u'子任务'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u"%s-%s"%(self.task.name, self.describe)
