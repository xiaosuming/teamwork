from django.db import models
from user.models import User
from task.models.Task import Task


class TaskVisitor(models.Model):
    """
    任务可见用户模型类
    """
    task = models.ForeignKey(
            to=Task,
            on_delete=models.CASCADE,
            verbose_name=u'任务',
            related_name="task_visitor"
            )

    visitor = models.ForeignKey(
            to=User,
            on_delete=models.CASCADE,
            verbose_name=u'可见用户',
            )

    class Meta:
        verbose_name=u'任务可见用法'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s'%(self.task.name, self.visitor.username)
