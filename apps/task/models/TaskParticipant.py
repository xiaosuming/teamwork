from django.db import models
from basemodel.BaseParticipant import BaseParticipant
from task.models.Task import Task


class TaskParticipant(BaseParticipant):
    """
    任务参与人模型类
    """
    task = models.ForeignKey(
            to=Task,
            on_delete=models.CASCADE,
            verbose_name=u'所属任务',
            related_name="task_participant"
            )

    class Meta:
        verbose_name = u'任务参与人'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s-%s'%(self.task.name, self.participant.username)
