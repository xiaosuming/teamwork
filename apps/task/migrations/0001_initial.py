# Generated by Django 2.2.6 on 2020-02-19 09:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CommentEnclosure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, null=True, verbose_name='文件名')),
                ('created_time', models.DateTimeField(auto_now_add=True, null=True, verbose_name='创建时间')),
            ],
            options={
                'verbose_name': '任务评论附件',
                'verbose_name_plural': '任务评论附件',
            },
        ),
        migrations.CreateModel(
            name='SubTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('describe', models.CharField(max_length=100, verbose_name='子任务描述')),
                ('start_time', models.DateField(verbose_name='开始时间')),
                ('end_time', models.DateField(verbose_name='结束时间')),
                ('state', models.CharField(choices=[('o', '进行中'), ('i', '审核中'), ('c', '已完成'), ('s', '已取消')], default='o', max_length=1, verbose_name='状态')),
            ],
            options={
                'verbose_name': '子任务',
                'verbose_name_plural': '子任务',
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='名称')),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('updated_time', models.DateTimeField(auto_now=True, verbose_name='修改时间')),
                ('start_time', models.DateField(verbose_name='开始时间')),
                ('end_time', models.DateField(verbose_name='结束时间')),
                ('describe', models.TextField(verbose_name='描述')),
                ('secret', models.BooleanField(default=False, verbose_name='私密')),
                ('task_class', models.CharField(choices=[('o', '普通任务'), ('t', '测试任务'), ('d', '设计任务'), ('f', '缺陷任务'), ('r', '需求任务'), ('e', '其他任务')], default='o', max_length=1, verbose_name='任务类型')),
                ('rate', models.CharField(choices=[('o', '普通'), ('i', '重要'), ('e', '紧急'), ('u', '重要紧急')], default='n', max_length=1, verbose_name='优先级')),
                ('state', models.CharField(choices=[('o', '未完成'), ('i', '待审核'), ('c', '已完成'), ('s', '已取消')], default='o', max_length=1, verbose_name='状态')),
            ],
            options={
                'verbose_name': '任务',
                'verbose_name_plural': '任务',
            },
        ),
        migrations.CreateModel(
            name='TaskComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('score', models.CharField(default=None, max_length=1, null=True, verbose_name='评分')),
                ('content', models.CharField(max_length=1024, null=True, verbose_name='内容')),
                ('comment_type', models.CharField(choices=[('s', '自我评价'), ('a', '审核意见'), ('r', '退回理由'), ('o', '常规报告')], default='o', max_length=1, verbose_name='进展类型')),
            ],
            options={
                'verbose_name': '任务进展与交流',
                'verbose_name_plural': '任务进展与交流',
            },
        ),
        migrations.CreateModel(
            name='TaskEnclosure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='文件名')),
                ('created_time', models.DateTimeField(auto_now_add=True, null=True, verbose_name='创建时间')),
            ],
            options={
                'verbose_name': '任务评论附件',
                'verbose_name_plural': '任务评论附件',
            },
        ),
        migrations.CreateModel(
            name='TaskFilter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='名称')),
                ('conditions', models.TextField(verbose_name='筛选条件json')),
            ],
            options={
                'verbose_name': '任务筛选条件',
                'verbose_name_plural': '任务筛选条件',
            },
        ),
        migrations.CreateModel(
            name='TaskParticipant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='加入时间')),
            ],
            options={
                'verbose_name': '任务参与人',
                'verbose_name_plural': '任务参与人',
            },
        ),
        migrations.CreateModel(
            name='TaskRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('record', models.CharField(max_length=100, verbose_name='操作记录')),
            ],
            options={
                'verbose_name': '任务操作记录',
                'verbose_name_plural': '任务操作记录',
            },
        ),
        migrations.CreateModel(
            name='TaskVisitor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='task_visitor', to='task.Task', verbose_name='任务')),
            ],
            options={
                'verbose_name': '任务可见用法',
                'verbose_name_plural': '任务可见用法',
            },
        ),
    ]
