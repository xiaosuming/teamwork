from django.contrib import admin
from task.models.Task import Task
from task.models.TaskParticipant import TaskParticipant
from task.models.TaskRecord import TaskRecord
from task.models.TaskVisitor import TaskVisitor
from task.models.SubTask import SubTask
from task.models.TaskComment import TaskComment
from task.models.TaskFilter import TaskFilter
from task.models.CommentEnclosure import CommentEnclosure
from task.models.TaskEnclosure import TaskEnclosure


admin.site.register(Task)
admin.site.register(TaskParticipant)
admin.site.register(TaskRecord)
admin.site.register(TaskVisitor)
admin.site.register(SubTask)
admin.site.register(TaskComment)
admin.site.register(TaskFilter)
admin.site.register(CommentEnclosure)
admin.site.register(TaskEnclosure)
