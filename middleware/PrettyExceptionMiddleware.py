from django.utils.deprecation import MiddlewareMixin
from django.http import JsonResponse
import traceback


class PrettyExceptionMiddleware(MiddlewareMixin):
    """以Json视图函数抛出的异常"""
    def process_exception(self, request, exception):
        traceback.print_exc()
        if hasattr(exception, 'code'):
            data = {
                    'code': 1,
                    'msg': "ERROR",
                    'details': str(exception)
                    }
            return JsonResponse(
                    status=exception.code if type(exception.code) == int else 500,
                    data=data
                    )
        else:
            data = {
                    'code': 1,
                    'msg': "ERROR",
                    'details': str(exception)
                    }
            return JsonResponse(
                    status=500,
                    data=data
                    )
