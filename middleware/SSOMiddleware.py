from user.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.deprecation import MiddlewareMixin
from starxteam.settings import REDIS_CONFIG, SSO_CONFIG, WHITE_LIST, HOST
from utils.errorcode.AuthenticationException import AuthenticationException
import redis
import base64
import json
import datetime
import hashlib
import requests


class CheckJWTMiddleware(MiddlewareMixin):
    def process_request(self, request):
        # 检查请求头中是否有JWT
        json_web_token = request.headers.get("JWT", None)
        if json_web_token and json_web_token != 'null':  # 有局部会话，检查缓存中的局部会话信息
            username = verify_jwt(json_web_token)
            if username:
                try:
                    user_obj = User.objects.get(username=username)
                except ObjectDoesNotExist:
                    user_obj = User.objects.create_user(username=username, password=SSO_CONFIG['INIT_PWD'])
                request.user = user_obj
            else:  # 缓存中没有局部会话, 解析JWT
                user_name, expires = parse_jwt(json_web_token, salt=SSO_CONFIG["SALT"])
                if user_name:
                    # 为当前请求添加用户
                    user_obj = User.objects.get(username=user_name)
                    request.user = user_obj
                    # 在请求中加入SET_JWT属性，用于CheckResponse中间件添加局部会话
                    request.set("SET_JWT", (json_web_token, expires))


def verify_jwt(jwt):
    """
    查询传入的局部会话
    :param jwt: 局部会话
    :return: 校验结果, 用户名
    """
    # 从缓存数据库中查询局部会话
    signature = jwt.split('.')[-1]
    conn = redis.Redis(
        host=REDIS_CONFIG['HOST'],
        port=REDIS_CONFIG['PORT'],
        password=REDIS_CONFIG['PASSWORD'],
        db=REDIS_CONFIG["DB"]
    )  # 连接缓存数据库
    username = conn.get(signature)

    if username:
        return username
    else:
        return None


def parse_jwt(jwt, alg="sha256", salt="konki_team"):
    """
    解密局部会话，提取其中的用户名
    :param jwt: 局部会话
    :param alg: 签名算法
    :param salt: 散列使用的盐
    :return: username
    """
    encode_header_str, encode_payload_str, signature = jwt.split(".")
    hash_obj = hashlib.new(alg)
    message = "{}.{}".format(encode_header_str, encode_payload_str).encode("utf8")
    hash_obj.update(message)
    hash_obj.update(salt.encode("utf8"))
    if hash_obj.hexidigest() == signature:
        payload_str = base64.b64decode(encode_header_str.decode("utf8")).decode("utf-8")
        payload = json.loads(payload_str)
        username = payload.get("username")
        expires = payload.get("expires")
        if expires > datetime.datetime.now():
            return username, expires
    return None, None


class AuthorizeRequiredMiddleware(MiddlewareMixin):
    def process_request(self, request):
        # 请求管理员页面或静态资源时不做验证
        if request.path.startswith("/admin"):
            return
        if request.path.startswith("/media"):
            return

        # 开发人员赋予admin账户用于测试
        client_ip = request.META.get("HTTP_X_REAL_IP", None) or request.META.get("REMOTE_ADDR")
        if client_ip in WHITE_LIST:  # 开发人员白名单
            user_obj = User.objects.get(username='admin')
            request.user = user_obj
            request.UAT = SSO_CONFIG['MASTER_KEY']
            return

        # 普通访问校验用户登录状态
        if not request.user.is_authenticated:
            # JWT中间件没有添加有户实例，用户首次访问，检查全局会话UAT
            uat = request.headers.get("UAT", None)
            if uat:
                # 有全局会话，请求认证服务器
                verify_uat_uri = SSO_CONFIG["AUTHENTICATE_URI"]
                response_data = requests.get(url=verify_uat_uri, headers={"UAT": uat})

                if response_data.status_code != 200:
                    # uat校验失败，报错
                    raise AuthenticationException(error=u"请至云桌面登录")

                res_json = response_data.json()
                res_data = res_json['data']
                verified = res_data.get('verified')

                if verified:  # 全局会话合法
                    user_name = res_data.get('username')
                    try:
                        user_obj = User.objects.get(username=user_name)
                    except ObjectDoesNotExist:
                        user_obj = User.objects.create_user(
                            username=user_name, password=SSO_CONFIG["INIT_PWD"]
                        )
                        user_obj.save()

                    # 为当前请求添加用户实例
                    request.user = user_obj
                    # 生成局部会话
                    jwt, expires = generate_jwt(user_name, salt=SSO_CONFIG['SALT'])
                    # 在请求中加入SET_JWT属性，用于CheckResponse中间件添加局部会话
                    cookies = json.dumps({"jwt": jwt, "expires": expires.isoformat()})
                    request.COOKIES["SET_JWT"] = cookies
                    return
            else:
                # 没有全局会话，报错
                raise AuthenticationException(error=u"请至云桌面登录")

    def process_response(self, request, response):
        if request.user.is_authenticated:
            # 当前请求中包含用户实例
            if response.status_code == 200 and not request.headers.get('JWT', None):
                # 请求中没有局部会话
                try:
                    jwt_str = request.COOKIES.get("SET_JWT")  # 有添加局部会话的设置
                    body = json.loads(response.content)
                    jwt_info = json.loads(jwt_str)
                    jwt = jwt_info["jwt"]
                    expires = datetime.datetime.fromisoformat(jwt["expires"])
                    body["JWT"] = {"JWT": jwt, "expires": expires}
                    response.content = json.dumps(body)
                except Exception:
                    pass
        return response


def generate_jwt(username, alg="sha256", salt="workbench"):
    """
    生成局部会话
    :param username: 用户名
    :param alg: 签名算法
    :param salt: 散列使用的盐
    :return: jwt
    """
    header = {
        "typ": "JWT",
        "alg": alg,
    }
    # 获取当前时间的字符串和过期时间
    now_str, expire_time = cacu_expiretime()
    # 负载信息
    payload = {
        "username": username,
        "signTime": now_str,
        "expires": expire_time.isoformat(),
    }
    # 编码便于传递
    encode_header_str = base64.b64encode(json.dumps(header).encode("utf8")).decode("utf8")
    encode_payload_str = base64.b64encode(json.dumps(payload).encode("utf8")).decode("utf8")
    message = "{}.{}".format(encode_header_str, encode_payload_str).encode("utf8")
    # 生成
    hash_obj = hashlib.new(alg)
    hash_obj.update(message)
    hash_obj.update(salt.encode("utf8"))
    signature = hash_obj.hexdigest()
    jwt = "{}.{}.{}".format(encode_header_str, encode_payload_str, signature)
    # 将JWT存入缓存数据库
    conn = redis.Redis(
        host=REDIS_CONFIG['HOST'],
        port=REDIS_CONFIG['PORT'],
        password=REDIS_CONFIG['PASSWORD'],
        db=REDIS_CONFIG["DB"]
    )  # 连接缓存数据库
    conn.set(signature, username)
    conn.expireat(signature, expire_time)
    return jwt, expire_time


def cacu_expiretime():
    """
    计算过期时间
    """
    now_date_time = datetime.datetime.now()  # 获取当前时间
    now_str = now_date_time.isoformat()  # 当前时间的字符串
    expire_time = now_date_time + datetime.timedelta(seconds=eval(SSO_CONFIG['JWT_EXPIRES']))  # UAT过期时间
    return now_str, expire_time
