from django.utils.deprecation import MiddlewareMixin
from apps.user.models import User
from django.http.response import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from utils.auth_util import check_token_id, check_jwt, map_user_with_jwt
import re


class CheckJWTTokenMiddleware(MiddlewareMixin):
    def process_request(self, request):
        """校验局部会话的中间件"""
        # 1. 获取局部会话
        jwt = request.headers.get('JWT')
        # 2. 如果有局部会话则在缓存中查找映射的用户
        if jwt:
            uid = check_jwt(jwt)
            if uid:
                try:
                    db_user = User.objects.get(id=uid)
                    request.user = db_user
                except ObjectDoesNotExist:
                    pass


class CheckTokenIdMiddleware(MiddlewareMixin):
    def process_request(self, request):
        """
        当没有有效的局部会话时, 检查全局会话
        """
        # 如果没有有效的全局会话, 则校验全局会话
        logout = re.search('logout', request.path)
        if logout:
            pass
        elif not request.user.is_authenticated:
            token_id = request.headers.get('TOKENID')
            if token_id:
                jwt = check_token_id(token_id)
                # 如果返回了有效的jwt
                if jwt:
                    db_user = map_user_with_jwt(jwt, token_id)
                    request.user = db_user
                    request.COOKIES["SET_JWT"] = jwt

    def process_response(self, request, response):
        # 当请求体中有设置局部会话的参数, 则在响应体中添加局部会话
        # response.__setitem__('SET_JWT', 'abc')
        jwt = request.COOKIES.get('SET_JWT')
        if jwt and response.status_code == 200:
            response.__setitem__('SET_JWT', jwt)
        return response


class LoginCheckMiddleware(MiddlewareMixin):
    def process_request(self, request):
        """
        当没有有效的局部会话时, 检查全局会话
        """
        # 用户没有认证
        get_file = re.search('frames', request.path)
        get_api = re.search('swagger', request.path)
        logout = re.search('logout', request.path)
        if get_file or get_api:
            request.user = User.objects.get(username='admin')
        if (not request.user.is_authenticated) and (not logout):
            res = {
                'code': 1,
                'msg': '没有有效身份信息'
            }
            return JsonResponse(res, status=401)
