from attendance.models.Attendance import Attendance
from attendance.models.LeaveApplication import LeaveApplication
import datetime


def record_sign(conn, user, today, in_time, out_time):
    # 判断考勤状态，公出/请假>漏签>早退>迟到>正常
    state = 's'  # standard
    leaveApplication = None
    startLimit = datetime.datetime(today.year, today.month, today.day, 11, 00, 00)
    endLimit = datetime.datetime(today.year, today.month, today.day, 18, 00, 00)

    if in_time and out_time:
        if in_time > startLimit:
            state = 't'  # tardy
        
        if out_time < endLimit:
            state = 'e'  # early

    if not in_time:
        print('没有签到时间')
        try:
            leaveApplication = LeaveApplication.objects.get(
                applicant=user,
                start_time__lte=startLimit,
                end_time__gte=startLimit,
            )
        except Exception:
            print('没有签到请假事件')
            print(Exception)
            state = 'm'  # missed
        else:
            if leaveApplication.leave_type != 'o':
                state = 'l'  # leave
            else:
                state = 'o'  # outside
    
    if not out_time and state != 'm':
        print('没有签退时间')
        try:
            leaveApplication = LeaveApplication.objects.get(
                applicant=user,
                start_time__lte=endLimit,
                end_time__gte=endLimit,
            )
        except Exception:
            print('没有签退请假事件')
            print(Exception)
            state = 'm'  # missed
        else:
            if leaveApplication.leave_type != 'o':
                state = 'l'  # leave
            else:
                state = 'o'  # outside

    # 存入数据库
    Attendance.objects.create(
        user=user,
        sign_date=today,
        in_time=in_time,
        out_time=out_time,
        state=state,
        application=leaveApplication,
    )

    # 删除redis中的缓存
    conn.delete('user_'+str(user.id))
