from django.db import transaction
from user.models import User
from project.models.Project import Project
from project.models.ProjectRecord import ProjectRecord
from project.models.ProjectParticipant import ProjectParticipant
from project.models.ProjectVisitor import ProjectVisitor
import datetime


FIELDNAME = {
        'name': '名称',
        'start_time': '开始时间',
        'end_time': '结束时间',
        'project_class': '项目类型',
        'describe': '项目描述',
        'secret': '私密性',
        }


def update_id_list(user, project, field, value):
    MODELTYPE = {
            'participant_ids': 'ProjectParticipant',
            'visitor_ids': 'ProjectVisitor',
            }
    USERTYPE = {
            'participant_ids': 'participant',
            'visitor_ids': 'visitor',
            }
    DELTYPE = {
            'participant_ids': '移出项目',
            'visitor_ids': '设为项目不可见',
            }
    ADDTYPE = {
            'participant_ids': '加入项目',
            'visitor_ids': '设为项目可见',
            }

    former_ids = eval("{}.objects.filter(project=project).values_list('{}', flat=True)"
                      .format(MODELTYPE[field], USERTYPE[field]))
    present_ids = value

    # 获取删去的用户
    del_ids = set(former_ids).difference(set(present_ids))
    if del_ids:
        for user_id in del_ids:
            eval("{}.objects.filter({}_id=user_id).delete()".format(MODELTYPE[field], USERTYPE[field]))
        user_names = ';'.join(list(User.objects.filter(id__in=del_ids).values_list('username', flat=True)))
        ProjectRecord.objects.create(
                user=user,
                project=project,
                record=u'{}{}'.format(user.username, user_names, DELTYPE[field])
                )
    
    # 获取新增用户
    add_ids = set(present_ids).difference(set(former_ids))
    if add_ids:
        for user_id in add_ids:
            eval("{}.objects.create({}_id=user_id, project=project)".format(MODELTYPE[field], USERTYPE[field]))
        user_names = ';'.join(list(User.objects.filter(id__in=add_ids).values_list('username', flat=True)))
        ProjectRecord.objects.create(
                user=user,
                project=project,
                record=u'{}{}'.format(user.username, user_names, ADDTYPE[field])
                )


def update_project(user, project, kwds):
    """
    update the informations of given project to the value of given kwds,
    and create related records
    """
    with transaction.atomic():
        for field, value in kwds.items():
            if field == 'participant_ids' or field == 'visitor_ids':
                update_id_list(user, project, field, value)
            elif field == 'secret':
                if str(eval('project.{}'.format(field))) != str(value):
                    eval("Project.objects.filter(id=project.id).update({}={})".format(field, value))
                    ProjectRecord.objects.create(
                            user=user,
                            project=project,
                            record="更新{}为{}".format(user.username, FIELDNAME[field], value)
                            )
            else:
                if str(eval('project.{}'.format(field))) != str(value):
                    print("value:", value)
                    eval('Project.objects.filter(id=project.id).update({}="{}")'.format(field, value))

                    ProjectRecord.objects.create(
                            user=user,
                            project=project,
                            record="更新{}为{}".format(user.username, FIELDNAME[field], value)
                            )
