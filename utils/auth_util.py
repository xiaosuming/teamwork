from django.conf import settings
from apps.user.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.forms.utils import ValidationError
import requests
import redis
import os
import jwt


AUTH_HOST = settings.HOSTS.get("OAUTH_HOST")
#
# auth_file = os.path.join(os.path.dirname(__file__), 'auth_data.json')
# with open(auth_file, 'rb') as rf:
#     AUTH_DATA = json.load(rf)
#     AUTH_ROUTER = AUTH_DATA.get('router')
#     AUTH_CRED = AUTH_DATA.get('credential')

SSO = settings.SSO
AUTH_ROUTER = SSO.get('router')
AUTH_CRED = SSO.get('credential')


def redis_conn():
    conn = redis.Redis(
        host=settings.REDIS['HOST'],
        port=settings.REDIS['PORT'],
        password=settings.REDIS['PASSWORD'],
        db=settings.REDIS["DB"],
        decode_responses=True,
    )
    return conn


def _read_pub_key():
    """读取公钥"""
    pub_key_path = AUTH_CRED.get('public_key')
    # pub_key_path = os.path.join(settings.BASE_DIR, pub_key_path)
    with open(pub_key_path, 'rb') as k:
        pub_key = k.read()
    return pub_key


def _parse_jwt(java_web_t, pub_key):
    try:
        payload = jwt.decode(java_web_t, pub_key)
    except jwt.exceptions.InvalidSignatureError:
        raise ValidationError('jwt失效')
    uid = payload.get('uid')
    username = payload.get('username')
    expires = payload.get('expires') - 60*60  # 全局会话过期时间减去1小时
    return uid, username, expires


def _mapping_jwt(java_web_t, uid, expires, token_id):
    conn = redis_conn()
    # 在redis中记录jwt与用户id的映射关系
    conn.set(java_web_t, uid)
    conn.expire(java_web_t, expires)
    # 自定义标注中心专用token_id
    token_id = "itag@{}".format(token_id)
    conn.set(token_id, java_web_t)
    conn.expire(token_id, expires)


def map_user_with_jwt(jave_w_t, token_id):
    """根据收到的jwt, 返回用户对象"""
    # 读取全局会话
    pub_key = _read_pub_key()
    # 解析jwt
    _, username, expires = _parse_jwt(jave_w_t, pub_key)
    # 创建用户
    try:
        db_user = User.objects.get(username=username)
    except ObjectDoesNotExist:
        db_user = User.objects.create_user(username=username)
    # 将jwt作为局部会话, 映射用户id
    _mapping_jwt(jave_w_t, db_user.id, expires, token_id)
    # 返回用户对象
    return db_user


def fetch_url(path_type):
    """获取需要的请求路径"""
    router = AUTH_ROUTER.get(path_type)
    return os.path.join(AUTH_HOST, router)


def check_token_id(token_id):
    """
    接受全局会话, 请求认证服务器, 获取jwt
    :param token_id:
    :return:
    """
    # 构建请求参数
    client_id = AUTH_CRED.get('client_id')
    response_type = 'id_token'
    url = fetch_url("authorisation")
    headers = {'TOKENID': token_id}
    # 请求jwt
    res = requests.get(
        url=url,  headers=headers,
        params={"client_id": client_id, "response_type": response_type},)
    if res.status_code == 200:
        res_data = res.json()
        java_w_t = res_data.get('jwt')
    else:
        java_w_t = None
    return java_w_t


def check_jwt(jwt):
    """检查缓存中是否存在局部会话"""
    conn = redis.Redis(
        host=settings.REDIS['HOST'],
        port=settings.REDIS['PORT'],
        password=settings.REDIS['PASSWORD'],
        db=settings.REDIS["DB"],
        decode_responses=True,
    )
    try:
        uid = conn.get(jwt)
    except Exception:
        uid = None
    return uid


def cancel_jwt(token_id):
    """取消局部会话"""
    conn = redis.Redis(
        host=settings.REDIS['HOST'],
        port=settings.REDIS['PORT'],
        password=settings.REDIS['PASSWORD'],
        db=settings.REDIS["DB"],
        decode_responses=True,
    )
    java_wt = conn.get(token_id)
    if java_wt:
        conn.delete(java_wt)
    try:
        conn.delete(token_id)
    except redis.exceptions.ConnectionError:
        pass
