from interest.models.InterestedProject import InterestedProject
from interest.models.InterestedTask import InterestedTask


def check_star(obj, user, task):
    """
    check project or task had been stared by an user or not
    """
    if task:
        if InterestedTask.objects.filter(task=obj, user=user).count() != 0:
            return True
        else:
            return False
    else:
        if InterestedProject.objects.filter(project=obj, user=user).count() != 0:
            return True
        else:
            return False


def parse_brief(objlist, user, task=False):
    """
    take a list of project or task,
    return a list of brief of all the objects in the given objlist
    """

    briefList = list()

    if task:
        for obj in objlist:
           brief = {'id': obj.id}
           brief['director'] = obj.director.username
           brief['name'] = obj.name
           brief['ddl'] = obj.end_time
           brief['describe'] = obj.describe
           brief['state'] = obj.get_state_display()
           brief['star'] = check_star(obj, user, task)
           brief['rate'] = obj.get_rate_display()
           briefList.append(brief)
    else:
        for obj in objlist:
           brief = {'id': obj.id}
           brief['name'] = obj.name
           brief['start_time'] = obj.start_time
           brief['end_time'] = obj.end_time
           brief['director'] = obj.director.username
           brief['supervisor'] = obj.supervisor.username
           brief['state'] = obj.get_state_display()
           brief['secret'] = obj.secret
           brief['project_class'] = obj.get_project_class_display()
           brief['star'] = check_star(obj, user, task)
           briefList.append(brief)

    return briefList
