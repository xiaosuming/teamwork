class AnonymousUserException(Exception):
    """
    用户未登录错误码
    """


    def __init__(self, code = 410, error=u"用户未登录", data=u"参数缺失"):
        self.code = code
        self.error = error
        self.data = data


    def __str__(self):
        return "参数缺失:%s"%(self.error)
