class ParaPatternException(Exception):
    """参数格式错误码"""


    def __init__(self, code=410, error=u'参数格式错误', data=u'参数格式错误'):
        self.code = code
        self.error = error
        self.data = data


    def __str__(self):
        return "参数格式错误:%s" %(self.error)
