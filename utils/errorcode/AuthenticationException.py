class AuthenticationException(Exception):
    """权限验证错误码"""


    def __init__(self, code=410, error=u'权限验证错误', data=u'权限验证错误'):
        self.code = code
        self.error = error
        self.data = data


    def __str__(self):
        return "权限验证错误:%s" %(self.error)
