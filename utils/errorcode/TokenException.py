# -*- coding:utf-8 -*-
# 参数缺失错误码
class TokenException(Exception):

    def __init__(self, code=433, error=u"资源缺失", data=u"资源缺失"):
        self.code = code
        self.error = error
        self.data = data

    def __str__(self):
        return "Token异常"
