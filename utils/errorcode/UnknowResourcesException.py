# -*- coding:utf-8 -*-
# 参数缺失错误码
class UnknowResourcesException(Exception):

    def __init__(self, code=411, error=u"参数缺失", data=u"参数缺失"):
        self.code = code
        self.error = error
        self.data = data

    def __str__(self):
        return "%s" %(self.error)
