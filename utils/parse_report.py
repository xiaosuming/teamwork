from report.models.Report import Report
from report.models.ReportUser import ReportUser


def parse_brief(report_queryset, received=False, user=None):
    report_list = list()
    for report in report_queryset:
        report_info = {
                'id': report.id,
                'user': report.user.username,
                'work_date': report.related_date.isoformat(),
                'date': report.created_time.strftime("%Y-%m-%d %H:%M:%S"),
                'report_type': report.get_report_type_display(),
                'plain_text': report.plain_text,
                }
        if received:
           report_info['readed']  = ReportUser.objects.filter(related_user=user, report=report).first().readed 

        report_list.append(report_info)

    return report_list


def parse_detail(reportObj):
    # 获取抄送对象
    copy_to_user = list()
    for relation in ReportUser.objects.filter(report=reportObj, relation_type='c'):
        copy_to_user.append(relation.related_user.username)

    # 获取审阅对象
    supervise_user = list()
    for relation in ReportUser.objects.filter(report=reportObj, relation_type='s'):
        supervise_user.append(relation.related_user.username)


    # 处理结构图数据
    #diagram = eval(reportObj.diagram)

    # 组织报告详情
    report_info = {
            'id': reportObj.id,
            'user': reportObj.user.username,
            'copy_to_user': copy_to_user,
            'supervise_user': supervise_user,
            'related_date': reportObj.related_date.isoformat(),
            'date': reportObj.created_time.strftime("%Y-%m-%d %H:%M:%S"),
            'report_type': reportObj.get_report_type_display(),
            'plain_text': reportObj.plain_text,
            'rich_text': reportObj.rich_text,
            'diagram': reportObj.diagram,
            }

    # 返回报告详情
    return report_info
