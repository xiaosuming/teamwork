from utils.errorcode.ParaPatternException import ParaPatternException
import datetime


def calculate_event_time(start_time, repeat_type, expiration, remind_type, remind_time=None):
    """
    输入开始日期，重复类型，截止时间和提醒时间，返回开始时间列表和提醒时间列表
    :param start_time: 开始时间, datetime
    :param repeat_type: 重复类型, string
    :param expiration: 截止时间, date
    :param remind_type: 提醒类型, str
    :param remind_time: 提醒时间, time
    :return: 开始时间列表, 提醒时间列表
    """
    # 统计所有需要添加的日期
    start_time_list = list()
    if repeat_type == 'd':
        # 每日重复
        delta = datetime.timedelta(days=1)
        while start_time.date() <= expiration:
            start_time_list.append(start_time)
            start_time += delta
    elif repeat_type == 'w':
        # 每周重复时
        delta = datetime.timedelta(days=7)
        while start_time.date() <= expiration:
            start_time_list.append(start_time)
            start_time += delta
    elif repeat_type == 'm':
        # 每月重复时
        while start_time.date() <= expiration:
            start_time_list.append(start_time)
            cur_mon = start_time.month
            try:
                start_time = start_time.replace(
                    month=cur_mon+1
                ) if cur_mon != 12 else start_time.replace(
                    year=start_time.year+1,
                    month=1
                )
            except ValueError:
                # 日期超过下月最大值，如01-31不能转为02-31，则将02-28/29加入列表
                start_time = start_time.replace(month=cur_mon+2)
                spec_time = start_time-datetime.timedelta(days=start_time.day)
                if spec_time <= expiration:
                    start_time_list.append(spec_time)
    elif repeat_type == 'a':
        # 每年重复
        while start_time.date() <= expiration:
            start_time_list.append(start_time)
            cur_year = start_time.year
            try:
                start_time = start_time.replace(
                    year=cur_year+1, 
                )
            except ValueError:
                # 2020/2/29无法转为2021/2/29，则添加2021/2/28
                start_time = start_time.replace(
                    year=cur_year+1,
                    month=start_time.month+1,
                    day=1,
                ) - datetime.timedelta(days=1)
    else:
        # 不重复
        start_time_list.append(start_time)

    # remind_time_list = calculate_remind_time(start_time_list, remind_type, remind_time)

    # return start_time_list, remind_time_list
    return start_time_list


def calculate_remind_time(start_time_list, remind_type, remind_time):
    """
    接受一组时间开始时间, 根据提醒类型, 返回提醒时间
    :param start_time_list: 事件开始时间
    :param remind_type: 提醒类型
    :param remind_time: 自定义提醒时间
    :return: 提醒时间列表
    """
    if remind_type not in ['f', 'h', 'd', 'c']:
        raise ParaPatternException(error=u'不能识别的提醒类型')

    remind_time_list = list()
    for start_time in start_time_list:
        if remind_type == 'f':
            # 前五分钟提醒
            remind_time_list.append(start_time-datetime.timedelta(minutes=5))
        elif remind_type == 'h':
            # 前半小时提醒
            remind_time_list.append(start_time-datetime.timedelta(minutes=30))
        elif remind_type == 'd':
            # 前一天提醒
            remind_time_list.append(start_time-datetime.timedelta(days=1))
        elif remind_type == 'c' and (type(remind_time) is datetime.time):
            # 自定义时间提醒
            remind_time_list.append(
                start_time.replace(hour=remind_time.hour, minute=remind_time.minute, second=remind_time.second))
        else:
            raise ParaPatternException(u'自定义提醒时间格式错误')
    return remind_time_list
