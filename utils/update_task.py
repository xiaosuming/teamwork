from django.db import transaction
from user.models import User
from task.models.Task import Task
from task.models.TaskRecord import TaskRecord
from task.models.TaskParticipant import TaskParticipant
from task.models.TaskVisitor import TaskVisitor
import datetime


FIELDNAME={
        'name': '名称',
        'start_time': '开始时间',
        'end_time': '结束时间',
        'describe': '任务描述',
        'secret': '私密性',
        'rate': '任务类型',
        'project': '关联任务',
        'supervisor': '审阅人',
        }


def update_id_list(user, task, field, value):
    MODELTYPE = {
            'participantIDs': 'TaskParticipant',
            'visitorIDs': 'TaskVisitor',
            }
    USERTYPE = {
            'participantIDs': 'participant',
            'visitorIDs': 'visitor',
            }
    DELTYPE = {
            'participantIDs': '移出任务',
            'visitorIDs': '设为任务不可见',
            }
    ADDTYPE = {
            'participantIDs': '加入任务',
            'visitorIDs': '设为任务可见',
            }

    formerIDs = eval("{}.objects.filter(task=task).values_list('{}', flat=True)".format(MODELTYPE[field], USERTYPE[field]))
    presentIDs = value

    # 获取删去的用户
    delIDs = set(formerIDs).difference(set(presentIDs))
    if delIDs:
        for userID in delIDs:
            eval("{}.objects.filter({}_id=userID).delete()".format(MODELTYPE[field], USERTYPE[field]))
        userNames = ';'.join(list(User.objects.filter(id__in=delIDs).values_list('username', flat=True)))
        TaskRecord.objects.create(
                user=user,
                task=task,
                record=u'{}{}'.format(user.username, userNames, DELTYPE[field])
                )
    
    # 获取新增用户
    addIDs = set(presentIDs).difference(set(formerIDs))
    if addIDs:
        for userID in addIDs:
            eval("{}.objects.create({}_id=userID, task=task)".format(MODELTYPE[field], USERTYPE[field]))
        userNames = ';'.join(list(User.objects.filter(id__in=addIDs).values_list('username', flat=True)))
        TaskRecord.objects.create(
                user=user,
                task=task,
                record=u'{}{}'.format(user.username, userNames, ADDTYPE[field])
                )


def update_task(user, task, kwds):
    """
    update the informations of given task to the value of given kwds,
    and create related records
    """
    with transaction.atomic():
        for field, value in kwds.items():
            if field == 'participantIDs' or field == 'visitorIDs':
                update_id_list(user, task, field, value)
            elif field == 'project' and task.project != value:
                print('更新项目')
                task.project=value
                task.save()
                TaskRecord.objects.create(
                        user=user,
                        task=task,
                        record=u'关联项目更改为{}'.format(user.username, value.name),
                        )
            elif field == 'supervisor' and task.supervisor != value:
                print('更新审阅人')
                task.supervisor=value
                task.save()
                TaskRecord.objects.create(
                        user=user,
                        task=task,
                        record=u'关联审核人更改为{}'.format(user.username, value.username),
                        )
            else:
                if str(eval('task.{}'.format(field))) != str(value):
                    eval("Task.objects.filter(id=task.id).update({}='''{}''')".format(field, value))
                    TaskRecord.objects.create(
                            user=user,
                            task=task,
                            record="{}更新为{}".format(user.username, FIELDNAME[field], value)
                            )
