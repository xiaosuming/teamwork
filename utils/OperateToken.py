from starxteam.settings import REDIS, SSO_CONFIG
import redis
import requests

# @class OperateToken Token操作
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	操作token
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2019/12/03 20:27:22|张志鹏|创建模块|完成|
# -----------------------------------------------------------------------------------------------------


def get_access_token(uat):
    conn = redis.Redis(
        host=REDIS['HOST'],
        port=REDIS['PORT'],
        password=REDIS['PASSWORD'],
        db=REDIS["DB"],
        decode_responses=True,
    )
    access_token = conn.get("workbench_access_token")
    if not access_token:  # 缓存中没有access_token
        refresh_token = conn.get("workbench_refresh_token")
        if refresh_token:
            access_token = fresh_token(refresh_token, conn, uat)
        else:  # refresh_token实效，重新请求token
            access_token = request_token(uat)
    return access_token


def fresh_token(refresh_token, conn, uat):
    """
    接受一个C.A.S返回的token信息字典和一个redis连接，将token按过期时间存入缓存数据库
    :param refresh_token: refresh_token字符串
    :param conn: redis连接对象
    :return:access_token
    """
    # 组织请求参数
    query_data = {
        "refresh_token": refresh_token,
        "client_id": SSO_CONFIG["CLIENT_ID"],
        "client_secret": SSO_CONFIG["CLIENT_SECRET"],
        "grant_type": "refresh_token"
    }
    query_headers = {"UAT": uat}
    # 请求新的access_token
    token_data = requests.post(SSO_CONFIG["TOKEN_URI"], data=query_data, headers=query_headers)
    if token_data.status_code == 200:
        token_dict = token_data.json()
        access_token = operate_token(token_dict, conn)
    else:
        access_token = None
    return access_token


def operate_token(token_dict, conn):
    """
    接受一个token字典，存入缓存并返回access_token
    :param token_dict:
    :param conn:
    :return:
    """
    access_token = token_dict["access_token"]
    expires_in = token_dict["expires_in"]
    token_type = token_dict["token_type"]
    refresh_token = token_dict["refresh_token"]
    # scope = token_dict["scope"]
    access_token_stored = "{} {}".format(token_type, access_token)
    conn.set("workbench_access_token", access_token_stored)
    conn.expire("workbench_access_token", expires_in)
    conn.set("workbench_refresh_token", refresh_token)
    return access_token_stored


def request_token(uat):
    """
    请求C.A.S获取token
    :return: access_token
    """
    query_params = {
        "response_type": "code",
        "client_id": SSO_CONFIG["CLIENT_ID"]
    }
    headers = {"UAT": uat}
    token_res = requests.get(SSO_CONFIG["AUTHORIZE_URI"], params=query_params, headers=headers)
    token_dict = token_res.json()
    if token_dict["code"] == 0:
        access_token = token_dict["data"]["access_token"]
    else:
        access_token = None
    return access_token


def remove_access_token(uat):
    conn = redis.Redis(
        host=REDIS['HOST'],
        port=REDIS['PORT'],
        password=REDIS['PASSWORD'],
        db=REDIS["DB"]
    )
    conn.delete("workbench_access_token")
    conn.delete("workbench_refresh_token")
    return request_token(uat)
