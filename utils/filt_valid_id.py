from user.models import User


def filt_valid_id(userIdList):
    """
    接受一个id列表，返回存在且is_active=1的id, username列表
    """
    idList = list()
    usernameList = list()
    for userID in userIdList:
        userObj = User.objects.filter(id=userID, is_active=1).first()
        if userObj != None and userObj.id not in idList:
            idList.append(userObj.id)
            usernameList.append(userObj.username)

    return [idList, usernameList]
