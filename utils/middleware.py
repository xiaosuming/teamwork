from django.utils.deprecation import MiddlewareMixin
from django.http.response import JsonResponse
from user.models import User


class GrantUserMiddleware(MiddlewareMixin):
    """为当前请求添加一个用户实例"""
    def process_request(self, request):
        # 1 获取请求
        # if request.path.startswith('/admin'):
        #     user = User.objects.get(id=1)
        # else:
        #     user = User.objects.get(id=13)
        user = User.objects.get(id=1)

        # 2.将用于添加给request
        request.user = user


class LoginCheckMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if not request.user.is_authenticated:
            return JsonResponse(
                {
                    "code": 1,
                    "msg": "ERROR",
                    "detail": "AnonymousUser"
                }
            )
