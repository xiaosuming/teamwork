from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from utils.errorcode.ParaMissingException import ParaMissingException
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from starxteam.settings import SQL_CONFIG
import datetime


def reminder(user_ids=None, message=None):
    """
    向客户端推送提醒
    :param user_ids:用户id列表
    :param message: 要推送的消息
    :return:
    """
    print("定时任务触发")
    channel_layer = get_channel_layer()  # 初始化channels
    for user_id in user_ids:
        async_to_sync(channel_layer.group_send)(  # 调用channels的群发功能
            "notification_"+str(user_id),
            {
                "type": "get_notification",  # 使用get_notification消费者函数
                "message": message,
            }
        )


def caculate_remind_datetime(initial=None, deadline=None, interval=None):
    """
    @:param: initial: 开始时间 years: 年 months: 月 days: 天 hours: 小时 minutes: 分钟 seconds： 秒
    @param: deadline: 结束时间 years: 年 months: 月 days: 天 hours: 小时 minutes: 分钟 seconds： 秒
    @param: interval: 间隔时间
    @result 返回距离经间隔时间到指定时间的日期
    """
    interval = datetime.timedelta(interval)
    if initial:
        trigger_time = initial + interval
    else:
        trigger_time = deadline - interval
    return trigger_time


def reminder_scheduler(**kwargs):
    """
    添加定时任务
    """
    # 初始化调度对象
    url = "mysql://{}:{}@{}:{}/{}".format(
        SQL_CONFIG["USER"], SQL_CONFIG["PASSWORD"], SQL_CONFIG["HOST"],
        SQL_CONFIG["PORT"], SQL_CONFIG["NAME"]
    )
    job_stores = {
        "default": SQLAlchemyJobStore(url=url)
    }
    job_scheduler = BackgroundScheduler(jobstores=job_stores)

    # 计算提醒的触发时间
    if "trigger_time" in kwargs.keys():  # 参数中有触发时间
        trigger_time = kwargs["trigger_time"]
    elif "interval" in kwargs.keys():  # 参数中有间隔时间
        interval = kwargs["interval"]
        if "initial_time" in kwargs.keys():  # 有初始化时间
            initial_time = kwargs["initial_time"]
            trigger_time = caculate_remind_datetime(initial=initial_time, interval=interval)
        if "deadline" in kwargs.keys():  # 有截至时间
            deadline = kwargs["dealine"]
            trigger_time = caculate_remind_datetime(deadline=deadline, interval=interval)
    else:  # 缺少时间参数
        raise ParaMissingException(error=u"缺少触发时间参数'trigger_time'或'interval'&'initial_time'/'deadline'")

    # 添加任务
    user_ids = kwargs["user_ids"]
    message = kwargs["message"]
    scheduler_job = job_scheduler.add_job(
        func=reminder,  # 执行的函数
        trigger='date',  # 按日期处发
        run_date=trigger_time,  # 触发日期
        args=[user_ids, message],  # 函数参数
    )

    # 开始调度器
    job_scheduler.start()
    return scheduler_job
