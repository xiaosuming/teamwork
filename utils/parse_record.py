from user.models import User
from project.models.ProjectRecord import ProjectRecord
from task.models.TaskRecord import TaskRecord


def parse_record(Obj, task=False):
    """
    return a list of all the record related to the taken Obj
    """
    if task:
        records = TaskRecord.objects.filter(task=Obj).order_by('-created_time') \
            .values('user', 'created_time', 'record')
        for record in records:
            record['user'] = User.objects.get(id=record['user']).username
            record['created_time'] = record['created_time'].date()
        return list(records)
    else:
        records = list()
        project_records = ProjectRecord.objects.filter(project=Obj).order_by('-created_time')
        for project_record in project_records:
            info = {
                "class": "project",
                "name": project_record.project.name,
                "created_time": project_record.created_time.isoformat(sep=' '),
                "user": project_record.user.username,
                "record": project_record.record
            }
            records.append(info)
        related_task_ids = Obj.attached_task.all().values_list('id', flat=True)
        task_records = TaskRecord.objects.filter(task_id__in=related_task_ids).order_by('-created_time')
        for task_record in task_records:
            info = {
                "class": "task",
                "name": task_record.task.name,
                "created_time": task_record.created_time.isoformat(sep=' '),
                "user": task_record.user.username,
                "record": task_record.record
            }
            records.append(info)
        records.sort(key=lambda x: x["created_time"])
        return records
