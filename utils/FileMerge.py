from enclosure.models.Enclosure import Enclosure
import hashlib
import os
import shutil
import random


class FileManager:
    def file_md5(self, tmp_dir, total_chunk):
        """
        生成文件唯一识别符
        :param tmp_dir: 其人片文件临时存放路径
        :param total_chunk: 切片总数
        :return: 文件唯一标识符
        """
        # 1.实例化新的md5对象
        md5_obj = hashlib.md5()
        # 2.依次读取切片文件，更新md5值
        for i in range(1, total_chunk+1):
            tmp_path = os.path.join(tmp_dir, str(i))
            if not os.path.join(tmp_path):
                continue
            with open(tmp_path, 'rb')as f:
                md5_obj.update(f.read())
        # 3.返回md5值
        return md5_obj.hexdigest()

    def file_merge(self, tmp_dir, file_name, file_type, total_chunk):
        """
        将上传的文件切片合成并存入相应的目录
        :param tmp_dir: 其人片文件临时存放路径
        :param file_name: 文件名
        :param file_type: 文件类型
        :param total_chunk: 切片总数
        :return: 附件对象id
        """
        # 存储文件
        # 1.1.生成唯一文件存储名
        md5 = self.file_md5(tmp_dir, total_chunk)

        # 1.2.检查是否已有相同文件
        enclosure = Enclosure.objects.filter(md5=md5).first()
        if enclosure:
            return enclosure.id, enclosure.path

        # 2.1.定义文件存储路径
        random_num = random.randint(1, 100)  # 文件随机储存到不同的路径
        save_path = os.path.join('media/enclosure/', str(random_num))

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        # 2.2.生成文件存储名称
        save_name = md5
        if "." in file_name:
            file_type = file_name.rsplit('.')[1]
            save_name = save_name + '.' + file_type
        file_path = os.path.join(save_path, save_name)
        file_size = 0

        # 2.3. 读取切片文件写入
        with open(file_path, 'wb+') as fp:
            for i in range(1, total_chunk + 1):
                tmp_path = os.path.join(tmp_dir, str(i))
                if not os.path.join(tmp_path):
                    continue
                tmp_file = open(tmp_path, 'rb').read()
                fp.write(tmp_file)
                file_size += len(tmp_file)

        # 2.4. 删除临时切片文件夹
        shutil.rmtree(tmp_dir)

        # 3.记录附件信息
        enclosure = Enclosure.objects.create(
            name=file_name,
            size=file_size,
            file_type=file_type,
            path=file_path,
            md5=md5,
        )

        # 5.返回文件参数
        return enclosure.id, enclosure.path

    def file_delete(self, enclosure):
        """
        接受一个附件对象，如没有关联项目，则删除
        :param enclosure: 附件对象
        """
        # 检查附件是否有关联项
        attached_task = enclosure.attached_task.count()
        attached_t_comment = enclosure.attached_task_comment.count()
        attached_project = enclosure.attached_project.count()

        total = attached_task + attached_t_comment + attached_project

        if total == 0:
            # 删除附件
            file_path = enclosure.path
            os.remove(file_path)
            enclosure.delete()
