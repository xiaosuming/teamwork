from user.models import User
from project.models.ProjectParticipant import ProjectParticipant
from task.models.TaskParticipant import TaskParticipant


def parse_detail(Obj, task=False):
    """
    take a instance of Project or Task,
    return a dict of detail of the instance
    """

    if task:
        detail = {'rate': Obj.get_rate_display()}
        detail['task_type'] = Obj.get_task_class_display()
        detail['project'] = (Obj.project.id, Obj.project.name) if Obj.project else (None, None)
        participantIDs = TaskParticipant.objects.filter(task=Obj).values_list('participant', flat=True)
    else:
        detail = {'project_class': Obj.get_project_class_display()}
        participantIDs = ProjectParticipant.objects.filter(project=Obj).values_list('participant', flat=True)
    participants = list(User.objects.filter(id__in=participantIDs).values_list('username', flat=True))

    detail['id'] = Obj.id
    detail['name'] = Obj.name
    detail['start_time'] = Obj.start_time
    detail['end_time'] = Obj.end_time
    detail['describe'] = Obj.describe
    detail['secret'] = Obj.secret
    detail['director'] = Obj.director.username
    detail['supervisor'] = Obj.supervisor.username
    detail['participant'] = participants
    detail['state'] = Obj.get_state_display()

    return detail
