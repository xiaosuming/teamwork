from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.base import SchedulerAlreadyRunningError
from apscheduler.jobstores.base import JobLookupError
from starxteam.settings import SQL_CONFIG
from utils.errorcode.ParaMissingException import ParaMissingException
from utils.errorcode.ParaPatternException import ParaPatternException
import datetime


class AgendaRemind:
    def __init__(self, job_store_url=None):
        """
        接受定时任务存储路径，生成任务调度器
        :param job_store_url:
        """
        if not job_store_url:
            job_store_url = "mysql://{}:{}@{}:{}/{}".format(
                SQL_CONFIG["USER"], SQL_CONFIG["PASSWORD"], SQL_CONFIG["HOST"],
                SQL_CONFIG["PORT"], SQL_CONFIG["NAME"])
        job_stores = {
            "default": SQLAlchemyJobStore(url=job_store_url)
        }
        self.job_scheduler = BackgroundScheduler(jobstores=job_stores)
        try:
            self.job_scheduler.start()
        except SchedulerAlreadyRunningError:
            pass

    def set_remind(self, start_time, expiration,  participant_ids, content, event_u_id,
                   repeat_type, remind_type, remind_time=None):
        """
        定时提醒用户事件
        :param start_time: 事件开始时间
        :param expiration: 事件结束时间
        :param participant_ids: 事件参与用户id列表
        :param content: 事件内容
        :param event_u_id: 事件唯一标识符
        :param repeat_type: 事件重复类型
        :param remind_type: 事件提醒类型
        :param remind_time: 事件提醒时间
        """
        # 1.计算提醒时间参数
        remind_time = cal_remind_time(remind_type, start_time, remind_time)

        # 2.1.初始化定时任务调度器参数
        job_kwargs = {
            "id": event_u_id,
            "func": send_remind,
            "args": (participant_ids, content),
            "trigger": "cron",
            "year": '*',
            "month": '*',
            "day": "*",
            "week": '*',
            "day_of_week": '*',
            "hour": remind_time.hour,
            "minute": remind_time.minute,
            "second": remind_time.second,
            "start_date": start_time - datetime.timedelta(days=1),
            "end_date": expiration,
            "replace_existing": True,
        }

        # 2.2.根据重复类型更新调度参数
        job_kwargs = update_job_kwargs(repeat_type, remind_time, job_kwargs)

        # 3.添加日程提醒任务
        self.job_scheduler.add_job(**job_kwargs)
        # 开启任务调度器
        try:
            self.job_scheduler.start()
        except SchedulerAlreadyRunningError:
            pass

    def modify_remind(self, job_id, **changes):
        """
        修改一条通知事件
        :param job_id: 事件id
        :param changes: 修改内容
        """
        changes.update({"func": send_remind})
        self.job_scheduler.modify_job(job_id, **changes)

    def remove_remind(self, job_id):
        """
        移除一条通知事件
        :param job_id: 事件id, event_9
        """
        try:
            self.job_scheduler.remove_job(job_id=job_id)
        except JobLookupError:
            pass


def send_remind(participant_ids, content):
    """
    向用户发送一条消息
    :param participant_ids: 用户id
    :param content: 提醒内容
    """
    channel_layer = get_channel_layer()
    with open("/home/starx/Desktop/a.txt", 'w') as a:
        a.write("test")

    for user_id in participant_ids:
        async_to_sync(channel_layer.group_send)(
            "notification_" + str(user_id),
            {
                "type": "send_remind",
                "message": content,
            }
        )


def cal_remind_time(remind_type, start_time, remind_time=None):
    """
    计算提醒时间
    :param remind_type: 提醒类型
    :param start_time: 事件开始时间
    :param remind_time: 自定义提醒时间
    :return: 事件提醒时间
    """
    if remind_type == 'f':
        # 前五分钟提醒
        remind_time = start_time - datetime.timedelta(minutes=5)
    elif remind_type == 'h':
        # 前半小时提醒
        remind_time = start_time - datetime.timedelta(minutes=30)
    elif remind_type == 'd':
        # 前一天提醒
        remind_time = start_time - datetime.timedelta(days=1)
    elif remind_type == 'c' and not remind_time:
        raise ParaMissingException(error=u'请输入自定义提醒时间')
    return remind_time


def update_job_kwargs(repeat_type, remind_time, job_kwargs):
    """
    返回符合要求的任务调度参数
    :param repeat_type: 重复类型
    :param remind_time: 提醒时间
    :param job_kwargs: 初始任务参数
    :return: 符合重复类型的任务参数
    """
    if repeat_type == 'n':
        # 永不重复
        job_kwargs.update(
            {
                "year": remind_time.year,
                "month": remind_time.month,
                "day": remind_time.day
            }
        )
    elif repeat_type == 'd':
        # 每天重复
        pass
    elif repeat_type == 'w':
        # 每周重复
        job_kwargs.update(
            {
                "year": '*',
                "month": '*',
                "day": "*",
                "week": '*',
                "day_of_week": remind_time.weekday(),
            }
        )
    elif repeat_type == 'm':
        # 每月重复
        job_kwargs.update(
            {
                "day": remind_time.day,
            }
        )
    elif repeat_type == 'a':
        # 按年重复
        job_kwargs.update(
            {
                "month": remind_time.month,
                "day": remind_time.day
            }
        )
    else:
        raise ParaPatternException(error=u'不能识别的重复方式')
    return job_kwargs
