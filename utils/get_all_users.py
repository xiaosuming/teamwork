from user.models import User


def get_all_users():
    allUser = list()
    userInfos = User.objects.filter(is_active=1).values_list('id', 'colour', 'nickname', 'username')
    for userInfo in userInfos:
        allUser.append(
            {
                'user_id': userInfo[0],
                'user_nickname': userInfo[2],
                'username': userInfo[3],
                'user_colour': userInfo[1],
            }    
        )
    return(allUser)
