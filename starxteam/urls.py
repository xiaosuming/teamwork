from django.contrib import admin
from django.urls import path
from django.urls import include
from django.conf.urls import url
from django.views.static import serve


urlpatterns = [
    path('admin/', admin.site.urls),
    path('teamwork/', include('api.urls')),
    url('media/(?P<path>.*)$', serve, {'document_root': 'media'}, name='media')
]


# 定时检查签到签退
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from starxteam.settings import SQL_CONFIG
from api.attendanceapi.check_attendance import check_sign_in, check_sign_out

check_in_scheduler = None
check_out_scheduler = None


def check_sign_scheduler():
    """
        添加定时任务
    """
    # 初始化调度对象
    url = "mysql://{}:{}@{}:{}/{}".format(
        SQL_CONFIG["USER"], SQL_CONFIG["PASSWORD"], SQL_CONFIG["HOST"],
        SQL_CONFIG["PORT"], SQL_CONFIG["NAME"]
    )
    job_stores = {
        "default": SQLAlchemyJobStore(url=url)
    }
    job_scheduler = BackgroundScheduler(jobstores=job_stores)
    # 添加任务
    global check_in_scheduler
    check_in_scheduler = job_scheduler.add_job(
        func=check_sign_in,
        trigger='cron',
        day_of_week='mon-fri',
        hour=12,
        replace_existing=True,
    )
    global check_out_scheduler
    check_out_scheduler = job_scheduler.add_job(
        func=check_sign_out,
        trigger='cron',
        day_of_week='tue-sat',
        hour=0,
        replace_existing=True,
    )

    # 开启任务调度器
    job_scheduler.start()

check_sign_scheduler()
