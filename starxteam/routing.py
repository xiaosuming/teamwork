from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
import consumer.routing


application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AuthMiddlewareStack(
        URLRouter(
            consumer.routing.websocket_urlpatterns
        )
    ),
})


# def receive(self, text_data):
#     text_data_json = json.loads(text_data)
#     message = text_data_json['message']
#
#     self.send(text_data=json.dumps({
#         'message': message
#     }))
#
# def receive(self, text_data):
#     text_data_json = json.loads(text_data)
#     message = text_data_json['message']
#
#     # Send message to room group
#     async_to_sync(self.channel_layer.group_send)(
#         self.room_group_name,
#         {
#             'type': 'chat_message',
#             'message': message
#         }
#     )
