FROM python:3.7
ENV PYTHONBUFFERED 1

RUN sed -i 's@http://deb.debian.org@http://mirrors.aliyun.com@g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get -y install vim \
    && mkdir -p /home/konkii_team \
    && cd /home/konkii_team/ \
    && mkdir logs\
    && mkdir media \
    && mkdir media/temp \
    && rm -r /var/lib/apt/lists/*

WORKDIR /home/konkii_team

COPY ./requirements.txt /home/konkii_team

RUN pip install -r /home/konkii_team/requirements.txt -i https://mirrors.aliyun.com/pypi/simple/ \
    && rm -rf ~./cache/pip \
    && rm -rf /tmp

COPY ./ /home/konkii_team

RUN chmod 777 start.sh

CMD ["./start.sh"]