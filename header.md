

## <span id="api-example-for-a-submenu-entry">头部</span>



    {
      "name": "项目协同API",
      "version": "0.3.0",
      "description": "apiDoc example project",
      "title": "Custom apiDoc browser title",
      "url": "http://www.sunsidetech.com:8002",
      "apidoc": {
        "header": {
          "title": "My own header title",
          "filename": "header.md"
        },
        "footer": {
          "title": "My own footer title",
          "filename": "footer.md"
        }
      }
    }
