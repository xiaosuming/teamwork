import django
import os


def initialize():
    '''系统初始化'''
    os.environ.setdefault(
        "DJANGO_SETTINGS_MODULE",
        "starxteam.settings"
    )
    django.setup(set_prefix=False)

    from user.models import User
    try:
        User.objects.create_superuser(username='admin', password='qwe12345', email='')
    except Exception:
        pass


if __name__ == "__main__":
    initialize()
