@[TOC]
# 接口描述
1. 所有请求参数都是字符串类型。
2. 请求参数，比如ID，名称，日期等都会在服务端进行校验，但是希望前端可以先行校验
3. 所有接口的响应都是json数据，格式如下：  
   无附加数据，  
   {
       "code": code,  // 错误码  
       "msg": message,  // 错误信息  
   }  
   有附加信息，  
   {  
       "code": code,  // 错误码  
       "msg": message,  // 错误信息  
       "data": data  // 附加数据  
   }  
  

# 用户
## 添加用户
**接口:** /users/  
**说明:** 添加用户，返回用户id，仅超级用户可访问
**请求类型: POST**  
**请求参数:**  
  * {Stirng}&emsp;username&emsp;用户姓名&emsp;有唯一性限制  
  * {String}&emsp;password&emsp;初始密码  
  * {Stirng}&emsp;email&emsp;用户邮箱  
  * {Stirng}&emsp;gender&emsp;用户性别&emsp;(('m', '男'), ('f', '女'))
    
**错误码:**
  * 0: 添加成功 data: 用户id
  * 1: 请求出错 msg: 错误信息

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  "data": 3,  
  }
  * 出错: code为1

## 修改用户密码  
**接口:** /users/  
**说明:** 添加用户，返回用户id
**请求类型: PUT**  
**请求参数:**  
  * {Stirng}&emsp;password&emsp;新密码  
  
**错误码:**
  * 0: 修改成功  
  * 1: 请求出错 msg: 错误信息  

**响应示例:**
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  }  
  * 出错: code为1  
  
## 删除用户  
**接口:** /users/  
**说明:** 标记用户为删除态，返回用户id，仅超级用户可以访问  
**请求类型: DELETE**  
**请求参数:**  
  * {Stirng}&emsp;username&emsp;用户姓名  

**错误码:**  
  * 0: 请求成功&emsp;data: 用户id  
  * 1: 请求出错&emsp;msg: 错误信息  

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  "data": 109,  
  }  
  * 出错: code为1  
  
# 公共分组
## 获取所有公共分组
**接口:**/groups/  
**说明:** 返回全部公共分组信息  
**请求类型: GET**  
**错误码:**  
  * 0: 请求成功&emsp;data: 公共分组列表信息
  * 1: 请求出错&emsp;msg: 错误信息  
  
**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  "data": \[  
  &emsp;&emsp;&emsp;&emsp;{  
  &emsp;&emsp;&emsp;&emsp;'groupid': 1,  
  &emsp;&emsp;&emsp;&emsp;'groupname': '分组一',  
  &emsp;&emsp;&emsp;&emsp;'groupuser': \[11, 12, 18],  
  &emsp;&emsp;&emsp;&emsp;}  
  &emsp;&emsp;&emsp;&emsp;]  
  }  
  * 出错: code为1  
   
## 新建公共分组
**接口:**/groups/  
**说明:** 新建公共分组  
**请求类型: POST**  
**请求参数:**  
 * {Stirng}&emsp;name&emsp;分组名称  
 
**错误码:**  
  * 0: 请求成功 data: 公共分组id
  * 1: 请求出错 msg: 错误信息  

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  "data": 2,  
  }  
  * 出错: code为1  
  
## 管理公共分组中的用户
**接口:**/groups/id/  
**说明:** 修改公共分组中的用户  
**请求类型: POST**  
**请求参数:**  
 * {List}&emsp;user_id&emsp;需要包含在公共分组中的用户id列表&emsp;\[1, 2, 3\]  
**错误码:**  
  * 0: 请求成功 
  * 1: 请求出错&emsp;msg: 错误信息  

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  }
  * 出错: code为1  
   
## 删除公共分组
**接口:**/groups/id/  
**说明:** 删除指定id的公共分组  
**请求类型: DELETE**  
**错误码:**  
  * 0: 请求成功
  * 1: 请求出错&emsp;msg: 错误信息  

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  }  
  * 出错: code为1  
   
# 私有分组  
## 获取用户自定义的所有分组  
**接口:**/privategroups/  
**说明:** 返回当前用户自定义的全部分组信息  
**请求类型: GET**  
**错误码:**  
  * 0: 请求成功&emsp;data: 私有分组列表信息
  * 1: 请求出错&emsp;msg: 错误信息  

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  "data": \[  
   &emsp;&emsp;&emsp;&emsp;{  
   &emsp;&emsp;&emsp;&emsp;"groupid": 1,  
   &emsp;&emsp;&emsp;&emsp;"groupname": '分组一',  
   &emsp;&emsp;&emsp;&emsp;"groupuser": \[11, 12, 18]  
   &emsp;&emsp;&emsp;&emsp;},  
   &emsp;&emsp;&emsp;&emsp;]  
  }  
  * 出错: code为1  
  
## 新建私有分组  
**接口:** /privategroups/  
**说明:** 新建私有分组  
**请求类型: POST**  
**请求参数:**  
 * {Stirng}&emsp;group_name&emsp;私有分组名称

**错误码:**  
  * 0: 创建成功&emsp;data: 新建分组id
  * 1: 创建失败&emsp;msg: 错误信息
**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  "data": 14,  
  }  
  * 出错: code为1  

## 修改私有分组内用户
**接口:** /privategroups/id/  
**说明:** 修改私有分组内用户信息  
**请求类型: POST**  
**请求参数:**
 * {List}&emsp;user_id&emsp;需要包含于私有分组内的全部用户id  
**错误码:**  
  * 0: 删除成功
  * 1: 删除失败&emsp;msg: 错误信息

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",   
  }  
  * 出错: code为1  
  
## 删除私有分组
**接口:** /privategroups/id/  
**说明:** 删除私有分组  
**请求类型: DELETE**  
**错误码:**  
  * 0: 删除成功
  * 1: 删除失败&emsp;msg: 错误信息

**响应示例:**  
  * 正常:  
  {  
  "code": 0,  
  "msg": "SUCCESS",  
  }  
  * 出错: code为1  
  
  
# 工作总结
## 获取所有收到的工作总结
**接口:** /reports/received/
**说明:** 获取收到的工作总结列表
**请求类型: GET**
**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 返回当前用户收到的工作总结列表
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  

**响应示例:**
* 正常:  
{  
"code": 0,  
'msg': 'SUCCESS',  
'data': \[  
&emsp;&emsp;{  
&emsp;&emsp;'user': '张三'，  
&emsp;&emsp;'date': '2019-10-10',  
&emsp;&emsp;'report_type':  '日报',  
&emsp;&emsp;'content': {  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;ops: \[  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{insert: 'Gandalf', attributes: { bold: true}},  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{insert: ' the ' },  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{insert: 'Grey', attributes: { color: '#cccccc'}}  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;]  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;&emsp;}  
&emsp;&emsp;&emsp;]  
&emsp;&emsp;}
* 出错: code为1

## 获取所有发送的工作总结
**接口:** /reports/issued/
**说明:** 返回当前用户发送的所有工作总结的列表
**请求类型: GET**
**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 当前用户发送的工作总结摘要列表
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  

**响应示例:**
* 正常:  
{  
"code": 0,  
'msg': 'SUCCESS',  
'data': \[  
&emsp;&emsp;{  
&emsp;&emsp;'id': 1,  
&emsp;&emsp;'user': '张三', 
&emsp;&emsp;'date': '2019-10-10',  
&emsp;&emsp;'report_type': '日报',  
&emsp;&emsp;'content': {  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;ops: \[  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{insert: 'Gandalf', attributes: { bold: true}},  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{insert: ' the ' },  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{insert: 'Grey', attributes: { color: '#cccccc'}},  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;]  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;]  
}
* 出错: code为1

## 发送工作总结
**接口:** /reports/issued/
**说明:** 新建工作总结
**请求类型: POST**
**请求参赛:**  
* {List}&emsp;copy_to_user&emsp;抄送用户id列表&emsp;\[11, 12, 13, 14, 15]  
* {List}&emsp;supervise_user&emsp;审阅用户id列表&emsp;\[1, 2, 3]
* {String}&emsp;report_type&emsp;工作总结类型&emsp;(('d', '日报'), ('w', '周报'), ('m', '月报'))  
* {Json}&emsp;content&emsp;工作总结内容  
* {List}&emsp;diagram&emsp;知识点结构图  

**错误码:**
  * 0: 请求成功
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  

**响应示例:**
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
}
* 出错: code为1

## 获取工作总结详情
**接口:** /reports/id/
**说明:** 返回id对应的工作总结详情  
**请求类型:** GET

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 工作总结详情
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
"code": 0,  
"msg": "SUCCESS",  
"report": {  
&emsp;&emsp;&emsp;&emsp;&emsp;'user': '张三',  
&emsp;&emsp;&emsp;&emsp;&emsp;'copy_to_user': \['张三', '里斯'],  
&emsp;&emsp;&emsp;&emsp;&emsp;'supervisor': \['张三'，'里斯'],  
&emsp;&emsp;&emsp;&emsp;&emsp;'date': "2019-10-10",    
&emsp;&emsp;&emsp;&emsp;&emsp;'report_type': '日报',  
&emsp;&emsp;&emsp;&emsp;&emsp;'content': {  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;ops: \[  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;insert: 'Gandalf',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attributes: { bold: true },  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{ insert: ' the ' },  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;insert: 'Grey',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;attributes: { color: '#cccccc' },  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;],  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;&emsp;&emsp;'diagram': \[  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;"content": 'Gandalf',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;"project": '',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;"task": '',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;"knowledge": '前端',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&ensp;},  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;]  
&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;}
* 错误:  code为1  

## 修改工作总结已读状态  
**接口:** /reports/id/
**说明:** 返回id对应的工作总结为已读状态  
**请求类型:** PUT

**错误码:**
  * 0: 请求成功
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
}
* 错误:  code为1  

# 项目
## 获取所有可见项目列表
**接口:** /projects/
**说明:** 返回所有可见项目的列表  
**请求类型:** GET

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 可见项目列表
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
** 响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': \[  
&emsp;&emsp;&emsp;&emsp;{  
&emsp;&emsp;&emsp;&emsp;&emsp;'id': 1,  
&emsp;&emsp;&emsp;&emsp;&emsp;'name': '团队协作项目'，  
&emsp;&emsp;&emsp;&emsp;&emsp;'start_time': '2019-10-15'  
&emsp;&emsp;&emsp;&emsp;&emsp;'end_time': '2019-11-11',  
&emsp;&emsp;&emsp;&emsp;&emsp;'director': '李大嘴',  
&emsp;&emsp;&emsp;&emsp;&emsp;'supevisor': '方大同',  
&emsp;&emsp;&emsp;&emsp;&emsp;'state': '验收中',  
&emsp;&emsp;&emsp;&emsp;&emsp;'secret': True,  
&emsp;&emsp;&emsp;&emsp;&emsp;'project_class': '软件开发',  
&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;]  
}
* 错误:  code为1  

## 新建项目
**接口:** /projects/
**说明:** 返回所有可见项目的列表  
**请求类型:** POST  
**请求参数:**
* {String}&emsp;name&emsp;项目名称  
* {String}&emsp;describe&emsp;项目描述
* {String}&emsp;project_class&emsp;项目类型&emsp;(('d', '软件开发'))  
* {Date}&emsp;start_time&emsp;开始时间&emsp;2019-10-12
* {Date}&emsp;end_time&emsp;结束时间&emsp;2019-11-11
* {Int}&emsp;supervisor_id&emsp;审核人id&emsp;1
* {Bool}&emsp;secret&emsp;是否私密项目&emsp;True\False  
* {List}&emsp;visitor_id&emsp;可见用户id列表&emsp;\[1, 2, 3]  
* {List}&emsp;participant_id&emsp;参与人id列表&emsp;\[4, 5, 6]

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 新建项目id
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': 13,  
}
* 错误: code为1  

## 项目详情
**接口:** /projects/id/
**说明:** 返回对应id的项目详情  
**请求类型:** GET  

**错误码:**
* 0: 请求成功&emsp;&emsp;data: 项目详情  
* 1: 请求失败&emsp;&emsp;msg: 错误信息  

**响应示例:**
* 正常:
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': {  
&emsp;&emsp;&emsp;&emsp;&emsp;'id': 1,  
&emsp;&emsp;&emsp;&emsp;&emsp;'name': '团队协作项目',  
&emsp;&emsp;&emsp;&emsp;&emsp;'start_time': '2019-10-15',  
&emsp;&emsp;&emsp;&emsp;&emsp;'end_time': '2019-11-11',  
&emsp;&emsp;&emsp;&emsp;&emsp;'director': '李大嘴',  
&emsp;&emsp;&emsp;&emsp;&emsp;'supevisor': '方大同',  
&emsp;&emsp;&emsp;&emsp;&emsp;'participant': \['张三', '里斯', '王五',]  
&emsp;&emsp;&emsp;&emsp;&emsp;'state': '验收中',  
&emsp;&emsp;&emsp;&emsp;&emsp;'secret': True,  
&emsp;&emsp;&emsp;&emsp;&emsp;'project_class': '软件开发',  
&emsp;&emsp;&emsp;&emsp;&emsp;'desc': '项目描述，描述项目...',  
&emsp;&emsp;&emsp;&emsp;},  
}
* 错误: code为1

## 更新项目
**接口:** /projects/id/
**说明:** 更新指定id的项目  
**请求类型:** POST  
**请求参数:**
* {String}&emsp;name&emsp;项目名称  
* {String}&emsp;describe&emsp;项目描述
* {String}&emsp;project_class&emsp;项目类型&emsp;(('d', '软件开发'))  
* {Date}&emsp;start_time&emsp;开始时间&emsp;2019-10-12
* {Date}&emsp;end_time&emsp;结束时间&emsp;2019-11-11
* {Int}&emsp;supervisor_id&emsp;审核人id&emsp;1
* {Bool}&emsp;secret&emsp;是否私密任务&emsp;True\False  
* {List}&emsp;visitor_id&emsp;可见用户id列表&emsp;\[1, 2, 3]  
* {List}&emsp;participant_id&emsp;参与人id列表&emsp;\[4, 5, 6]

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 修改项目id
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': 13,  
}
* 错误: code为1  

## 更新项目状态
**接口:** /projects/id/
**说明:** 更改指定id的项目的状态  
**请求类型:** PUT  
**请求参数:**
* {String}&emsp;state&emsp;项目状态  

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 修改后的状态
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': '已完成',  
}
* 错误: code为1  

# 任务
## 获取所有可见任务列表
**接口:** /tasks/
**说明:** 返回所有用户可见任务的列表  
**请求类型:** GET

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 可见任务列表
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
** 响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': \[  
&emsp;&emsp;&emsp;&emsp;{  
&emsp;&emsp;&emsp;&emsp;&emsp;'id': 1,  
&emsp;&emsp;&emsp;&emsp;&emsp;'name': '团队协作项目使用文档'，  
&emsp;&emsp;&emsp;&emsp;&emsp;'ddl': '2019-11-11',  
&emsp;&emsp;&emsp;&emsp;&emsp;'director': '李大嘴',  
&emsp;&emsp;&emsp;&emsp;&emsp;'state': '验收中',  
&emsp;&emsp;&emsp;&emsp;&emsp;'desctibe': '风生水起啊，每天就爱穷开心阿，逍遥的魂儿啊，假不正经啊',  
&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;]  
}
* 错误:  code为1  

## 新建任务
**接口:** /tasks/
**说明:** 新建任务  
**请求类型:** POST  
**请求参数:**
* {String}&emsp;name&emsp;任务名称  
* {String}&emsp;describe&emsp;任务描述
* {String}&emsp;rate&emsp;任务优先级&emsp;(('o', '普通'),('i', '重要'),('e','紧急'),('u','重要紧急'))  
* {Date}&emsp;start_time&emsp;开始时间&emsp;2019-10-12
* {Date}&emsp;end_time&emsp;结束时间&emsp;2019-11-11  
* {Int}&emsp;project_id&emsp;关联项目id  
* {Int}&emsp;supervisor_id&emsp;审核人id&emsp;1
* {Bool}&emsp;secret&emsp;是否私密任务&emsp;True\False  
* {List}&emsp;visitor_id&emsp;可见用户id列表&emsp;\[1, 2, 3]  
* {List}&emsp;participant_id&emsp;参与人id列表&emsp;\[4, 5, 6]

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 新建任务id
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': 6,  
}
* 错误: code为1  

## 任务详情
**接口:** /tasks/id/
**说明:** 返回对应id的任务详情  
**请求类型:** GET  

**错误码:**
* 0: 请求成功&emsp;&emsp;data: 任务详情  
* 1: 请求失败&emsp;&emsp;msg: 错误信息  

**响应示例:**
* 正常:
{  
'code': 0,  
'msg': 'SUCCESS',  
'data': {  
&emsp;&emsp;&emsp;&emsp;&emsp;'id': 1,  
&emsp;&emsp;&emsp;&emsp;&emsp;'name': '团队协作项目文档',  
&emsp;&emsp;&emsp;&emsp;&emsp;'start_time': '2019-10-15',  
&emsp;&emsp;&emsp;&emsp;&emsp;'end_time': '2019-11-11',  
&emsp;&emsp;&emsp;&emsp;&emsp;'director': '李大嘴',  
&emsp;&emsp;&emsp;&emsp;&emsp;'supevisor': '方大同',  
&emsp;&emsp;&emsp;&emsp;&emsp;'participant': \['张三', '里斯', '王五',]  
&emsp;&emsp;&emsp;&emsp;&emsp;'state': '代审核',  
&emsp;&emsp;&emsp;&emsp;&emsp;'secret': True,  
&emsp;&emsp;&emsp;&emsp;&emsp;'rate': '紧急',
&emsp;&emsp;&emsp;&emsp;&emsp;'project': (1, '团队协作项目'),  
&emsp;&emsp;&emsp;&emsp;&emsp;'desc': '任务描述，描述任务...',  
&emsp;&emsp;&emsp;&emsp;&emsp;'record': \[  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'2019-10-16 李大嘴创建了本任务',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'2019-10-16 李大嘴创建了子任务',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;]  
&emsp;&emsp;&emsp;&emsp;&emsp;'subtask': \[  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'participant': '张三',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'brief': '这是一个子任务',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'end_time': '2019-10-18',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;'state': '进行中',  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;]  
&emsp;&emsp;&emsp;&emsp;},  
}
* 错误: code为1

## 更新任务
**接口:** /tasks/id/
**说明:** 更新指定id的任务  
**请求类型:** POST  
**请求参数:**
**请求参数:**
* {String}&emsp;name&emsp;任务名称  
* {String}&emsp;describe&emsp;任务描述
* {String}&emsp;rate&emsp;任务优先级&emsp;(('o', '普通'),('i', '重要'),('e','紧急'),('u','重要紧急'))  
* {Date}&emsp;start_time&emsp;开始时间&emsp;2019-10-12
* {Date}&emsp;end_time&emsp;结束时间&emsp;2019-11-11  
* {Int}&emsp;project_id&emsp;关联项目id  
* {Int}&emsp;supervisor_id&emsp;审核人id&emsp;1
* {Bool}&emsp;secret&emsp;是否私密任务&emsp;True\False  
* {List}&emsp;visitor_id&emsp;可见用户id列表&emsp;\[1, 2, 3]  
* {List}&emsp;participant_id&emsp;参与人id列表&emsp;\[4, 5, 6]

**错误码:**
  * 0: 请求成功
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',   
}
* 错误: code为1  

## 更新任务状态
**接口:** /tasks/id/
**说明:** 更新指定id的任务状态  
**请求类型:** PUT  
**请求参数:**
* {String}&emsp;state&emsp;任务状态  

**错误码:**
  * 0: 请求成功
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',   
}
* 错误: code为1  

## 获取任务的子任务列表
**接口:** /tasks/sub/
**说明:** 返回指定任务的子任务列表  
**请求类型:** GET  
**请求参数:**
* {String}&emsp;task_id&emsp;任务id  

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 子任务列表
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',   
'data': \[  
&emsp;&emsp;&emsp;&emsp;{  
&emsp;&emsp;&emsp;&emsp;&emsp;'id': 1,  
&emsp;&emsp;&emsp;&emsp;&emsp;'director': '张三',  
&emsp;&emsp;&emsp;&emsp;&emsp;'ddl': '2019-10-15',  
&emsp;&emsp;&emsp;&emsp;&emsp;'describe': '风生水起啊，每天就爱穷开心阿，逍遥的魂儿啊，假不正经啊',  
&emsp;&emsp;&emsp;&emsp;&emsp;'state': '未完成',  
&emsp;&emsp;&emsp;&emsp;&emsp;},  
&emsp;&emsp;&emsp;&emsp;]  
}
* 错误: code为1  

## 新建子任务
**接口:** /tasks/sub/
**说明:** 新建子任务  
**请求类型:** POST  
**请求参数:**
* {Int}&emsp;task_id&emsp;所属任务id  
* {Int}&emsp;participant_id&emsp;参与人id  
* {String}&emsp;describe&emsp;子任务描述  
* {Date}&emsp;start_time&emsp;开始时间  
* {Date}&emsp;end_time&emsp;结束时间  

**错误码:**
  * 0: 请求成功&emsp;&emsp;data: 新建子任务id
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',   
'data': 15,  
}
* 错误: coed为1

## 修改子任务状态
**接口:** /tasks/sub/sub_id/
**说明:** 修改子任务状态  
**请求类型:** PUT  
**请求参数:**
* {String}&emsp;state&emsp;子任务状态&emsp;&emsp;(('o', '进行中'), ('i', '审核中'), ('c', '已完成'), ('s', '已取消'))  

**错误码:**
  * 0: 请求成功  
  * 1: 请求失败&emsp;&emsp;msg: 错误信息  
  
**响应示例:**  
* 正常:  
{  
'code': 0,  
'msg': 'SUCCESS',    
}
* 错误: coed为1
